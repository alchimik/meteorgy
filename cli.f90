program cli

 use params     , only:  lstr_ , stdout , stderr , dp , noerr
 use types      , only:  pbc , vector , pbc_convert , chk_pbc
 use file_io    , only:  file_attr , init_file , get_filename , basename , dirname
 use file_io    , only:  any_read , any_write , convert_frmt , increment_enum , name_to_frmt 
 use accounting , only:  sort_one , sort_by_tag , invert_order , compare_pbc , chk_tagmask
 use parse      , only:  parse_query , parse_selection , compact_selection
 use parse      , only:  print_integers , print_floats , print_elem , print_human_float
 use types      , only:  pbc_print , atom_print , vector_print

 implicit none
 ! ----------------------------------------------------------------------------------------
 ! Meteorgy CLI, interface most of the functionality
 ! Copyright (C) 2017-2019 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS For A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ----------------------------------------------------------------------------------------

  ! command line
  character(len=:)  , allocatable  :: execute &  ! executable string, i.e. 'meteorgy rewrite' or 'any2xsf'
                                    , mode    &  ! mode, i.e. 'rewrite', 'adsorb' or 'geominfo'
                                    , query      ! query command

  type(file_attr)                  :: infile  &  ! input file
                                    , reffile &  ! reference file
                                    , outfile    ! output file

  integer    :: exe   & ! number of arguments in executable string, used to shift arguments starting from query
              , nargs & ! number of arguments
              , stat    ! current status

  ! query
  character(len=1)  , allocatable  :: qcmd(:)    ! query command
  character(len=:)  , allocatable  :: qarg(:)    ! query argument
  
  character(len=:)  , allocatable  :: code , c   ! file type, i.e. vasp, xyz, xsf
  
  integer                          :: i, j, k, l ! counters

  type(pbc)                        :: geom   &   ! input geometry
                                    , gref       ! reference geometry
  
  logical                          :: geom_lr    ! buffer original coordinate state

  ! geominfo
  logical                          :: verbose
  real(dp)                         :: center(3)
  integer           ,  allocatable :: selec(:) , subset(:) ! selections
  type(vector)      ,  allocatable :: vectors(:)           ! vector scratch
  character(len=:)  ,  allocatable :: printline
  
  ! ----------------------------------------------------------------------------
  ! stuff everything in one executable
  ! make functionality available as first argument (mode) or by renaming/linking
  ! to the executable.
  ! Both commands lead to the same results:
  !
  !   $ meteorgy <mode> [<arguments>]
  !   $ <mode> [<arguments>]
  ! ----------------------------------------------------------------------------

  nargs = command_argument_count()

  ! check if name is meteorgy
  execute = get_arg(0)
  if( basename(execute) == 'meteorgy' ) then ! [/path/]meteorgy <mode>
    exe     = 1
    mode    = get_arg(1)
    execute = execute // ' ' // mode
  else                                         ! [/path/]<mode> (i.e. softlink)
    exe     = 0
    mode    = basename(execute)
  end if

  ! ----------------------------------------------------------------------------
  ! program run for each execution mode
  select case( mode )

    case( 'rewrite' )

      call rewrite()
      if( stat /= 0 ) stop 1

    case( 'adsorb' )

      call adsorb()
      if( stat /= 0 ) stop 1

      !if( code == 'vasp' ) code = 'vasp_def'
      !outfile          = reffile
      !outfile%frmt     = infile%frmt
      !outfile%name     = set_filename( outfile )

    case( 'geominfo' )

      call geominfo()
      if( stat /= 0 ) stop 1
      stop

    case( 'template' )

      call template()
      stop

    ! --------------------------------------------------------------------
    ! shortcuts
    case( 'any2vasp' )

      query = '%%!'
      call rewrite()
      outfile%frmt = name_to_frmt( 'vasp' )
      if( stat /= 0 ) stop 1

    case( 'any2xyz' )

      query = '%%!'
      call rewrite()
      outfile%frmt = name_to_frmt( 'xyz' )
      if( stat /= 0 ) stop 1

    case( 'any2dftb' )

      query = '%%!'
      call rewrite()
      outfile%frmt = name_to_frmt( 'dftb' )
      if( stat /= 0 ) stop 1

    case( 'any2xsf' )

      query   = '%%!'
      !call noerr()

      call setup()
      if( stat /= 0 ) stop 2
      call convert_frmt( outfile , name_to_frmt('xsf') )
      outfile%name = '-'

    case default

      call manual( mode )
      stop


  end select
  ! ----------------------------------------------------------------------------

  ! write output
  outfile%stat = stat
  call pbc_convert( geom , geom_lr )
  call any_write( outfile , geom )


 contains


  ! ----------------------------------------------------------------------------
  function get_arg( num ) RESULT( arg )
    character(len=:) , allocatable  :: arg
    integer , INTENT(IN) :: num
    integer :: i
    ! --------------------------------------------------------------------------

    ! empty if not available
    if( command_argument_count() < num ) then
      arg = ''
      return
    end if

    call get_command_argument( num , lenGTH = i )
    ALLOCATE( character(len=i) :: arg )
    call get_command_argument( num , VALUE = arg )
  end function get_arg
  ! ------------------------------------------------------------------------ end


  ! --------------------------------------------------------------------------------------
  subroutine setup()
    integer               :: k
  !use file_io, only: extract_name
    ! -----------------------------------------------------------
    ! parse command line / read files
    ! -----------------------------------------------------------

    if( .not. ALLOCATED(query) ) then
      ! if allocated, then query is predefined by shortcut.
      query = get_arg( exe + 1 )                     ! get query
    else
      ! shortcut does not read query from command line --> shift all commands by -1
      exe = exe - 1
    end if 

    if( nargs == exe ) then                          ! print manual if no args
      call manual( mode )
      stat = 1
      return
    end if

    if( nargs == exe + 1 ) then
      write(stderr,'(A,I3,A,I3,A)') "[setup]       Error, query or input file missing."
      stat = 1
      return
    end if 

    call parse_query( query , qcmd , qarg , stat )   ! parse query
    if( stat /= 0 ) return

    infile  = init_file( get_arg( exe + 2 ) )        ! get infile

    call any_read( infile , geom )                   ! read system
    geom_lr = geom%lr
    stat  = infile%stat

    if( stat /= 0 ) return
  
    outfile  =  infile
    k = len(infile%name)
    if( outfile%name(k-1:k) /= '_m' ) outfile%name = outfile%name // '_m'

  end subroutine setup


  ! ----------------------------------------------------------------------------
  subroutine rewrite()
  use algebra3d  , only: boxheight
  use types      , only: atomic_number, element_symbol
  use parse      , only: keyplane
  use meteorgy   , only: add_and_delete_atoms , make_supercell , make_slab &
                 , make_sphere , set_vacuum , standardize_box , wrap_in_cell , center_xyz
  use accounting , only: sort_by_distance
    integer               :: e , i , j , k
    character(len=2)      :: elem
    integer , allocatable :: selec(:)  ! substitution 
    integer               :: fix , plane , tdiag(3)
    real(dp)              :: thk , shf , vac , tvec(3)
    integer , allocatable :: orphan(:) , widow(:) , mask(:)

    ALLOCATE( selec(0) )

    ! -----------------------------------------------------------
    ! parse command line / read file
    ! -----------------------------------------------------------
    call setup()
    if( stat /= 0 ) return

    ! (optional) get output file name
    if( nargs == exe + 3 ) then
      outfile = init_file( get_arg( exe + 3 ) )
      ! if desired format cannot be guessed, use input format. Adjust filename accordingly
      if( outfile%stat /= 0 )  call convert_frmt( outfile , infile%frmt )

    else if( nargs > exe + 3 ) then
      write(stderr,'(A)') "[rewrite]     Error, too many arguments. Try:"
      write(stderr,'(14X,A)') "$ rewrite <query>[:<args>] <input file> [<output file>]"
      stat = 1
      return
    end if

    ! -----------------------------------------------------------
    ! test is number of query arguments is correct
    ! -----------------------------------------------------------
    k = 0
    do i=1,size(qcmd)
      select case( qcmd(i) )
        case( 'l' ) ; k = k+1
        case( 'm' ) ; k = k+2
        case( 'o' ) ; k = k+1
        case( 's' ) ; k = k+1
        case( 'C' ) ; k = k+1
        case( 'E' ) ; k = k+1
        case( 'M' ) ; k = k+2
        case( 'O' ) ; k = k+1
        case( 'S' ) ; k = k+4
        case( 'T' ) ; k = k+2
        case( 'W' ) ; k = k+1
        case( 'X' ) ; k = k+3
      end select
    enddo

    if( k /= size(qarg) ) then
      write(stderr,'(A,I2,A,I2,A)') "[rewrite]     Error, Wrong number of arguments for query commands '"//qcmd(:)// &
                                               "'. Needed:",k,", found:",size(qarg),"!"
      stat = -1
      return
    end if

    ! -----------------------------------------------------------
    ! run commands sequentially
    ! -----------------------------------------------------------
    k = 0
    do i=1,size(qcmd)
      select case( qcmd(i) )

        case( '!' )  ! do nothing without error message  -----------------------
          if( .false. ) deallocate( geom%atm )
          

        case( 'a' )  ! sort by cell vector  ------------------------------------
          write(stderr,'(a)') "[rewrite]     Sort atoms along cell vector 'a'"
          call pbc_convert( geom , .FALSE. )
          call sort_one( '1-' , geom%atm )

        case( 'b' )  ! sort by cell vector  ------------------------------------
          write(stderr,'(a)') "[rewrite]     Sort atoms along cell vector 'b'"
          call pbc_convert( geom , .FALSE. )
          call sort_one( '2-' , geom%atm )

        case( 'c' )  ! sort by cell vector  ------------------------------------
          write(stderr,'(a)') "[rewrite]     Sort atoms along cell vector 'c'"
          call pbc_convert( geom , .FALSE. )
          call sort_one( '3-' , geom%atm )
        
        case( 'd' )  ! convert to direct/fractional coordinates  ---------------
          write(stderr,'(a)') "[rewrite]     Convert to fractional coordinates"
          geom_lr = .false.

        case( 'e' )  ! sort by element  ----------------------------------------
          write(stderr,'(a)') "[rewrite]     Sort atoms by elements"
          call sort_one( 'e+' , geom%atm )

        case( 'i' )  ! invert order  -------------------------------------------
          write(stderr,'(a)') "[rewrite]     Invert order of atoms"
          call invert_order( geom%atm )

        case( 'l' )  ! set last l atoms as selection  --------------------------

          k = k+1
          read(qarg(k),*,iostat=stat) fix
          
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (l) Error, cannot read l (= '"//trim(qarg(k))//"')!"
            return
          else if( fix > size(geom%atm) ) then
            write(stderr,'(A)') "[rewrite]     (l) Warning, l > Number of atoms. Select all atoms"
            fix = size(geom%atm)
          end if

          if( ALLOCATED(selec) ) DEALLOCATE(selec)
          ALLOCATE( selec(fix) )

          do j = 1 , fix
            selec(j) = size(geom%atm) - fix + j
          enddo
          write(stderr,'(a)') "[rewrite]     Set selection to "//compact_selection( selec , size(geom%atm) )

        case( 'm' )  ! mirror and average positions in whole system  -----------
          write(stderr,'(a)') "[rewrite]     Average positions along mirror plane"
          k = k+1  ;  plane = keyplane( trim(qarg(k)) )
          if( plane == 0 ) then
            write(stderr,'(a)') "[rewrite]     (m) Error, Plane '"//trim(qarg(k))//"' not readable.  Use ab, bc or ca."
            stat = 1  ;  return
          end if

          k = k+1  ;  read(qarg(k),*,iostat=stat) shf
          if( stat /= 0 ) then
            write(stderr,'(a)') "[rewrite]     (m) Error, Shift '"//trim(qarg(k))//"' not readable.  Use float point variable."
            stat = 1  ;  return
          end if
          
          gref = geom
          call pbc_convert( geom , .false. )

          ! keep constraint atoms
          shf = 2.0d0*shf
          do j = 1,size(geom%atm)
            geom%atm(j)%r(plane) = shf-geom%atm(j)%r(plane)
          end do
          call compare_pbc( geom , gref , mask , orphan , widow )
          if( size(orphan) > 0 .or. size(widow) > 0 ) then
            write(stderr,'(a)') "[rewrite]     (m) Error, bad mirrorplane!"
            stat = 1
            return
          end if
          call chk_tagmask( atm=geom%atm , stat=stat )
          if( stat /= 0 ) return
          call sort_by_tag( geom%atm )
          do j = 1,size(geom%atm)
            geom%atm(j)%r(:) = 0.5d0*( geom%atm(j)%r(:) + gref%atm(j)%r(:) )
          end do

        case( 'o' )  ! sort by distance  ---------------------------------------
          k = k+1
          read(qarg(k),*,iostat=stat) fix
          
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (o) Error, cannot read atom index (= '"//trim(qarg(k))//"')!"
            return
          else if( fix > size(geom%atm) ) then
            write(stderr,'(A)') "[rewrite]     (o) Error, atom index out of range!"
            return
          end if
          call sort_by_distance( geom%atm(fix)%r , geom%atm )
          write(stderr,'(a,I4)') "[rewrite]     Sort atoms by proximity around atom index", fix
          
        case( 'r' )  ! convert to cartesian coordinates  -----------------------
          write(stderr,'(a)') "[rewrite]     Convert to cartesian coordinates"
          geom_lr = .true.
          
        case( 's' )  ! set selection  ------------------------------------------
          k = k+1
          write(stderr,'(a)') "[rewrite]     Setting selection to '"//trim(qarg(k))//"'..."
          call parse_selection( size(geom%atm) , trim(qarg(k)) , selec , stat )
          if( stat /= 0 ) return
          
        case( 'w' )  ! wrap geometry in cell
          write(stderr,'(a)') "[rewrite]     Wrap atoms in cell boundaries"
          call wrap_in_cell( geom )

        case( 'x' )  ! sort by cartesian vector  -------------------------------
          write(stderr,'(a)') "[rewrite]     Sort atoms along cartesian axis 'x'"
          call pbc_convert( geom , .TRUE. )
          call sort_one( '1-' , geom%atm )

        case( 'y' )  ! sort by cartesian vector  -------------------------------
          write(stderr,'(a)') "[rewrite]     Sort atoms along cartesian axis 'y'"
          call pbc_convert( geom , .TRUE. )
          call sort_one( '2-' , geom%atm )

        case( 'z' )  ! sort by cartesian vector  -------------------------------
          write(stderr,'(a)') "[rewrite]     Sort atoms along cartesian axis 'z'"
          call pbc_convert( geom , .TRUE. )
          call sort_one( '3-' , geom%atm )

        case( 'C' )  ! compare atoms -------------------------------------------
          k = k+1
          reffile  = init_file( qarg(k) )        ! get reference
          write(stderr,'(a)') "[rewrite]     Compare with atoms in "//get_filename(reffile)
          call any_read( reffile , gref )        ! read reference
          stat  = reffile%stat
          if( stat /= 0 ) return
          call compare_pbc( geom , gref , mask , orphan , widow )
          write(stderr,'(a)') "              mask:    "//compact_selection( mask   , size(geom%atm) )
          write(stderr,'(a)') "              orphans: "//compact_selection( orphan , size(geom%atm) )
          write(stderr,'(a)') "              widows:  "//compact_selection( widow  , size(geom%atm) )
          write(stderr,'(a)') "[rewrite]     Assimilate structures..."
          call chk_tagmask( atm=geom%atm , stat=stat )
          if( stat /= 0 ) return
          call sort_by_tag( geom%atm )



        case( 'D' )  ! delete atoms  -------------------------------------------
          write(stderr,'(a)') "[rewrite]     Delete selected atoms"
          call add_and_delete_atoms( geom , oselec=selec )

        case( 'E' )  ! set element for selection  ------------------------------
          k = k+1
          elem = trim(qarg(k))
          e    = atomic_number( elem )
          write(stderr,'(a)') "[rewrite]     Transform selected atoms to '"//element_symbol(e)//"'"
          
          do j = 1,size(selec)
            geom%atm( selec(j) )%e = e
          enddo
        
        case( 'F' )  ! release/fix atoms (release all, then fix selection) -----
          write(stderr,'(a)') "[rewrite]     Reset constraints. Only constrain selected atoms"

          if( size(selec) == 0 ) then
            geom%lc = .FALSE.
          else
            geom%lc = .TRUE.
            do j = 1,size(geom%atm)
              geom%atm( j )%c = .TRUE.
            enddo
            do j = 1,size(selec)
              geom%atm( selec(j) )%c = .FALSE.
            enddo
          end if
          
        case( 'M' )  ! mirror unconstraint part of system  ---------------------
          write(stderr,'(a)') "[rewrite]     Mirror unconstraint atoms"
          k = k+1  ;  plane = keyplane( trim(qarg(k)) )
          if( plane == 0 ) then
            write(stderr,'(a)') "[rewrite]     (M) Error, Plane '"//trim(qarg(k))//"' not readable.  Use ab, bc or ca."
            stat = 1  ;  return
          end if

          k = k+1  ;  read(qarg(k),*,iostat=stat) shf
          if( stat /= 0 ) then
            write(stderr,'(a)') "[rewrite]     (M) Error, Shift '"//trim(qarg(k))//"' not readable.  Use float point variable."
            stat = 1  ;  return
          end if
          
          gref = geom
          call pbc_convert( geom , .true. )

          ! keep constraint atoms
          shf = 2.0d0*shf
          do j = 1,size(geom%atm)
            if( geom%atm(j)%c ) then
              geom%atm(j)%r(plane) = shf-geom%atm(j)%r(plane)
            end if
          end do
          
          call compare_pbc( geom , gref , mask , orphan , widow )
          write(stderr,'(a)') "              mask:    "//compact_selection( mask   , size(geom%atm) )
          write(stderr,'(a)') "              orphans: "//compact_selection( orphan , size(geom%atm) )
          write(stderr,'(a)') "              widows:  "//compact_selection( widow  , size(geom%atm) )
          write(stderr,'(a)') "[rewrite]     Assimilate structures..."
          call chk_tagmask( atm=geom%atm , stat=stat )
          if( stat /= 0 ) return
          call sort_by_tag( geom%atm )

        case( 'O' )  ! set output format  --------------------------------------
          k = k+1
          write(stderr,'(a)') "[rewrite]     Set output format to '"//trim(qarg(k))//"'. Filename updated..."
          call convert_frmt( outfile , name_to_frmt(trim(qarg(k))) ) 
        
        case( 'S' )  ! cut slab, parameters: plane, thickness, shift, vacuum ---
          ! S +<plane>+<thk>+<shf>+<vac>
          write(stderr,'(a)') "[rewrite]     Cutting surface slab..."
          k = k+1  ;  plane = keyplane( trim(qarg(k)) )
          if( plane == 0 ) then
            write(stderr,'(A)') "[rewrite]     (S) Error, Plane '"//trim(qarg(k))//"' not readable.  Use ab, bc or ca."
            stat = 1  ;  return
          end if

          k = k+1  ;  read(qarg(k),*,iostat=stat) thk
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (S) Error, Thickness '"//trim(qarg(k))//"' not readable.  Use float point variable."
            stat = 1  ;  return
          end if
          thk = thk / boxheight( geom%box , plane )

          k = k+1  ;  read(qarg(k),*,iostat=stat) shf
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (S) Error, Shift '"//trim(qarg(k))//"' not readable.  Use float point variable."
            stat = 1  ;  return
          end if
          shf = shf / boxheight( geom%box , plane )

          k = k+1  ;  read(qarg(k),*,iostat=stat) vac
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (S) Error, Vacuum '"//trim(qarg(k))//"' not readable.  Use float point variable."
            stat = 1  ;  return
          end if

          write(stderr,'(A)') "              Plane:     "//trim(qarg(k))
          write(stderr,'(A)') "              Thickness: "//trim(qarg(k))
          write(stderr,'(A)') "              Shift:     "//trim(qarg(k))
          write(stderr,'(A)') "              Vacuum:    "//trim(qarg(k))

          call make_slab( geom , plane , shf , shf+thk )
          call set_vacuum( geom , plane , vac , 'c' )
          call standardize_box( geom )

        case( 'T' )  ! cut sphere, parameters: center atom, radius  ------------

          write(stderr,'(a)') "[rewrite]     Cutting sphere..."
          k = k+1  ;  read(qarg(k),*,iostat=stat) fix
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (T) Error, cannot read atom index (= '"//trim(qarg(k))//"')!"
            return
          else if( fix > size(geom%atm) ) then
            write(stderr,'(A)') "[rewrite]     (T) Error, atom index out of range!"
            return
          end if

          k = k+1  ;  read(qarg(k),*,iostat=stat) thk
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (T) Error, Radius '"//trim(qarg(k))//"' not readable.  Use float point variable."
            stat = 1  ;  return
          end if
          
          write(stderr,'(A)') "              Origin:    "//print_floats( tvec )
          write(stderr,'(A)') "              Radius:    "//trim(qarg(k))

          geom_lr = .true.
          call pbc_convert( geom , .true. )
          tvec = geom%atm(fix)%r

          call make_sphere( geom , tvec , thk )
          call sort_by_distance( tvec , geom%atm )


        case( 'W' )  ! wrap geometry around atom  ------------------------------
          k = k+1
          write(stderr,'(a)') "[rewrite]     Wrap atoms into shifted cell boundaries. "// &
                                            "Cell centered around atom '"//trim(qarg(k))//"'"
          read(qarg(k),*,iostat=stat) fix
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     Error, Atom '"//trim(qarg(k))//"' faulty.  Use integer in range."
            stat = 1  ;  return
          end if
          fix = MODULO(fix-1,size(geom%atm))+1

          call pbc_convert( geom , .FALSE. )
          tvec = geom%atm(fix)%r
          call wrap_in_cell( geom , tvec - 0.5D0 )


        case( 'X' )  ! make supercell, parameters: a, b, c  --------------------

          write(stderr,'(a)') "[rewrite]     Make supercell..."
          k = k+1  ;  read(qarg(k),*,iostat=stat) tdiag(1)
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (X) Error, cannot interpret "//trim(qarg(k))// &
                                                 " as multiplier for lattice vector a"
            return
          end if
          k = k+1  ;  read(qarg(k),*,iostat=stat) tdiag(2)
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (X) Error, cannot interpret "//trim(qarg(k))// &
                                                 " as multiplier for lattice vector b"
            return
          end if
          k = k+1  ;  read(qarg(k),*,iostat=stat) tdiag(3)
          if( stat /= 0 ) then
            write(stderr,'(A)') "[rewrite]     (X) Error, cannot interpret "//trim(qarg(k))// &
                                                 " as multiplier for lattice vector c"
            return
          end if
          if( tdiag(1) <= 0 .and. tdiag(2) <= 0 .and. tdiag(3) <= 0 ) then
            write(stderr,'(A)') "[rewrite]     (X) Error, only positive values allowed"
            return
          end if

          write(stderr,'(A)') "              new value of a: "//print_human_float( tdiag(1)*sqrt(sum( geom%box(:,1)**2 )) )
          write(stderr,'(A)') "              new value of b: "//print_human_float( tdiag(2)*sqrt(sum( geom%box(:,2)**2 )) )
          write(stderr,'(A)') "              new value of c: "//print_human_float( tdiag(3)*sqrt(sum( geom%box(:,3)**2 )) )

          call make_supercell( geom , tdiag )


        case default ! error  --------------------------------------------------

          write(stderr,'(A)') "[rewrite]     Error, Unknown command '"//qcmd//"'. Stop."
          stat = 1
          return

      end select
    enddo

  end subroutine rewrite


  ! --------------------------------------------------------------------------------------
  subroutine adsorb()
  use types     , only: atom
  use meteorgy  , only: add_and_delete_atoms , adsorb_atoms , wrap_in_cell
  use algebra3d , only: rotmat3d
  use params    , only: pi => pi_
    
    integer , allocatable :: sads(:) , ssurf(:)  ! substitution 

    type(atom) , allocatable :: scratch(:)

    integer               :: catom , cmode
    
    real(dp)              :: angle , shift(3) , rotate(3,3) ! manual shift

    real(dp)              :: center(3)

    logical               :: lchain , lcoads , ldefmsg    ! mode

    ! -----------------------------------------------------------
    ldefmsg     = .FALSE.
    lchain      = .FALSE.
    lcoads      = .FALSE.
    angle       =  0.0D0
    shift       =  0.0D0
    center      =  0.0D0
    rotate(1,1) =  1.0D0  ;  rotate(1,2) = 0.0D0  ;  rotate(1,3) = 0.0D0
    rotate(2,1) =  0.0D0  ;  rotate(2,2) = 1.0D0  ;  rotate(2,3) = 0.0D0
    rotate(3,1) =  0.0D0  ;  rotate(3,2) = 0.0D0  ;  rotate(3,3) = 1.0D0

    ! -----------------------------------------------------------
    ! parse command line / read files
    ! -----------------------------------------------------------
    call setup()                                     ! print manual, count args, read query and surface
    if( stat /= 0 ) return

    if( nargs < exe + 5 .or. nargs > exe + 6 ) then
      write(stderr,'(A)') "[adsorb]      Error, wrong number of arguments. Try:"
      write(stderr,'(14X,A)') "$ adsorb <query>[:<args>]> <surf.in> <surf selection> <ads.in> <ads selection> [<output>]'"
      stat = 1
      return

    else
      call parse_selection( size(geom%atm) , get_arg( exe + 3 ) , ssurf , stat )
      if( stat /= 0 ) return

      reffile = init_file( get_arg( exe + 4 ) )      ! get reffile
      call any_read( reffile , gref )                ! read adsorbate
      stat  = reffile%stat
      if( stat /= 0 ) return

      call parse_selection( size(gref%atm) , get_arg( exe + 5 ) , sads , stat )
      if( stat /= 0 ) return

    end if

    ! (optional) get output file name
    if( nargs == exe + 6 ) then
      outfile = init_file( get_arg( exe + 5 ) )
      ! if desired format cannot be guessed, use input format. Adjust filename accordingly
      if( outfile%stat /= 0 )  call convert_frmt( outfile , infile%frmt )
      
    end if

    call pbc_convert( geom , .TRUE. )              ! 1. convert to cartesian
    call pbc_convert( gref , .TRUE. )

    ! -----------------------------------------------------------
    ! parse query
    ! -----------------------------------------------------------

    ! check number of query arguments qarg
    k = 0
    do i=1,size(qcmd)
      select case( qcmd(i) )
        case( 'c' ) ; k = k+1
        case( 'o' ) ; k = k+1
        case( 's' ) ; k = k+3
        case( 'x' ) ; k = k+1
        case( 'y' ) ; k = k+1
        case( 'z' ) ; k = k+1
      end select
    enddo

    if( k /= size(qarg) ) then
      write(stderr,'(A,I2,A,I2,A)') "[adsorb]      Error, Wrong number of arguments for query commands '"//qcmd(:)// &
                                               "'. Needed:",k,", found:",size(qarg),"!"
      stat = -1
      return
    end if

    ! loop through query, run commands
    k = 0
    do i=1,size(qcmd)
      select case( qcmd(i) )
        case( '1' )  ! single bond mode ----------------------------------------
          lchain     = .TRUE.
          ldefmsg    = .TRUE.
          write(stderr,'(A)') "[adsorb]      Run in single bond mode..."

        case( 'b' )
          lchain     = .TRUE.
          ldefmsg    = .TRUE.
          write(stderr,'(A)') "[adsorb]      Run in single bond mode..."

        case( 'c' )  ! coadsorption mode ---------------------------------------
          k = k+1
          lcoads     = .TRUE.
          ldefmsg    = .TRUE.
          read(qarg(k),*,iostat=stat) cmode
          if( stat /= 0 ) then
            write(stderr,'(A)') "[adsorb]      Error, could not read coadsorption mode! Try again..."
            return
          else if( SUM( (geom%box(:,1)-gref%box(:,1))**2 ) > 1.0D-3 .or. &
                   SUM( (geom%box(:,2)-gref%box(:,2))**2 ) > 1.0D-3 .or. &
                   SUM( (geom%box(:,3)-gref%box(:,3))**2 ) > 1.0D-3 ) then
            write(stderr,'(A)') "[adsorb]      Warning, incompatible cells... results might be nonsense..."
          end if

        case( 'd' )  ! dentation mode ------------------------------------------
          ldefmsg    = .TRUE.
          write(stderr,'(A)') "[adsorb]      Run in dentation mode..."

        case( 'o' )  ! center-atom of cells for coadsorption -------------------
          k = k+1
          read(qarg(k),*,iostat=stat) catom
          if( stat /= 0 ) then
            write(stderr,'(A)') "[adsorb]      Warning, could not read atom number! Ignore center option..."
          else
            catom  =  MODULO(catom-1,size(geom%atm))+1
            write(stderr,'(A,I4,A)') "[adsorb]      Center around atom",catom,"..."
          end if

        case( 'r' )  ! apply rotation on shift ---------------------------------
          shift = MATMUL( rotate , shift )
          write(stderr,'(A,3F6.1,A)') "[adsorb]      New adsorbate shift: ",shift," ..."

        case( 's' ) ! set shift ------------------------------------------------
          k = k+1  ;  read(qarg(k),*,iostat=stat) shift(1)
          if( stat /= 0 .and. len(trim(qarg(k))) /= 0 )  &
              write(stderr,'(A)') "[adsorb]      Warning, could not read x-shift! Set to zero..."

          k = k+1  ;  read(qarg(k),*,iostat=stat) shift(2)
          if( stat /= 0 .and. len(trim(qarg(k))) /= 0 )  &
              write(stderr,'(A)') "[adsorb]      Warning, could not read y-shift! Set to zero..."

          k = k+1  ;  read(qarg(k),*,iostat=stat) shift(3)
          if( stat /= 0 .and. len(trim(qarg(k))) /= 0 )  &
              write(stderr,'(A)') "[adsorb]      Warning, could not read z-shift! Set to zero..."

          write(stderr,'(A,3F6.1,A)') "[adsorb]      Adsorbate shift: ",shift," ..."

          stat = 0

        case( 'x' )  ! set up / extend rotation matrix for rotation around x ---
          k = k+1
          read(qarg(k),*,iostat=stat) angle
          if( stat == 0 ) then
            write(stderr,'(A,F6.1,A)') "[adsorb]      Rotate along x-axis by ",angle," degree..."
            rotate = MATMUL( rotmat3d( (/1.0D0,0.0D0,0.0D0/) , angle/180.0D0*pi ) , rotate )
          else
            write(stderr,'(A)')        "[adsorb]      Error, could not read angle! Ignore..."
          end if

        case( 'y' )  ! set up / extend rotation matrix for rotation around y ---
          k = k+1
          read(qarg(k),*,iostat=stat) angle
          if( stat == 0 ) then
            write(stderr,'(A,F6.1,A)') "[adsorb]      Rotate along y-axis by ",angle," degree..."
            rotate = MATMUL( rotmat3d( (/0.0D0,1.0D0,0.0D0/) , angle/180.0D0*pi ) , rotate )
          else
            write(stderr,'(A)')        "[adsorb]      Error, could not read angle! Ignore..."
          end if

        case( 'z' )  ! set up / extend rotation matrix for rotation around z ---
          k = k+1
          read(qarg(k),*,iostat=stat) angle
          if( stat == 0 ) then
            write(stderr,'(A,F6.1,A)') "[adsorb]      Rotate along z-axis by ",angle," degree..."
            rotate = MATMUL( rotmat3d( (/0.0D0,0.0D0,1.0D0/) , angle/180.0D0*pi ) , rotate )
          else
            write(stderr,'(A)')        "[adsorb]      Error, could not read angle! Ignore..."
          end if

        case default ;  write(stderr,'(A)') "[adsorb]      Error, Unknown command '"//qcmd//"'. Stop." ; return
      end select
    enddo
    ! ----------------------------------------------------------------
    ! always: make cartesian , shift atoms, merge/substitute atoms
    ! conditionals: set shift/rotation matrix
    ! ----------------------------------------------------------------
    if( .not. ldefmsg  ) then
      write(stderr,'(A)') "[adsorb]      No mode set. Running with default settings (dentation/comparison mode)..."
    end if

    if( lcoads ) then
      call pbc_convert( geom , .FALSE. )              ! 1. convert to fractionals
      call pbc_convert( gref , .FALSE. )              !
      center = geom%atm(catom)%r
      call wrap_in_cell( geom , center - 0.5D0 )

      if( cmode == 2 ) then
                                                      ! 2. add atoms
        call wrap_in_cell( gref , center + (/ +0.5D0 , -0.5D0 , -0.5D0 /) )
        call add_and_delete_atoms( geom , oatm=gref%atm )

        call pbc_convert( geom , .TRUE. )             ! 3. convert to cartesian (use old box params!)
        gref%box(:,1)  =  geom%box(:,1)                ! 4. convert box
        gref%box(:,2)  =  geom%box(:,2)
        geom%box(:,1)  =  gref%box(:,2) + gref%box(:,1)
        geom%box(:,2)  =  gref%box(:,2) - gref%box(:,1)
         
        call wrap_in_cell( geom )                     ! 5. (aesthetical) wrap back in cell

      else if( cmode == 3 ) then
                                                      ! 2. add atoms
        call wrap_in_cell( gref , center + (/ -0.5D0 , +0.5D0 , -0.5D0 /) )
        call add_and_delete_atoms( geom , oatm=gref%atm )

        call wrap_in_cell( gref , center + (/ +0.5D0 , +0.5D0 , -0.5D0 /) )
        call add_and_delete_atoms( geom , oatm=gref%atm )

        call pbc_convert( geom , .TRUE. )             ! 3. convert to cartesian (use old box params!)
        gref%box(:,1) =  geom%box(:,1)                ! 4. convert box
        gref%box(:,2) =  geom%box(:,2)
        geom%box(:,1) =  gref%box(:,2) + 2 * gref%box(:,1)
        geom%box(:,2) =  gref%box(:,2) -     gref%box(:,1)
         
        call wrap_in_cell( geom )                     ! 5. (aesthetical) wrap back in cell

      else
        if( cmode /= 4 ) write(stderr,'(A)') "[adsorb]      Coadsorption mode not available, default to 4."
                                                      ! 2. add atoms
        call wrap_in_cell( gref , center + (/ +0.5D0 , -0.5D0 , -0.5D0 /) )
        call add_and_delete_atoms( geom , oatm=gref%atm )

        call wrap_in_cell( gref , center + (/ -0.5D0 , +0.5D0 , -0.5D0 /) )
        call add_and_delete_atoms( geom , oatm=gref%atm )

        call wrap_in_cell( gref , center + (/ +0.5D0 , +0.5D0 , -0.5D0 /) )
        call add_and_delete_atoms( geom , oatm=gref%atm )

        call pbc_convert( geom , .TRUE. )             ! 3. convert to cartesian (use old box params!)
        geom%box(:,1) = 2 * geom%box(:,1)             ! 4. convert box
        geom%box(:,2) = 2 * geom%box(:,2)
        call wrap_in_cell( geom )                     ! 5. (aesthetical) wrap back in cell
      end if
      write(stderr,'(A,I2,A)') "[adsorb]      Coadsorption mode",cmode,"..."

      
    else
      ! rotate
      do i = 1,size(gref%atm)
        gref%atm(i)%r = MATMUL( rotate , gref%atm(i)%r )
      enddo

      call adsorb_atoms( geom , gref%atm , sads , ssurf , lchain , shift )
    end if

  end subroutine adsorb


  ! --------------------------------------------------------------------------------------
  subroutine geominfo()
  use algebra3d , only:  matinv3d , calc_gval , value3d => value
  use meteorgy  , only:  center_xyz , wrap_in_cell
  use parse     , only:  add_to_selection , mkrange
    
    character(len=:)  ,  allocatable :: tmp
    real(dp)          ,  allocatable :: vec(:,:)
    real(dp) :: result
    integer :: fix,s,t,u


    ! -----------------------------------------------------------
    ! parse command line / read files
    ! -----------------------------------------------------------

    call setup()

    !initialize
    center = 0.0D0
    verbose = .FALSE.
    ALLOCATE( vectors(20) ) ! dynamically increased if necessary
    printline = ''

    call pbc_convert( geom , .TRUE. )

    ! -----------------------------------------------------------
    ! parse query
    ! -----------------------------------------------------------
    ! check number of query arguments qarg
    k = 0
    do i=1,size(qcmd)
      select case( qcmd(i) )
        ! Define center:
        case( '0' ) ; k = k+1   ! center of mass from new selec
        case( '1' ) ; k = k+0   ! center of mass from selec
        case( '2' ) ; k = k+3   ! center as new vector
        case( '3' ) ; k = k+1   ! center as vector

        ! Effect selec:
        case( '4' ) ; k = k+1   ! explicit choice
        case( 'e' ) ; k = k+1   !? set element filter
        case( 'x' ) ; k = k+1   !? #atm atoms with smallest x component
        case( 'y' ) ; k = k+1   !? #atm atoms with smallest y component
        case( 'z' ) ; k = k+1   !? #atm atoms with largest z component
        case( 'r' ) ; k = k+1   !? #atm atoms in closest range to atom
        case( 'R' ) ; k = k+1   !? all atoms within radius to atom

        ! Results from selec:
        case( 'p' ) ; k = k+0   !? selection itself
        case( 'g' ) ; k = k+1   ! 'g value'
        case( 'X' ) ; k = k+1   !? write xyz-file from selection

        ! Effect vectors (all derived from selec, or explicit definition)
        case( 'V' ) ; k = k+3   ! explicit definition
        case( 'E' ) ; k = k+1   !? by element (filter)
        case( 'c' ) ; k = k+1   ! copy (selec)
        case( 'C' ) ; k = k+1   ! center of mass (selec)
        case( 'o' ) ; k = k+2   ! shifted origin by vector (new from selec)
        case( 'O' ) ; k = k+2   ! shifted origin by vector (modify vectors)
        
        ! Results from vectors:
        case( 'B' ) ; k = k+1   ! 'g value'
        case( 'G' ) ; k = k+1   ! 'g value'
        case( 'd' ) ; k = k+1   !? average distance
        case( 's' ) ; k = k+1   !? shortest distance
        case( 'l' ) ; k = k+1   !? longest distance
        case( 'a' ) ; k = k+1   !? average angle, first vector to following
        case( 'A' ) ; k = k+1   !? average angle pairwise

        case( 'v' ) ; k = k+0   ! verbose mode
      end select
    enddo
!


    if( k /= size(qarg) ) then
      write(stderr,'(A,I2,A,I2,A)',advance='no') "[geominfo]    Error, Wrong number of arguments for query commands '"
      do i=1,size(qcmd)
        write(stderr,'(A)'    ,    advance='no')  qcmd(i)
      enddo
      write(stderr,'(A,I2,A,I2,A)')              "'. Needed:",k,", found:",size(qarg),"!"
      stat = -1  ;  return
    end if

    ! loop through query, run commands
    k = 0
    l = 0
    do i=1,size(qcmd)
      select case( qcmd(i) )

        ! ----------------------------------------------------------------------
        ! Define center:
        case( '0' )   ! center of mass from new selec --------------------------
          k = k+1
          call parse_selection( size(geom%atm) , trim(qarg(k)) , selec , stat )

          ! set center
          center = center_xyz( geom%atm , selec )

          ! add to vectors
          l = l+1
          vectors(l)%l = .TRUE.
          vectors(l)%t = 0
          vectors(l)%r = center

          ! wrap geometry
          call pbc_convert( geom , .FALSE. )
          call wrap_in_cell( geom , MATMUL(matinv3d(geom%box),center) - 0.5 )
          call pbc_convert( geom , .TRUE. )
            
          if(verbose) then ! -------------------------------
            write(stderr,'(A)') "[geominfo]    Select new atoms by index and wrap atoms in unit cell around center of mass"
            write(stderr,'(A)') "              Atoms: "//print_integers(selec)
            write(stderr,'( )')
            call vector_print( vectors , selection=(/ l /) , center=center , truefalse=' B' )
            write(stderr,'( )')
          end if 

        case( '1' )   ! center of mass from selec ------------------------------
          center = center_xyz( geom%atm , selec )

          ! add to vectors
          l = l+1
          vectors(l)%l = .TRUE.
          vectors(l)%t = 0
          vectors(l)%r = center

          call pbc_convert( geom , .FALSE. )
          call wrap_in_cell( geom , MATMUL(matinv3d(geom%box),center) - 0.5 )
          call pbc_convert( geom , .TRUE. )

          if(verbose) then ! -------------------------------
            write(stderr,'(A)') "[geominfo]    Wrap all unit cell around center of mass of current selection"
            write(stderr,'(A)') "              Atoms: "//print_integers(selec)
            write(stderr,'( )')
            call vector_print( vectors , selection=(/ l /) , center=center , truefalse=' B' )
            write(stderr,'( )')
          end if 

        case( '2' )   ! center as new vector -----------------------------------
        case( '3' )   ! center as vector ---------------------------------------

          k = k+1
          read(qarg(k),*,iostat=stat) s
          
          if( stat /= 0 .or. s > size(selec) .or. s <= 0 ) then
            write(stderr,'(A)') "[geominfo]    Error in selected vector for center. Keep old center."
            cycle
          end if

          ! set center
          center = vectors(s)%r

          call pbc_convert( geom , .FALSE. )
          call wrap_in_cell( geom , MATMUL(matinv3d(geom%box),center) - 0.5 )
          call pbc_convert( geom , .TRUE. )

          if(verbose) then ! -------------------------------
            write(stderr,'(A)') "[geominfo]    Pick vector as new center"
            write(stderr,'(A)') "              Vector: "//print_integers( (/s/) )
            write(stderr,'( )')
            call vector_print( vectors , selection=(/ s /) , center=center , truefalse=' B' )
            write(stderr,'( )')
          end if 

        ! ----------------------------------------------------------------------
        ! Effect selec:
        case( '4' )   ! explicit choice ----------------------------------------
          k = k+1
          selec = add_to_selection( trim(qarg(k)) , selec , size(geom%atm) )

          if(verbose) then
            write(stderr,'(A)') "[geominfo]    Added atoms to selection by index."
            write(stderr,'(A)') "              All selected atoms: "//print_integers(selec)

            write(stderr,'( )')
            call atom_print( geom%atm , selection=selec , lcartesian=.TRUE. )
            write(stderr,'( )')
          end if 

        case( 'e' )   ! set element filter -------------------------------------
        case( 'x' )   ! #atm atoms with smallest x component -------------------
        case( 'y' )   ! #atm atoms with smallest y component -------------------
        case( 'z' )   ! #atm atoms with largest z component --------------------
        case( 'r' )   ! #atm atoms in closest range to atom --------------------
        case( 'R' )   ! all atoms within radius to atom ------------------------

        ! ----------------------------------------------------------------------
        ! Results from selec:
        case( 'p' )   ! selection itself ---------------------------------------
        case( 'g' )   ! 'g value' ----------------------------------------------
          k = k+1
          call calculate_g_value( 'r' )

        case( 'X' )   ! write xyz-file from selection --------------------------

        ! ----------------------------------------------------------------------
        ! Effect vectors (all derived from selec, or explicit definition) ------
        case( 'V' )   ! explicit definition ------------------------------------
        case( 'E' )   ! by element (filter) ------------------------------------
        case( 'c' )   ! copy (selec) -------------------------------------------
          k = k+1
          call parse_selection( size(selec) , trim(qarg(k)) , subset , stat )
          call check_vector_allocation( vectors , l+size(subset) )

          do j = 1,size(subset)
            s = selec(subset(j))
            vectors(l+j)%l = .TRUE.
            vectors(l+j)%t = s
            vectors(l+j)%r = geom%atm(s)%r
          enddo

          l = l+size(subset)

          if(verbose) then ! -------------------------------
            write(stderr,'(A)') "[geominfo]    Add subset of selected atom coordinates to vectors."
            write(stderr,'(A)') "              Selection: "//print_integers(selection_subset( selec , subset ))
            
            write(stderr,'( )')
            call vector_print( vectors , selection=mkrange(1,l) , center=center , truefalse=' B' )
            write(stderr,'( )')
          end if

        case( 'C' )   ! center of mass (selec) ---------------------------------
          k = k+1
          call parse_selection( size(selec) , trim(qarg(k)) , subset , stat )
          call check_vector_allocation( vectors , l+1 )
          subset = selection_subset( selec , subset )
          
          l = l+1

          vectors(l)%l = .TRUE.
          vectors(l)%t = 0
          vectors(l)%r = center_xyz( geom%atm , subset )


          if(verbose) then ! -------------------------------
            write(stderr,'(A)') "[geominfo]    Add center of mass of selected atoms to vectors."
            write(stderr,'(A)') "              Selection: "//print_integers( subset )
            
            write(stderr,'( )')
            call vector_print( vectors , selection=mkrange(1,l) , center=center , truefalse=' B' )
            write(stderr,'( )')
          end if

        case( 'o' )   ! shifted origin by vector (new from selec) --------------
          k = k+1
          call parse_selection( l , trim(qarg(k)) , subset , stat )
          if( size(subset) >= 1 ) then
            if( size(subset) > 1 ) &
                write(stderr,'(A)') "[geominfo]    " // &
                    "Warning  More than one vector selected, will only use first selected for shifting..."
            s = subset(1)
          else
            write(stderr,'(A)') "[geominfo]    Warning  No vector selected, will only use first for shifting..."
            s = 1
          end if
          
          k = k+1
          call parse_selection( size(selec) , trim(qarg(k)) , subset , stat )
          call check_vector_allocation( vectors , l+size(subset) )
          subset = selection_subset( selec , subset )
          
          do j = 1,size(subset)
            t = subset(j)
            vectors(l+j)%l = .FALSE.
            vectors(l+j)%t = t
            vectors(l+j)%r = geom%atm(t)%r - vectors(s)%r
          enddo

          l = l+size(subset)

          if(verbose) then ! -------------------------------
            write(stderr,'(A,I3,A)') "[geominfo]    Add selected atoms to vectors (shifted by vector",s,"."
            write(stderr,'(A)') "              Selection: "//print_integers( subset )
            
            write(stderr,'( )')
            call vector_print( vectors , selection=mkrange(1,l) , center=center , truefalse=' B' )
            write(stderr,'( )')
          end if

        case( 'O' )   ! shifted origin by vector (modify vectors) --------------
          k = k+1
          call parse_selection( l , trim(qarg(k)) , subset , stat )
          if( size(subset) >= 1 ) then
            if( size(subset) > 1 ) &
                write(stderr,'(A)') "[geominfo]    " // &
                    "Warning  More than one vector selected, will only use first selected for shifting..."
            s = subset(1)
          else
            write(stderr,'(A)') "[geominfo]    Warning  No vector selected, will only use first for shifting..."
            s = 1
          end if
          
          k = k+1
          call parse_selection( l , trim(qarg(k)) , subset , stat )
          
          do j = 1,size(subset)
            t = subset(j)
            vectors(t)%l = .FALSE.
            vectors(t)%r = vectors(t)%r - vectors(s)%r
          enddo

          if(verbose) then ! -------------------------------
            write(stderr,'(A,I3,A)') "[geominfo]    Shift selected vectors along vector",s,"."
            write(stderr,'(A)') "              Selection: "//print_integers( subset )
            
            write(stderr,'( )')
            call vector_print( vectors , selection=mkrange(1,l) , center=center , truefalse=' B' )
            write(stderr,'( )')
          end if

        
        ! ----------------------------------------------------------------------
        ! Results from vectors:
        case( 'B' )   ! 'g value' ----------------------------------------------
          k = k+1
          call calculate_g_value( 'b' )

        case( 'G' )   ! 'g value' ----------------------------------------------
          k = k+1
          call calculate_g_value( 'v' )

        case( 'd' )   ! average distance ---------------------------------------
          k = k+1
          call parse_selection( l , trim(qarg(k)) , subset , stat )
          
          result = 0.0D0
          do j = 1,size(subset)
            s = subset(j)
            if( vectors(s)%l ) write(stderr,'(A)') "[geominfo]    Warning, vector",s," seems to be a coordinate!"
            result = result + value3d( vectors(s)%r )
          enddo
          result = result / size(subset)

          if(verbose) then ! -------------------------------
            write(stderr,'(A)') "[geominfo]    Add center of mass of selected atoms to vectors."
            write(stderr,'(A)') "              Selection: "//print_integers( subset )
            
            write(stderr,'( )')
            call vector_print( vectors , selection=mkrange(1,l) , center=center , truefalse=' B' )
            write(stderr,'( )')
          end if

        case( 's' )   ! shortest distance --------------------------------------
        case( 'l' )   ! longest distance ---------------------------------------
        case( 'a' )   ! average angle, first vector to following ---------------
        case( 'A' )   ! average angle pairwise ---------------------------------

        case( 'v' ) ; k = k+0   ! verbose mode ---------------------------------
          write(stderr,'(A)') "[geominfo]    Switch on verbosity. See all I do..."
          verbose = .TRUE.

        case default  ! --------------------------------------------------------
          write(stderr,'(A)') "[geominfo]    Error, Unknown command '"//qcmd(i)//"' ..."

      end select
    enddo

    write(stdout,'(A)') printline
  end subroutine geominfo
  ! ---------------------------------------------------------------------------------- end


  ! --------------------------------------------------------------------------------------
  subroutine check_vector_allocation( vectors , newsize )

    ! check size and reallocate is necessary (doubling size to keep reallocation rare)
    type(vector) , INTENT(INOUT) ,  allocatable :: vectors(:)
    type(vector)                 ,  allocatable :: ivectors(:)
    integer      , INTENT(IN)    :: newsize
    integer                      :: oldsize

    oldsize = size(vectors)

    if( oldsize < newsize ) then

      ! back up
      ALLOCATE( ivectors( oldsize ) )
      ivectors(:) = vectors(:)

      ! reallocate
      DEALLOCATE( vectors )
      ALLOCATE( vectors( 2 * newsize ) )

      ! play back
      vectors(:oldsize) = ivectors(:)

      ! initialize rest by false and zero
      do i = oldsize+1,newsize
        vectors(i)%l  = .FALSE.
        vectors(i)%t  = 0
        vectors(i)%r  = (/ 0.0D0 , 0.0D0 , 0.0D0 /)
      enddo
    end if

  end subroutine check_vector_allocation
  ! ---------------------------------------------------------------------------------- end


  ! --------------------------------------------------------------------------------------
  function selection_subset( selection , subset ) RESULT( sel_sub )
    ! deconvolute selections and subsets...
    integer , INTENT(IN) :: selection(:) , subset(:)
    integer :: sel_sub( size(subset) ) , i
    do i = 1,size(subset)
      sel_sub(i) = selection(subset(i))
    enddo
  end function selection_subset
  ! ---------------------------------------------------------------------------------- end


  ! --------------------------------------------------------------------------------------
  subroutine calculate_g_value( inputmode )
  use algebra3d , only:  matinv3d , calc_gval
    character(len=1)  , INTENT(IN)  :: inputmode
    character(len=:)  , allocatable :: tmp
    real(dp)          , allocatable :: vec(:,:) , gval
    logical                         :: lusesel , lassumecoord
    integer                         :: s,t,u

    select case( inputmode )
      case( 'r' )  ; lusesel = .TRUE.  ; lassumecoord = .TRUE.
      case( 'v' )  ; lusesel = .FALSE. ; lassumecoord = .TRUE.
      case default ; lusesel = .FALSE. ; lassumecoord = .FALSE.
    end select
    
    call parse_selection( size(selec) , trim(qarg(k)) , subset , stat )
    
    s = size(subset)
    ! set up input for gval
    ALLOCATE( vec(3,s-1) )

    if( lassumecoord ) then
      if( s < 2 .or. s > 4 ) write(stderr,'(A)') "[geominfo]    Error - "// &
           "need two, three or four coordinates for distance, angles or dihedrals"
      if( lusesel )  subset = selection_subset( selec , subset )

      do j = 1,s-1
        t        = subset( j )
        u        = subset(j+1)
        if( lusesel ) then
          vec(:,j) = geom%atm(u)%r - geom%atm(t)%r
        else
          vec(:,j) = vectors(u)%r - vectors(t)%r
        end if
      enddo

    else
      if( s < 1 .or. s > 3 ) write(stderr,'(A)') "[geominfo]    Error - "// &
           "need one, two or three coordinates for distance, angles or dihedrals"

      do j = 1,s
        t        = subset(j)
        vec(:,j) = vectors(t)%r
      enddo

    end if

    ! calculate
    gval  =  calc_gval( vec )

    ! output
    ALLOCATE( character(len=14) :: tmp )
    write(tmp,'(1X,F13.8)') gval
    printline = printline//tmp 
    
    ! verbosity
    if(verbose) then   ! -----------------------------------
      ! description
      write(stderr,'(A)',ADVANCE='no') "[geominfo]    Between "

      if( lusesel )           then ;  write(stderr,'(A)',ADVANCE='no') "selected atoms"
      else if( lassumecoord ) then ;  write(stderr,'(A)',ADVANCE='no') "coordinates in vectors"
      else                         ;  write(stderr,'(A)',ADVANCE='no') "vectors"                ; end if

      write(stderr,'(A)') ":  distance, angle or dihedral ..."

      ! list selection
      if( lusesel ) then ; write(stderr,'(A)') "              Atoms: "//print_integers( subset )
      else               ; write(stderr,'(A)') "              Vectors: "//print_integers( subset )
      end if

      ! result
      select case( size(vec,2) )
        case(1); write(stderr,'(A,F10.5)') "              Distance: " , gval
        case(2); write(stderr,'(A,F10.5)') "              Angle: "    , gval
        case(3); write(stderr,'(A,F10.5)') "              Dihedral: " , gval
      end select

      ! print table
      write(stderr,'( )')
      if( lusesel ) then
        call atom_print( geom%atm , selection=subset , lcartesian=.TRUE. )
      else
        call vector_print( vectors , selection=subset , center=center , truefalse=' B' )
      end if
      write(stderr,'( )')
    end if
    
    ! clean up
    DEALLOCATE( vec , tmp )

  end subroutine calculate_g_value
  ! ---------------------------------------------------------------------------------- end
  
  
!  ! --------------------------------------------------------------------------------------
!  subroutine calculate_average( inputmode )
!  use algebra3d , only:  matinv3d , calc_gval
!    character(len=1)  , INTENT(IN)  :: inputmode
!    character(len=:)  , allocatable :: tmp
!    real(dp)          , allocatable :: average , minimum , maximum , angle1 , anglep
!    integer                         :: s,t,u
!    call parse_selection( l , trim(qarg(k)) , subset , stat )
!    
!    result = 0.0D0
!    do j = 1,size(subset)
!      s = subset(j)
!      if( vectors(s)%t ) write(stderr,'(A)') "[geominfo]    Warning, vector",s," seems to be a coordinate!"
!      result = result + value3d( vectors(s) )
!    enddo
!    result = result / size(subset)
!
!    if(verbose) then ! -------------------------------
!      write(stderr,'(A)') "[geominfo]    Add center of mass of selected atoms to vectors."
!      write(stderr,'(A)') "              Selection: "//print_integers( subset )
!      
!      write(stderr,'( )')
!      call vector_print( vectors , selection=mkrange(1,l) , center=center , truefalse=' B' )
!      write(stderr,'( )')
!    end if
!  end subroutine calculate_average


  ! ----------------------------------------------------------------------------
  subroutine manual( mode )

    character(len=*) , INTENT(IN) :: mode
    integer                       :: o

    o = stdout

    select case( mode )

      case( 'rewrite' )

 write(o,'( )')
 write(o,'(A)') "  Usage:  "//execute//" <query>[:<args>] <input file> [<output file>]"
 write(o,'( )')
 write(o,'(A)') "A query is a sequence of single  characters commands.  Advanced commands require"
 write(o,'(A)') "additional arguments added  separated by colons  after the query.  Arguments are"
 write(o,'(A)') "parsed in the order of their occurence. It is always started by a '%'. The given"
 write(o,'(A)') "file name extension determines the output format."
 write(o,'( )')
 write(o,'(A)') "  Commands:"
 write(o,'( )')
 write(o,'(A)') "   a, b, c                -  sort atoms along lattice vectors. Return fractional coordinates."
 write(o,'(A)') "   d                      -  convert to fractional (direct) coordinates."
 write(o,'(A)') "   e                      -  sort atoms by elements (increasing order)."
 write(o,'(A)') "   i                      -  invert order"
 write(o,'(A)') "   l + <num>              -  make selection from last num atoms."
 write(o,'(A)') "   o + <atom>             -  sort by distance to atom"
 write(o,'(A)') "   r                      -  convert to cartesian coordinates."
 write(o,'(A)') "   s + <selection>        -  set selection."
 write(o,'(A)') "   w                      -  wrap atoms in cell boundaries."
 write(o,'(A)') "   x, y, z                -  sort atoms along cartesian vectors. Return cartesian coordinates."
 write(o,'(A)') "   C + <reference>        -  compare input system with reference and arrange atoms similarly."
 write(o,'(A)') "   D                      -  delete selected atoms."
 write(o,'(A)') "   E + <symbol>           -  set element for selection."
 write(o,'(A)') "   F                      -  fix/set constraint for selection."
 write(o,'(A)') "   M + <shift>            -  mirror unconstraint part of the system."
 write(o,'(A)') "   O + <format>           -  set output format, supported are vasp, dftb and xyz."
 write(o,'(A)') "   S +<pl>+<th>+<sh>+<va> -  make surface slab. [pl]ane (=ab/bc/ca), [th]ickness, [sh]ift, [va]cuum."
 write(o,'(A)') "   T + <atom> + <r>       -  make spheric cluster centered around atom with radius r."
 write(o,'(A)') "   W + <atom>             -  wrap atoms in shifted cell boundaries, centered around atom."
 write(o,'(A)') "   X + <a> + <b> + <c>    -  make simple supercell by multiplying lattice vectors."
 write(o,'( )')
 write(o,'(A)') "  Examples:"
 write(o,'( )')
 write(o,'(A)') "            $ rewrite %ber CONTCAR"
 write(o,'( )')
 write(o,'(A)') "    Sort CONTCAR along b axis, then by elements, convert to cartesian and"
 write(o,'(A)') "    write new geometry to POSCAR.out."
 write(o,'( )')
 write(o,'(A)') "            $ rewrite %lzFe:70 CONTCAR"
 write(o,'( )')
 write(o,'(A)') "    Sort atoms in CONTCAR along z axis, fix 70 last atoms, sort by elements."
 write(o,'( )')

      case( 'adsorb' )

 write(o,'( )')
 write(o,'(A)') "  Usage:  "//execute//" <query>[:<args>]> <surf.in> <surf selection> <ads.in> <ads selection> [<output>]"
 write(o,'( )')
 write(o,'(A)') "Adsorption requires  adsorbate and surface geometries,  as well as sets of atoms"
 write(o,'(A)') "to place the  adsorbate in  a well defined way.  Geometries can be  given in any"
 write(o,'(A)') "supported  file  format.  Selections  are used for  substitutions in  both input"
 write(o,'(A)') "geometries,  the comparison  is done atom  by atom  in the order  of each set of"
 write(o,'(A)') "selected atoms. Two modes are available, comparison (default) and one bond mode."
 write(o,'(A)') "In comparsion mode, the adsorbate is  aligned to match the first three atoms and"
 write(o,'(A)') "shifted to the  center of mass of  the selected  atoms.  All selected  atoms are"
 write(o,'(A)') "substituted by averaged positions. In one bond mode, the first, second and third"
 write(o,'(A)') "atom define positioning, angle and dihedral on the surface."
 write(o,'( )')
 write(o,'(A)') "A query is a sequence of single  characters commands.  Advanced commands require"
 write(o,'(A)') "additional arguments added  separated by colons  after the query.  Arguments are"
 write(o,'(A)') "parsed in the order of their occurence. It is always started by a '%'."
 write(o,'( )')
 write(o,'( )')
 write(o,'(A)') "  Commands:"
 write(o,'( )')
 write(o,'(A)') "   1 , b           -  adsorb using one bond, see below"
 write(o,'(A)') "   c               -  coadsorb, see below"
 write(o,'(A)') "   d               -  adsorb as dentate, see below"
 write(o,'(A)') "   r               -  rotate shift vector following x, y, z"
 write(o,'(A)') "   s + <shift>     -  set shift vector of adsorbate"
 write(o,'(A)') "   x,y,z + <angle> -  rotate adsorbate along cartesian axis before adsorption"
 write(o,'( )')
 write(o,'( )')
 write(o,'(A)') "  One bond:"
 write(o,'( )')
 write(o,'(A)') "Bind adsorbate to surface atom  substitution.i  Specify up to three common atoms"
 write(o,'(A)') "to define position and orientation. First atom pair, A/X, sets the position, the"
 write(o,'(A)') "second, B/Y,  aligns the adsorbate along the X-Y bond,  and the last, C/Z aligns"
 write(o,'(A)') "the dihedral angle between adsorbate and surface."
 write(o,'( )')
 write(o,'(A)') "Substitution for up to three given adsorbate structures is by followin scheme:"
 write(o,'( )')
 write(o,'(A)') "    ads-A    +    X-surf     -->     ads-X-surf"
 write(o,'(A)') "  ads-B-A    +  Y-X-surf     -->   ads-B-X-surf"
 write(o,'(A)') "  ads-B-A-C  +  Y-X-Z-surf   -->   ads-B-X-Z-surf"
 write(o,'( )')
 write(o,'(A)') "A is always substituted by X, Y by C (bond distance of A-B, direction of X-Y)"
 write(o,'( )')
 write(o,'( )')
 write(o,'(A)') "  Dentate adsorbate:"
 write(o,'( )')
 write(o,'(A)') "Arrange adsorbate by comparison with chosen atoms. Chosen atoms are compared and"
 write(o,'(A)') "positions averaged. Order of atom list kept from surface"
 write(o,'( )')
 write(o,'(A)') "  ads-(A,B,C)  +  (X,Y,Z)-surf   -->   ads-(X',Y',Z')-surf"
 write(o,'( )')
 write(o,'(A)') "  Examples:"
 write(o,'( )')
 write(o,'(A)') "            $ adsorb %1 dftb_run.gen h2o.xyz"
 write(o,'( )')
 write(o,'(A)') "Adsorb molecule in h2o.xyz to surface in dftb_run.gen. Do not change coordinates"
 write(o,'(A)') "but check nearest neighbors and return warnings."
 write(o,'( )')
 write(o,'(A)') "            $ adsorb %1:15:1 CONTCAR h2o.xyz"
 write(o,'( )')
 write(o,'(A)') "Adsorb molecule in h2o.xyz to surface in CONTCAR. Shift molecule such that"
 write(o,'(A)') "atom 1 is on atom 15 of the surface. Delete adsorbate atom 1."
 write(o,'( )')
 write(o,'(A)') "            $ adsorb %1:15,7:1,3 CONTCAR h2o.xyz"
 write(o,'( )')
 write(o,'(A)') "Adsorb molecule in h2o.xyz to surface in CONTCAR. Shift molecule such that atom 1"
 write(o,'(A)') "is on atom 15 of the surface. Align bond '1,3' with bond '15,7'. Delete adsorbate"
 write(o,'(A)') "atom 1 and surface atom 7."
 write(o,'( )')
 write(o,'(A)') "            $ adsorb %d:16,18:2,5 POSCAR phosphate.xyz"
 write(o,'( )')
 write(o,'(A)') "Adsorb molecule in phosphate.xyz to surface in POSCAR. Match atoms 2 and 5 in"
 write(o,'(A)') "molecule with atoms 16 and 18 in surface (i.e. to form a bidentate)"
 write(o,'( )')

      case( 'geominfo' )

 write(o,'( )')
 write(o,'(A)') "  Usage:  "//execute//" <command> <input file>"
 write(o,'( )')
 write(o,'(A)') "Get advanced geometry information"
 write(o,'(A)') "Workflow:"
 write(o,'( )')
 write(o,'(A)') "  Commands:"
 write(o,'(A)') "   o + <atom sel.>    -  wrap unit cell around center of mass of selection, add atoms to selection"
 write(o,'(A)') "   O                  -  same as 0, but with current selection"
 write(o,'(A)') "   s + <atom sel.>    -  add atoms to selection"
 write(o,'(A)') "   S + <subset>       -  save subset of current selection as vector coordinates"
 write(o,'(A)') "   g + x[,y[,z[,a]]]] -  calculate distance x-y or angle x-y-z or dihedral x-y-z-a from selected atoms"
 write(o,'(A)') "   G + x[,y[,z[,a]]]] -  calculate distance x-y or angle x-y-z or dihedral x-y-z-a from set of vector coordinates"
 write(o,'(A)') "   B + xy[,yz[,za]]]  -  calculate distance xy or angle xy-yz or dihedral xy-yz-za from set of vectors"
 write(o,'(A)') "   v                  -  verbose mode"
 write(o,'(A)') "   e + <elem>         -  set element filter, only add atoms of that element to selection."
 write(o,'(A)') "   r + <atom> + <r>   -  add atoms around picked atom within radius r, sorted by distance"
 write(o,'(A)') "   n + <number>       -  only keep first n atoms of selection"
 write(o,'(A)') "   c                  -  clean up selection, i. e. fix redundancies and range"
 
 write(o,'( )')
 write(o,'(A)') "  Define center:"
 write(o,'(A)') "   0 + <selection>    -  center of mass from new selection"
 write(o,'(A)') "   1                  -  center of mass from current selection"
 write(o,'(A)') "   2 + <x> <y> <x>    -  center from manually entered vector"
 write(o,'(A)') "   3 + <pick>         -  center from picked vector"
 write(o,'( )')
 write(o,'(A)') "  Change selection:"
 write(o,'(A)') "   4 + <selection>    -  explicit choice"
 write(o,'(A)') "   e + <element>      -  set element filter"
 write(o,'(A)') "   x + <#atm>         -  add #atm atoms with smallest x component"
 write(o,'(A)') "   y + <#atm>         -  add #atm atoms with smallest y component"
 write(o,'(A)') "   z + <#atm>         -  add #atm atoms with largest z component"
 write(o,'(A)') "   r + <#atm>         -  add #atm atoms in closest range to atom"
 write(o,'(A)') "   R + <radius>       -  all atoms within radius to atom"
 write(o,'( )')
 write(o,'(A)') "  Results from selection:"
 write(o,'(A)') "   p                  -  current selection"
 write(o,'(A)') "   g + <set>          -  calculate distance, angle or dihedral, depending on given set"
 write(o,'(A)') "   X + <file name>    -  write xyz-file from selection"
 write(o,'( )')
 write(o,'(A)') "  Set vectors:"
 write(o,'(A)') "   V + <x> <y> <z>    -  explicit definition"
 write(o,'(A)') "   E + <element>      -  by element (filter)"
 write(o,'(A)') "   c + <atom set>     -  copy atom set"
 write(o,'(A)') "   C + <atom set>     -  center of mass of atom set"
 write(o,'(A)') "   o +<pick> <at. set>-  shifted origin by coordinate vector (new from atom selection)"
 write(o,'(A)') "   O +<pick> <co. set>-  shifted origin by coordinate vector (modify coordinate vectors)"
 write(o,'( )')
 write(o,'(A)') "  Results from vectors:"
 write(o,'(A)') "   B + <coord. set>   -  see 'g' command"
 write(o,'(A)') "   G + <distance set> -  see 'g' command, but assume distances as input"
 write(o,'(A)') "   d + <distance set> -  average distance"
 write(o,'(A)') "   s + <distance set> -  shortest distance"
 write(o,'(A)') "   l + <distance set> -  longest distance"
 write(o,'(A)') "   a + <distance set> -  average angle, first vector to following"
 write(o,'(A)') "   A + <distance set> -  average angle pairwise"
 write(o,'( )')
 write(o,'(A)') "  Verbose mode, explore features with more detailed information and interim results on the fly:"
 write(o,'(A)') "   v                  -  toggle verbose mode"
 write(o,'( )')

      case( 'any2xsf' )

 write(o,'(A)') 'Used as direct interface to XCrysden. Add the following lines'
 write(o,'(A)') 'to your custom-definitions:'
 write(o,'(A)') ''
 write(o,'(A)') 'addOption --meteorgy any2xsf {'
 write(o,'(A)') '                    load any meteorgy supported file format'
 write(o,'(A)') '}'

      case( 'any2vasp' )

 write(o,'(A)') "  Usage:  "//execute//" <input file>"
 write(o,'( )')
 write(o,'(A)') "Make VASP geometry from input. Use more helpful defaults..."

      case( 'any2dftb' )

 write(o,'(A)') "  Usage:  "//execute//" <input file>"
 write(o,'( )')
 write(o,'(A)') "Make DFTB+ geometry from input."

      case( 'any2xyz' )

 write(o,'(A)') "  Usage:  "//execute//" <input file>"
 write(o,'( )')
 write(o,'(A)') "Make DFTB+ geometry from input."

      case default

 write(o,'(A)') "  Unknown command '"//execute//"'!"
 write(o,'( )')
 write(o,'(A)') "Try rewrite, adsorb or modify. Find details in source code."
    stop 1

    end select

    stop

  end subroutine manual
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  subroutine template()
    ! ----------------------------------------------------------
    ! parse command line / read files
    ! ----------------------------------------------------------
    call setup()

    ! ----------------------------------------------------------
    ! parse query
    ! ----------------------------------------------------------
    ! check number of query arguments qarg
    k = 0
    do i=1,size(qcmd)
      select case( qcmd(i) )
        case( 'x' ) ; k = k+0
      end select
    enddo

    if( k /= size(qarg) ) then
      write(stderr,'(A,I2,A,I2,A)') "[template]    Error, Wrong number of arguments for query commands '"//qcmd(:)// &
                                               "'. Needed:",k,", found:",size(qarg),"!"
      stat = -1  ;  return
    end if

    ! loop through query, run commands
    k = 0
    do i=1,size(qcmd)
      select case( qcmd(i) )
        case( 'x' )
          k = k+0
          write(stderr,'(A)') "[template]    Do something..."

        case default ;  write(stderr,'(A)') "[template]    Error, Unknown command '"//qcmd//"'. Stop." ; return
      end select
    enddo

  end subroutine template


end program cli
