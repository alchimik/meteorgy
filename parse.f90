module parse
use params , only: dp , lstr_ , str_ , stderr
use types  , only: atom , atomic_number , element_symbol

 implicit none
 private    ::     dp , lstr_ , str_ , stderr
 private    ::     atom , atomic_number , element_symbol
 public

 private    ::     expand_selection
! ---------------------------------------------------------------------------------------
! parse command line and "simple" quantities
! copyright (c) 2017 adrian hühn
! 
! this program is free software; you can redistribute it and/or modify it under
! the terms of the gnu general public license as published by the free software
! foundation; either version 2 of the license, or (at your option) any later version.
!
! this program is distributed in the hope that it will be useful, but without any
! warranty; without even the implied warranty of merchantability or fitness for a
! particular purpose.  see the gnu general public license for more details.
!
! you should have received a copy of the gnu general public license along with this
! program; if not, write to the free software foundation, inc., 51 franklin street,
! fifth floor, boston, ma  02110-1301, usa.
! ---------------------------------------------------------------------------------------

 contains

  ! ----------------------------------------------------------------------------
  function get_arg( num ) result( arg )
    character(len=:) , allocatable  :: arg
    integer , intent(in) :: num
    integer :: i
    ! ----------------------------------------------------------------

    ! empty if not available
    if( command_argument_count() < num ) then
      arg = ''
      return
    end if

    call get_command_argument( num , length = i )
    allocate( character(len=i) :: arg )
    call get_command_argument( num , value = arg )
  end function get_arg
  ! ------------------------------------------------------------------------ end

  ! ----------------------------------------------------------------------------
  subroutine split_keyline( key , line )
    character(len=:) , allocatable , intent(out)   :: key
    character(len=*)               , intent(inout) :: line
    integer :: i

    ! find first white space
    line = adjustl(line)
    do i = 1,len(line)
      if( line(i:i) == ' ' ) exit
    end do

    ! split key from line
    key = line(:i-1)
    line = adjustl(line(i:))

  end subroutine split_keyline
  ! ----------------------------------------------------------------------------
  ! ----------------------------------------------------------------------------
  subroutine parse_query( query , command , options , stat )
    use types, only: is_char
    ! --------------------------------------------------------------------------
    ! query:    formatted string of commands and options
    ! command:  array of characters, each defining a single action
    ! options:  array of strings, processed by actions
    !
    ! a query is started by '%'. it follows a string of characters defining
    ! the command. after the command, options might follow, each separated by
    ! colons. i.e.
    ! i.e. '%abc' is a query of three actions a,b, and c without additional
    ! options. queries can also be fairly complex:
    !
    !    %abcdef:12:3-8:poscar:0.74
    !
    ! this is a query of 6 options and 4 values, '12', '3-8', 'poscar' and
    ! '0.74'. note, that all this values are saved as strings, and would need
    ! to be processed to be identified integers, floats, files, ranges, etc.
    !
    ! --------------------------------------------------------------------------
    character(len=*) , intent(in)  :: query
    character(len=1) , allocatable , intent(out) :: command(:)
    character(len=:) , allocatable , intent(out) :: options(:)
    integer                        , intent(out) :: stat
    character(len=:) , allocatable :: iquery
    
    integer :: i , j , k , l
    integer :: fchar , lchar
    logical :: newword , newcmd

    stat   = 0
    iquery = trim(adjustl(query))

    if( iquery(1:1) /= '%' ) then
      write(stderr,'(a)') "[parse query] error: command does not start with '%'!"
      stat = 1
      return
    endif

    if( allocated (command) ) deallocate(command)
    if( allocated (options) ) deallocate(options)

    if( iquery(2:2) /= '%' ) then ! oldstyle query '%xyz:argx,argy1,argy2,argz'
      write(stderr,'(a)') "[parse query] warning: using oldstyle parsing! use '%%' for new style."

      ! find first colon
      do i=1,len(iquery)
        if( iquery(i:i) == ';' .or. iquery(i:i) == ':' ) then
          exit
        end if
      enddo
      k = i

      ! count colons
      l=0
      do i=k,len(iquery)
        if( iquery(i:i) == ';' .or. iquery(i:i) == ':' )  l = l+1
      enddo

      ! allocate
      allocate( command(k-2) )
      allocate( character(len = len(iquery)) :: options(l) )

      ! fill command
      do i=2,k-1
        command(i-1) = iquery(i:i)
      enddo

      ! fill query
      j = 1
      l = 0
      do i=k+1,len(iquery)
        if( iquery(i:i) == ';' .or. iquery(i:i) == ':' ) then
          options(j) = iquery(i-l:i-1)
          j = j+1
          l = 0
        else
          l = l+1
        end if
      enddo
      options(j) = iquery(i-l:)


    else ! new parsing: '%% x = argx ;y=argy1 argy2; z argz'

      if( iquery(3:3) == ';' ) then
        write(stderr,'(a)') "[parse query] error: query cannot start with semicolon!"
        stat = 1
        return
      endif
      ! append semicolon, if not found
      i = len(iquery)
      if( iquery(i:i) /= ';' ) iquery = iquery//';'

      ! count semicolons / number of commands
      j=0
      do i=1,len(iquery)
        if( iquery(i:i) == ';' )  j = j+1
      enddo

      ! number of arguments
      call count_words( ';= ' , iquery(3:) ,  k , l )

      ! allocate
      allocate( command( j ) )
      allocate( character( len = l ) :: options( k-j ) )

      ! fill command and query sequentially
      k  =  0
      l  =  0
      newword  =  .true.
      newcmd   =  .true.
      do i=3,len(iquery)
        if( is_char( iquery(i:i) , ';:= ' ) ) then

          if( .not. newword ) then ! char after word
            ! parse word as command or option
            if( newcmd ) then
              k          =  k+1
              command(k) =  iquery(j:j)
              newcmd     =  .false.
            else
              l          =  l+1
              options(l) =  iquery(j:i-1)
            end if
          end if

          newword  =  .true.

        else if( newword ) then ! first char of word

          j        =  i
          newword  =  .false.

        end if
        
        if( is_char( iquery(i:i) , ';:' ) )  newcmd  =  .true.

      end do
    end if

  end subroutine parse_query
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure elemental function keyplane( key_ ) result( plane )
    ! --------------------------------------------------------------------------
    ! fuzzy plane/direction parser
    ! 
    ! return plane as integer, where 1, 2, 3 reflect a, b, c, or x, y, z, and
    ! 0 if not recognized
    !   1      2      3      0
    !   a      b      c     any
    !   x      y      z     thi
    !   bc     ac     ab    nge
    !  (100)  (010)  (001)  lse
    ! --------------------------------------------------------------------------
    character(len=*) , intent(in) :: key_
    character(len=2)              :: key
    integer                       :: plane

    key = key_

    if     ( key == 'a ' .or. key == 'x ' .or. key == '1 ' .or. key == 'bc' .or. key == 'cb' ) then
      plane = 1
    else if( key == 'b ' .or. key == 'y ' .or. key == '2 ' .or. key == 'ac' .or. key == 'ca' ) then
      plane = 2
    else if( key == 'c ' .or. key == 'z ' .or. key == '3 ' .or. key == 'ab' .or. key == 'ba' ) then
      plane = 3
    else
      plane = 0
    end if
    ! ----------------------------------------------------------------
  end function keyplane
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function expand_selection( string , nat ) result( selection )
    use types, only: is_digit
    ! --------------------------------------------------------------------------
    ! ASSUMES VALID SELECTION!  USE parse_selection FOR CORRECTIONS
    ! 
    ! expand a selection string like '1-5,15,36-32,11' to an integer array like 
    ! 
    ! (/1,2,3,4,5,15,36,35,34,33,32,11/)
    ! 
    ! note that separator and range are hardcoded as hyphen and comma
    !
    ! --------------------------------------------------------------------------
    integer          , allocatable :: selection(:) , buffer(:)
    character(len=*) , intent(in)  :: string
    integer          , intent(in)  :: nat
    character(len=32)              :: temp
    logical          , allocatable :: lrange(:)
    logical :: invert
    integer :: i , j , k , l

    ! special instructions 'all' or 'none'

    if( string == 'all' ) then
      allocate( selection(nat) )
      do i = 1,nat
        selection(i) = i
      enddo
      return
    end if

    if( string == 'none' ) then
      allocate( selection(0) )
      return
    end if

    ! inversion

    if( string(1:1) == '^' ) then
      invert = .true.
      k = 2
    else
      invert = .false.
      k = 1
    end if

    ! first loop through string --> parse integers and range/separators

    l = len(string) 
    allocate( buffer(l), lrange(l) )

    temp = ''
    j = 1
    l = 0
    do i = k,len(string)
      ! (1) sign/number
      if( is_digit(string(i:i)) .or. ( string(i:i) == '-' .and. j == 1 ) ) then
        temp(j:j) = string(i:i)
        j = j+1
      else
        j = 1
        l = l+1
        read(temp,*) buffer(l)
        temp = ''
        
        ! (2) range/separator)
        if( string(i:i) == '-' ) then
          lrange(l) = .true.
        else if( string(i:i) == ',' ) then
          lrange(l) = .false.

        else ! ERROR
          allocate( selection(0) )
          return
        end if

      end if
    enddo

    ! last entries:
      l = l+1
      read(temp,*) buffer(l)
      lrange(l) = .false.


    ! calculate size of selection

    k = l
    do i = 1,l-1
      if( lrange(i) ) then
        k = k + ABS(buffer(i)-buffer(i+1)) - 1
      end if
    end do

    allocate( selection(k) )
    
    ! second loop: fill selection

    k = 0
    do i = 1,l
      if( lrange(i) ) then
        do j = buffer(i) , buffer(i+1) , sign(1 , buffer(i+1)-buffer(i))
          k = k+1
          selection(k) = j
        end do
        k = k-1 ! last entry set too early
      else
        k = k+1
        selection(k) = buffer(i)
      end if
    end do

    if( invert ) selection = invert_selection( selection , nat )

  end function expand_selection
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function compact_selection( selec , nat ) result( string )
    ! --------------------------------------------------------------------------
    ! compact a selection  (/1,2,3,4,5,15,36,35,34,33,32,11/) to a string like
    ! '1-5,15,36-32,11'.
    ! --------------------------------------------------------------------------
    character(len=:) , allocatable :: string
    integer          , intent(in)  :: selec(:)
    integer          , intent(in)  :: nat
    integer          , allocatable :: buffer(:)
    logical          , allocatable :: lrange(:)
    character(len=16)              :: frag
    logical :: lncr
    integer :: i,j,k,l,m,n

    l = size(selec) 
    allocate( buffer(l), lrange(l) )

    k = 1
    lrange(:) = .false.
    lncr      = .true.
    buffer(1) = selec(1)
    do i = 2 , l-1

      m = selec(i+1)-selec(i  )
      n = selec(i  )-selec(i-1)
      
      if( abs(m) == 1 .and. abs(n) == 1 .and. n == m .and. lncr ) then

        lrange(k) = .true.
        if(k>1) lncr = .not. lrange(k-1)

      else

        k = k+1
        buffer(k) = selec(i)

      end if
    end do
    k = k+1
    buffer(k) = selec(l)

    do i = 1 , k
      write(frag,"(i15,a1)") buffer(i) , merge( '-' , ',' , lrange(i) )
      string = string//frag
    end do

    k = 0
    do i = 1,len(string)
      if( string(i:i) == ' ' ) cycle
      k = k+1
      string(k:k) = string(i:i)
    end do
    string = string(1:k-1)
    ! --------------------------------------------------------------------------
  end function compact_selection
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function add_to_selection( string , selec , nat ) result( selection )
    integer , allocatable :: selection(:) , newselec(:), buffer(:)
    ! --------------------------------------------------------------------------
    ! thin wrapper around expand_selection. use to add more atoms to given
    ! selection.
    ! --------------------------------------------------------------------------
    character(len=*)      , intent(in)    :: string
    integer , allocatable , intent(in)    :: selec(:)
    integer               , intent(in)    :: nat
    integer               :: p , q
  
    newselec =  expand_selection( string , nat )

    ! behave if selec not allocated
    if( allocated(selec) ) then
      buffer = selec
    else
      allocate( buffer(0) )
    end if

    ! concatenate arrays
    p  =  size(buffer)
    q  =  size(newselec)
    allocate( selection( p + q ) )
    selection(   :p)  =  buffer
    selection(p+1: )  =  newselec

  end function add_to_selection
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function fix_selection( selec , nat ) result( selection )
    integer , allocatable  :: selection(:)
    ! --------------------------------------------------------------------------
    ! fix out of range entries and redundancies. for redundancies keep first
    ! occurrence.
    !
    ! warning: it corrects any selection to a valid one!
    ! --------------------------------------------------------------------------
    integer , intent(in) , allocatable :: selec(:)
    integer , intent(in)               :: nat
    integer :: i,j,k,n

    ! allocation status
    if( .not. allocated(selec) ) then
      allocate( selection(0) )
      return
    end if

    selection = selec

    ! fix out of boundaries, tag redundancies
    do i = 1,size(selec)

      ! fix out-of-boundary values
      selection(i) = modulo(selec(i)-1,nat)+1

      ! tag redundancies
      do j = i-1 , 1 , -1

        if( selection(j) == selection(i) ) then
          selection(i) = -42
          exit
        end if

      enddo

    enddo
      
    ! remove redundancies, k is new size in the end
    k = 0
    do i = 1,size(selec)
      if( selection(i) /= -42 ) then
        k = k+1
        selection(k) = selection(i)
      end if
    enddo


    ! set final selec
    selection = selection(1:k)

  end function fix_selection
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function invert_selection( selec , nat ) result( selection )
    integer , allocatable   :: selection(:)
    integer , allocatable , intent(in)    :: selec(:)
    integer , intent(in)    :: nat
    integer  :: i , k , s ,    tmp(nat)

    allocate( selection(nat) )

    ! initialize
    do i = 1,nat
      tmp(i) = i
    enddo

    ! mark excluded values
    k = merge( size(selec) , 0 , allocated(selec) )
    do i = 1,k
      s = modulo(selec(i)-1,nat)+1
      tmp(s) = -42
    enddo

    ! invert
    k = 0
    do i = 1,nat
      if( tmp(i) /= -42 ) then
        k = k+1
        selection(k) = tmp(i)
      end if
    enddo

    selection = selection(1:k)

  end function invert_selection
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  subroutine parse_selection( nat , string , selec , stat )
    ! --------------------------------------------------------------------------
    ! (1)  parse a selection from a string like "1-9,15,36,11"
    ! (2)  invert selection with leading tilde, '^'.
    ! (3)  result is stored in selec, with the order given in selection
    ! (4)  check correctness of selection content (no redundancies, does not
    !      exceed nat, ...)
    ! (5)  fix selection or leave array deallocated in case of errors.
    ! --------------------------------------------------------------------------
    integer               , intent(in)  :: nat
    character(len=*)      , intent(in)  :: string
    integer , allocatable , intent(out) :: selec(:)
    integer               , intent(out) :: stat
    character(len=:) , allocatable      :: istring

    istring = glob_separator( string , ',' )

    call check_selection( nat , istring , stat )
    if( stat /= 0 ) return

    selec = expand_selection( istring , nat )
    selec = fix_selection( selec , nat )

    write(stderr,'(a)') "[parse selection] read in selection and fixed any problems on the fly..."
    write(stderr,'(a)') "[parse selection] Using the following selection now: "//compact_selection( selec , nat )

  end subroutine parse_selection
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  subroutine check_selection( nat , string , stat )
    use types, only: is_char, is_digit
    ! --------------------------------------------------------------------------
    ! check and report errors in input string
    ! --------------------------------------------------------------------------
    integer               , intent(in)  :: nat
    character(len=*)      , intent(in)  :: string
    integer               , intent(out) :: stat
    character(len=1) :: r , s
    integer          :: i , j , k , l

    stat = 0
    s    = ','
    r    = '-'

    ! check inversion (first character)
    if( string(1:3) == 'all' .or. string(1:4) == 'none' ) then
      return
    else if( string(1:1) == '^' ) then
      k = 2
    else
      k = 1
    end if

    ! check error: start and end of string (should be part of a number)
    l = len(string)
    if( string(k:k) /= '-' .and. .not. is_digit(string(k:k)) .or. .not. is_digit(string(l:l)) ) then
        write(stderr,'(a)') "[parse selection] Error: faulty start or end of input string!"
        stat = 1
        return
    end if
    
    ! loop through selection
    do i = 2 , len(string)-1

      ! check error: faulty characters
      if( .not. is_char( string(i:i) , '-'//s//r ) .and. .not. is_digit( string(i:i) ) ) then
        write(stderr,'(a)') "[parse selection] Error: faulty character, "//string(i:i)//"in input string!"
        stat = 1
        return
      end if
      
      ! check error: consecutive range and/or separator
      if( string(i:i) == s .and. string(i+1:i+1) == s .or.                 &
          string(i:i) == s .and. string(i+1:i+1) == r .or.                 &
          string(i:i) == r .and. string(i+1:i+1) == s .or.                 &
          string(i:i) == r .and. string(i+1:i+1) == r .and. r /= '-' .or.  &
          string(i:i) == r .and. string(i+1:i+1) == r .and. string(i-1:i-1) == r  ) then
        write(stderr,'(a)') "[parse selection] Error: consecutive '"//s//"' or '"//r//"'!"
        stat = 1
        return
      end if 

      ! check error: consecutive digit (if r != '-')
      if( is_digit( string(i:i) ) .and. string(i+1:i+1) == r .and. r /= '-' ) then
        write(stderr,'(a)') "[parse selection] Error: consecutive numbers!"
        stat = 1
        return
      end if 
      ! consecutive number
    
    end do ! check loop through selection

  end subroutine check_selection
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function mkrange( from , to ) result( range )
    integer , intent(in)  :: from , to
    integer , allocatable :: range(:)
    integer :: i,k
    allocate( range( to - from + 1 ) )
    k = 0
    do i = from,to
      k = k+1
      range(k) = i
    enddo
  end function mkrange
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure subroutine count_words( sep , str , counter , maxsize )
    use types, only: is_char
    !
    ! count words in string str. treat characters in sep as separator
    !
    character(len=*)   , intent(in)  :: sep , str
    integer , optional , intent(out) :: counter , maxsize
    logical :: lnw
    integer :: c , m
    integer :: i , j

    ! initialize
    c   =  0
    m   =  0
    lnw =  .true.

    ! count first character of every word, keep size of longest word in maxsize
    do i = 1,len(str)

      if( is_char( str(i:i) , sep ) ) then

        if( .not. lnw )  m  =  max( m , i-j )
        lnw  =  .true.

      else if( lnw ) then

        c    =  c + 1
        j    =  i
        lnw  =  .false.

      end if

    enddo

    if(present( counter ))  counter  =  c
    if(present( maxsize ))  maxsize  =  m

  end subroutine count_words
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  subroutine parse_elem( line , elem )
    ! --------------------------------------------------------------------------
    ! parse elements from line (slim down repetitive noodle code)
    ! --------------------------------------------------------------------------

    character(len=*) , intent(in)   :: line
    integer          , intent(out)  &
                     , allocatable  :: elem(:)

    integer          , allocatable  :: ielem(:)
    character(len=2) , allocatable  :: carray(:)
    integer                         :: i , k

    ! --------------------------------------------------------------------------
    allocate( carray(len(trim(adjustl(line)))/2+2) )

    carray(:)  = "  "               ! 1. initialize character array
    read(line,*,iostat=k) carray(:) ! 2. parse line (iostat ignored but needed
                                    !    to suppress read error)

    k = 1
    do while( carray(k) /= "  " )   ! 3. check termination
      k = k+1
    end do
    k = k-1
    
    if( allocated(elem) ) deallocate(elem) 
    allocate( elem(k) )             ! 4. allocate output

    do i = 1,k                      ! 5. save elements as atomic numbers
      elem(i)  = atomic_number( carray(i) )
    end do

    deallocate( carray )

  end subroutine parse_elem
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  subroutine group_elem( atm , formula , lsort )
    ! --------------------------------------------------------------------------
    ! group elements from atm array by identifying consecutive groups of
    ! different elements and - if requested - sorting them
    ! 
    ! formula - 2d array containing atomic number and number of atoms in group
    ! 
    !            formula(1,:) - atomic number
    !            formula(2,:) - group number of atoms
    ! --------------------------------------------------------------------------
    type(atom)            , intent(in)  :: atm(:)
    integer , allocatable , intent(out) :: formula(:,:)
    logical , optional    , intent(in)  :: lsort

    integer  :: f(2,size(atm))
    integer  :: t(2)
    integer  :: i,j,k,l
    ! --------------------------------------------------------------------------

    ! initialize arrays before comparison
    f(1,1) = atm(1)%e
    f(2,1) = 1
    f(:,2:)= 0

      j  = 1
      ! compare
      do i = 2 , size(atm)       ! loop through element array
        if( f(1,j) /= atm(i)%e ) then
          j = j + 1
          f(1,j) = atm(i)%e      ! assign with element number
        end if
        f(2,j) = f(2,j) + 1      ! count element
      end do
      k = j   ! k equals number of atom sorts now

    ! --------------------------------------------------------------------------
    ! sort + delete redundancies if requested

    if( present(lsort) ) then; if( lsort ) then

      ! sort atoms with insertion sort, o(n^2)
      do i = 2,k
        t = f(:,i)
        j = i-1
        do while( f(1,j) > t(1) .and. j > 0 ) ! move
          f(:,j+1) = f(:,j)
          j = j-1
        enddo
        f(:,j+1) = t        ! return
      enddo

      ! join nonredundant groups in beginning of array
      l = 1
      do i = 2,k
        if( f(1,i) == f(1,l) ) then
          f(2,l) = f(2,l) + f(2,i)
        else
          l=l+1
          f(:,l) = f(:,i)
        endif
      enddo

      k = l    ! final size of output array

    end if; end if  ! lsort
    ! --------------------------------------------------------------------------

    allocate( formula(2,k) )
    formula(:,:) = f(:,1:k)

  end subroutine group_elem
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  function print_elem( elem ) result( line )
  ! ----------------------------------------------------------------------------
  ! print list of elements in one line
  ! ----------------------------------------------------------------------------
    integer , intent(in) :: elem(:)
    character(len=:) , allocatable :: line
    character(len=8) , allocatable :: frmt
    integer  :: i
    ! --------------------------------------------------------------------------
    allocate( character(len=3*size(elem)) :: line ) ! 1. create a line
    frmt="(****a3)"
    write(frmt(2:5),'(i4.4)') size(elem)
    write(line,frmt) element_symbol(elem(:))

    line = glob_separator( line )                 ! 2. remove redundant spaces

  end function print_elem
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  function print_integers( num ) result( line )
  ! ----------------------------------------------------------------------------
  ! print integers in one line
  ! ----------------------------------------------------------------------------
    integer , intent(in) :: num(:)
    character(len=:) , allocatable  :: line
    character(len=:) , allocatable  :: frmt
    ! --------------------------------------------------------------------------
    if( size(num) == 0 ) then
      line  = ''
      return
    end if

    ! 1. create string buffer
    allocate( character(len=16*size(num)) :: line )
    frmt="(****i16)"
    write(frmt(2:5),'(i4.4)') size(num)
    ! 2. print integers
    write(line,frmt) num(:)
    ! 3. glob spaces
    line  = glob_separator( line )

  end function print_integers
  ! ------------------------------------------------------------------------ end

  
  ! ----------------------------------------------------------------------------
  function print_floats( fnum , digits , lnoexp ) result( line )
  ! ----------------------------------------------------------------------------
  ! print floats in line (default: 6 significant digits, scientific notation)
  ! without float, values are rounded if possible
  ! ----------------------------------------------------------------------------
    real(dp) , intent(in) :: fnum(:)
    integer  , intent(in) , optional :: digits
    logical  , intent(in) , optional :: lnoexp
    character(len=:) , allocatable :: line
    character(len=:) , allocatable :: frmt
    integer :: d , i
    ! --------------------------------------------------------------------------
    d    = 6   ! -x.xxxxxe-xxx_
    if( present(digits) )  d = merge( digits , 20 , digits < 20 )

    !allocate( character(len=d+8) :: pnum(size(fnum)) ) ! 1. create a line

    if( present(lnoexp) ) then
      !frmt = "(****f**.**)"
      !write(frmt,"('(',i4.4,'f',i2.2,'.',i2.2,')')") size(fnum) , d+8 , d-1
      !write(line,frmt) fnum(:)
      line = ''
      do i = 1,size(fnum)
        line = line // print_human_float( fnum(i) , d )
      end do
    else
      frmt = "(****es**.**)"
      write(frmt,"('(',i4.4,'es',i2.2,'.',i2.2,')')") size(fnum) , d+8 , d-1
      
      allocate( character(len=(d+8)*size(fnum)) :: line ) ! 1. create a line
      write(line,frmt) fnum(:)
    end if

    line = glob_separator( line )                  ! 2. remove redundant spaces
  end function print_floats
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  function print_human_float( fnum , digits ) result( pnum )
  ! ----------------------------------------------------------------------------
  ! print floats in line (default: 6 significant digits, smart rounding)
  ! ----------------------------------------------------------------------------
    real(dp) , intent(in) :: fnum
    integer  , intent(in) , optional :: digits
    character(len=:) , allocatable :: pnum
    character(len=:) , allocatable :: frmt
    integer :: d , j , k
    logical :: l9 , l0
    character(len=1) :: sub
    ! --------------------------------------------------------------------------
    d    = 6   ! -x.xxxxxe-xxx_
    if( present(digits) )  d = merge( digits , 20 , digits < 20 )

    allocate( character(len=d+8) :: pnum ) ! 1. create a line

    frmt = "(f**.**)"
    write(frmt,"('(f',i2.2,'.',i2.2,')')") d+8 , d-1

    write(pnum,frmt) fnum

    ! round the obvious
    l9  = .false.
    l0  = .false.
    sub = ' '
    do j = len(pnum) , 3 , -1

      ! leave one digit behind decimal point
      if( pnum(j-1:j-1) == '.' )  then
        sub = '0'
      end if

      ! skip decimal point
      if( pnum(j:j) == '.' )  then
        continue
      ! substitute zeroes by spaces
      else if( .not. l9 .and. pnum(j:j) == '0' )  then
        pnum(j:j) = sub
        l0 = .true.
      ! substitute consecutive nines by spaces
      else if( pnum(j-2:j) ==  '999' .or. &
               pnum(j-3:j) == '9.99' ) then
        pnum(j:j) = sub
        l9 = .true.
      ! substitute all nines if found at least three
      else if( l9 .and. pnum(j:j) == '9' )  then
        pnum(j:j) = sub
      ! round up last digit after nines
      else if( l9 )  then
        read(  pnum(j-1:j-1) , '(I1)' )  k
        write( pnum(j-1:j-1) , '(I1)' )  k+1
        exit
      else !if( l0 )  then
        exit
      end if

    end do
    
  end function print_human_float
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  function glob_separator( line , separator ) result( glob )
    ! --------------------------------------------------------------------------
    ! remove all spaces, replacing with one separator
    ! --------------------------------------------------------------------------
    character(len=*) , intent(in)            :: line
    character(len=1) , intent(in) , optional :: separator
    character(len=:) , allocatable           :: glob
    character(len=1) :: sep
    logical          :: lsep
    integer          :: i , k
    ! --------------------------------------------------------------------------

    glob = trim(adjustl(line))     ! 1. exterminate initial and final spaces
    sep = ' '
    if( present( separator ) ) sep = separator

    k = 0
    lsep = .false.
    do i = 1,len(glob)

      if( glob(i:i) == ' ' &
     .or. glob(i:i) == sep ) then  ! 2. find gap of whitespaces and separators
        lsep = .true.
        cycle
      else if( lsep ) then         ! 3. add one separator after reaching end of gap
        k = k+1
        glob(k:k) = sep
      end if

      k = k+1                      ! 4. rewrite line
      glob(k:k) = glob(i:i)
      lsep = .false.

    end do

    glob = glob(1:k)               ! 5. reallocate line with new termination

  end function glob_separator
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  function string_substitute( line , seek , replace ) result( substitute )
    ! --------------------------------------------------------------------------
    ! replace occurrence of 'seek' with 'replace' sequentially
    ! --------------------------------------------------------------------------
    character(len=*) , intent(in)  :: line , seek , replace
    character(len=:) , allocatable :: substitute , buffer
    !character(len=len(line))       :: buffer
    integer          :: i , j , k , l , m , n
    ! --------------------------------------------------------------------------
    m = len(seek)-1
    n = len(replace)-1

    if( len(seek) >= len(replace) ) then
      ! no need for reallocations
      k = 1
      l = 1
      substitute = line
      do while( k <= len(line)-m )
        if( line(k:k+m) == seek ) then
          substitute(l:l+n) = replace
          k = k+m
          l = l+n
        else
          substitute(l:l) = line(k:k)
        end if
        k = k+1
        l = l+1
      end do
      ! add sometimes remaining last characters
      do while( k <= len(line) )
        substitute(l:l) = line(k:k)
        k = k+1
        l = l+1
      end do
      ! reallocate once
      substitute = substitute(1:l)
    else
      ! reallocate for every substitution
      k = 1
      l = 1
      substitute = line
      do while( k <= len(line)-m )
        if( line(k:k+m) == seek ) then
          substitute = substitute(:l-1) // replace // substitute(l+m+1:)
          k = k+m
          l = l+n
        end if
        k = k+1
        l = l+1
      end do
    end if
  
  end function string_substitute
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  subroutine print_w_newline( funit , string )
    ! --------------------------------------------------------------------------
    ! print strings containing newline characters excaped by '\n'
    ! --------------------------------------------------------------------------
    integer          , intent(in)  :: funit
    character(len=*) , intent(in)  :: string
    character(len=*) , parameter   :: seek='\n'
    character(len=len(string))     :: buffer
    integer          :: i , j , k , l , m , n
    ! --------------------------------------------------------------------------
    k = 1
    l = 0
    do while( k < len(string) )
      if( string(k:k+1) == '\n' ) then
        write(funit,'(a)') buffer(1:l)
        k = k+1
        l = 0
      else
        l = l+1
        buffer(l:l) = string(k:k)
      end if
      k = k+1
    end do
    if( k == len(string) ) then ! add last character(s), if not '\n'
      l = l+1
      buffer(l:l) = string(k:k)
    end if
    write(funit,'(a)') buffer(1:l)
  end subroutine print_w_newline
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  elemental function upper_case( string )
    ! convert all lower case letters to capitals
    character(len=*), intent(in) :: string
    character(len=len(string))   :: upper_case
    integer                      :: i,k

    do i = 1,len(string)
      k = iachar( string(i:i) )
      if( k > 96 .and. k <= 122 ) then; upper_case(i:i) = achar(k-32) 
      else                            ; upper_case(i:i) = string(i:i)
      end if
    end do
  end function upper_case
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  elemental function lower_case( string )
    ! convert all upper capitals to lower case
    character(len=*), intent(in) :: string
    character(len=len(string))   :: lower_case
    integer                      :: i,k

    do i = 1,len(string)
      k = iachar( string(i:i) )
      if( k > 64 .and. k <= 90 ) then; lower_case(i:i) = achar(k+32)
      else                           ; lower_case(i:i) = string(i:i)
      end if
    end do
  end function lower_case
  ! ------------------------------------------------------------------------ end


!  ! ----------------------------------------------------------------------------
!  function tag_file( filename , tag , ext , dig ) result( fname )
!  ! ----------------------------------------------------------------------------
!  ! (1) parse filename
!  ! (2) if tag not present, add tag
!  ! (3) if tag present, add number
!  ! (4) if tag+number present, increment number
!  !  + take care of file extension
!  ! ----------------------------------------------------------------------------
!
!    character(len=*)   , intent(in)  :: filename , tag , ext
!    integer , optional , intent(in)  :: dig
!    character(len=:)   , allocatable :: fname , frmt
!    integer :: ec , c , i , j , k , stat
!    logical :: lext
!    
!    ! --------------------------------------------------------------------------
!    ec  = 2 ;  if( present( dig )) ec  = dig
!
!    ! format
!    frmt = '(i??.??)'
!    write(frmt(3:4),'(i2.2)') ec
!    write(frmt(6:7),'(i2.2)') ec
!
!
!    fname = trim(adjustl(filename))
!
!    i = len(trim(adjustl(fname)))
!    j = len(trim(adjustl(ext)))
!    k = len(trim(adjustl(tag)))
!
!    ! cut extension
!    lext=.false.
!    if( fname(i-j+1:i) == ext ) then
!      fname = fname(1:i-j)
!      i = len(trim(adjustl(fname)))
!      lext=.true.
!    endif
!
!    ! enumerated output geometries starting with 2 and two digits, start overwriting after 100
!    c = 1
!    if( fname(i-j+1:i) == tag .or. fname(i-j-ec+1:i-ec) == tag ) then
!      ! read counter
!      if( iachar(fname(i:i)) >= 48 .and. iachar(fname(i:i)) < 58 ) then
!        read(fname(i-ec+1:i),frmt,iostat=stat) c
!        if( stat /= 0 ) then
!          write(stderr,'(a)') "[rot] warning, messy filename: '"//fname//"'! will count from 10 now."
!          c = 9
!        endif
!        if( c == (10**ec)-1 ) c = -1
!      else ! append space for enumeration
!        i=i+ec
!        deallocate( fname )
!        allocate( character(len=i) :: fname )
!        read(filename,*,iostat=stat) fname
!      endif
!
!      ! update counter
!      write(fname(i-ec+1:i),frmt) c+1
!
!    else
!      fname = fname//tag
!    endif
!    
!    if( lext ) fname = fname//ext
!
!  end function tag_file
!  ! ------------------------------------------------------------------------ end


end module parse
