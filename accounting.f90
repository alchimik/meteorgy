MODULE accounting


USE params , ONLY: dp , str_ , stderr , rnginit
USE types  , ONLY: pbc , atom

 IMPLICIT NONE
 PRIVATE    ::     dp , str_ , stderr , rnginit
 PRIVATE    ::     pbc , atom 
 PUBLIC

 ! ---------------------------------------------------------------------------------------
 ! Sorting, reordering, comparisons
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ---------------------------------------------------------------------------------------

 ! ------------------------------------------------------------------------------
 ! DEFINITIONS
 ! ===========
 ! nat   -  number of atoms
 ! mask  -  integer array that consists of all integers from 1 to nat, without
 !          redundancies
 ! selec -  subset of mask
 ! key   -  choice of quantity for sorting (details below)
 ! 
 ! ------------------------------------------------------------------------------
  
  ! --------------------------------------------------------------------------
  ! Performance: if ever used for large systems (>> 100 atoms), implement
  !              counting sort for elements and merge sort for everything
  !              else. Merge sort is better for partially ordered systems
  ! --------------------------------------------------------------------------
 CONTAINS
  
  SUBROUTINE sort_by_tag( atm )
    ! --------------------------------------------------------------------------
    ! Sort atoms by tag assuming tag is used for list positions
    !
    ! atm: array of atoms
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    TYPE(atom)                 :: at
    INTEGER                    :: k , l

    k = 1
    DO WHILE( k < SIZE(atm) )
      k = MERGE( k+1 , k , atm(k)%t == k )
      l = atm(k)%t

      ! swap
      at     = atm(l) 
      atm(l) = atm(k) 
      atm(k) = at
    ENDDO
  END SUBROUTINE sort_by_tag


  subroutine chk_tagmask( atm , mask , stat )
    use parse, only: print_integers , compact_selection
    ! --------------------------------------------------------------------------
    ! check wether tag or mask contains invalid entries
    ! --------------------------------------------------------------------------
    type(atom) , intent(in) , optional :: atm(:)
    integer    , intent(in) , optional :: mask(:)
    integer    , intent(out), optional :: stat
    integer :: i,j,k,nat
    character(len=:) , allocatable :: line
    integer          , allocatable :: sel(:)
    logical          , allocatable :: test(:)

    if( present(atm) ) then
      nat  =  size(atm)
      allocate( sel(nat) , test(nat) )
      test(:) = .true.
      stat = 0

      do i= 1 , nat 

        ! find out-of-range tags and cross existing in-range tags
        if( atm(i)%t /= modulo(atm(i)%t-1,nat)+1 )  then
          write(stderr,'(a)') '[check tag]   Warning, index '//print_integers((/i/))  &
                           // ' out of range, '//print_integers((/atm(i)%t/))         &
                           // ' (range: '     // print_integers((/nat/))//')'
          stat = -1
        else
          test( atm(i)%t ) = .false.
        end if

        ! find redundant tags and give bundled report
        k=1
        sel(k) = i
        do j = i+1,nat
          if( atm(i)%t == atm(j)%t ) then
            k = k+1
            sel(k) = j
          end if
        end do
        if( k > 1 )  then
          write(stderr,'(a)') '[check tag]   Warning, redundant tags '//print_integers((/atm(i)%t/))  &
                           // ' in '//compact_selection( sel(1:k) , nat )
          stat = 1
        end if
      end do
      ! seek missing tags and bundle report
      k=0
      do i= 1 , nat 
        if( test(i) )  then
          k = k+1
          sel(k) = i
        end if
      end do
      if( k > 0 )  then
        write(stderr,'(a)') '[check tag]   Warning, missing tags '//compact_selection( sel(1:k) , nat )
        stat = 1
      end if
        
    end if

    if( present(mask) ) then
      nat  =  size(mask)
      allocate( sel(nat) , test(nat) )
      test(:) = .true.

      do i= 1 , nat 

        ! find out-of-range masks and cross existing in-range masks
        if( mask(i) /= modulo(mask(i)-1,nat)+1 )  then
          write(stderr,'(a)') '[check mask]  Warning, index '//print_integers((/i/))  &
                           // ' out of range, '//print_integers((/mask(i)/))         &
                           // ' (range: '     // print_integers((/nat/))//')'
          stat = -1
        else
          test( mask(i) ) = .false.
        end if

        ! find redundant masks and give bundled report
        k=1
        sel(k) = i
        do j = i+1,nat
          if( mask(i) == mask(j) ) then
            k = k+1
            sel(k) = j
          end if
        end do
        if( k > 1 )  then
          write(stderr,'(a)') '[check mask]  Warning, redundant masks '//print_integers((/mask(i)/))  &
                           // ' in '//compact_selection( sel(1:k) , nat )
          stat = 1
        end if
      end do
      ! seek missing masks and bundle report
      k=0
      do i= 1 , nat 
        if( test(i) )  then
          k = k+1
          sel(k) = i
        end if
      end do
      if( k > 0 )  then
        write(stderr,'(a)') '[check mask]  Warning, missing masks '//compact_selection( sel(1:k) , nat )
        stat = 2
      end if
        
    end if

  end subroutine chk_tagmask


  SUBROUTINE sort_by_mask( mask , atm )
    ! --------------------------------------------------------------------------
    ! Sort atoms along with a list of positions
    !
    ! atm:  array of atoms
    ! mask: any list with same size as atm containing values from 1 to size(atm)
    ! --------------------------------------------------------------------------
    INTEGER    , INTENT(INOUT) :: mask(:)
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    TYPE(atom)                 :: at
    INTEGER                    :: k , l , m

    k = 1
    DO WHILE( k < SIZE(atm) )
      k  = MERGE( k+1 , k , mask(k) == k )
      l  = mask(k)
      
      ! swap
      at      = atm(k) 
      atm(k)  = atm(l)   ; mask(k) = mask(l)
      atm(l)  = at       ; mask(l) = l
    ENDDO
  END SUBROUTINE sort_by_mask


  SUBROUTINE sort_elem( atm )
    ! --------------------------------------------------------------------------
    ! Sort atoms by element (lowest atomic number first) with counting sort
    ! --------------------------------------------------------------------------
    TYPE(atom)       , INTENT(INOUT) :: atm(:)

    ! temporary copy
    TYPE(atom) :: at

    ! control
    INTEGER          :: i , j

    
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE sort_elem


  SUBROUTINE sort_one( key , atm )
    ! --------------------------------------------------------------------------
    ! Sort atoms with insertion sort, O(n^2)
    !
    ! key      - two character instruction to sort, i.e. 'y+'
    ! key(1:1) - sort by, element (e), tag (t) or coordinates (x,y,z)
    ! key(2:2) - sort in ascending order (+) or descending order (-)
    ! atm: array of atoms
    ! --------------------------------------------------------------------------
    CHARACTER(LEN=2) , INTENT(IN)    :: key
    TYPE(atom)       , INTENT(INOUT) :: atm(:)

    ! temporary copy
    TYPE(atom) :: at

    ! control
    INTEGER          :: i , j
    CHARACTER(LEN=2) :: is_key ! insertion sort key

    ! check key(1)
    IF( key(1:1) /= 'e' .AND. key(1:1) /= 't' .AND.   &
        key(1:1) /= '1' .AND. key(1:1) /= '2' .AND. key(1:1) /= '3' ) THEN
      WRITE(stderr,'(A)') '[sort one] ERROR: unknown sort key ('//key//')'
      RETURN
    END IF

    ! check key(2), set up key(s)
    IF( key(2:2) == '+' ) THEN
      is_key = key(1:1)//'>'
    ELSE IF( key(2:2) == '-' ) THEN
      is_key = key(1:1)//'<'
    ELSE
      WRITE(stderr,'(A)') '[sort one] ERROR: unknown sort key ('//key//')'
      RETURN
    END IF


    ! Sort atoms with insertion sort, O(n^2)
    DO i = 2,SIZE(atm)
      at = atm(i)
      j = i-1
      DO WHILE( compare( is_key , atm(j) , at ) .AND. j > 0 ) ! move
        atm(j+1) = atm(j)
        j = j-1
      ENDDO
      atm(j+1) = at       ! return
    ENDDO

  CONTAINS
    FUNCTION compare( key , at1 , at2 )
      CHARACTER(LEN=2) , INTENT(in) :: key
      TYPE(atom)       , INTENT(in) :: at1 , at2
      LOGICAL :: compare
  
      SELECT CASE(key)
        CASE('e>') ; compare = at1%e     > at2%e 
        CASE('e<') ; compare = at1%e     < at2%e
        !CASE('e=') ; compare = at1%e    == at2%e
        CASE('t>') ; compare = at1%t     > at2%t
        CASE('t<') ; compare = at1%t     < at2%t
        !CASE('t=') ; compare = at1%t    == at2%t
        CASE('1>') ; compare = at1%r(1)  > at2%r(1)
        CASE('1<') ; compare = at1%r(1)  < at2%r(1)
        !CASE('1=') ; compare = at1%r(1) == at2%r(1)
        CASE('2>') ; compare = at1%r(2)  > at2%r(2)
        CASE('2<') ; compare = at1%r(2)  < at2%r(2)
        !CASE('2=') ; compare = at1%r(2) == at2%r(2)
        CASE('3>') ; compare = at1%r(3)  > at2%r(3)
        CASE('3<') ; compare = at1%r(3)  < at2%r(3)
        !CASE('3=') ; compare = at1%r(3) == at2%r(3)
      END SELECT
      ! ------------------------------------------------------------------ END
    END FUNCTION compare
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE sort_one


  SUBROUTINE sort_by_distance( point , atm )
    ! --------------------------------------------------------------------------
    ! Sort atoms with insertion sort, O(n^2), by distance to point
    !
    ! atm: array of atoms
    ! --------------------------------------------------------------------------
    REAL(dp)   , INTENT(IN)    :: point(3)
    TYPE(atom) , INTENT(INOUT) :: atm(:)

    REAL(dp)   :: dst(SIZE(atm))
    
    TYPE(atom) :: at
    REAL(dp)   :: a(3) , d

    ! control
    INTEGER          :: i , j

    ! get distance
    DO i = 1,SIZE(atm)
      a  (:)  = point(:) - atm(i)%r(:)
      dst(i)  = a(1)**2 + a(2)**2 + a(3)**2
    ENDDO

    ! Sort atoms with insertion sort, O(n^2)
    DO i = 2,SIZE(atm)
      at = atm(i)
      d  = dst(i)
      j = i-1
      DO WHILE( dst(j) > d .AND. j > 0 ) ! move
        atm(j+1) = atm(j)
        dst(j+1) = dst(j)
        j = j-1
      ENDDO
      atm(j+1) = at       ! return
      dst(j+1) = d
    ENDDO

    ! ---------------------------------------------------------------------- END
  END SUBROUTINE sort_by_distance
  
  
  SUBROUTINE invert_order( atm )
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    TYPE(atom) :: at
    INTEGER    :: i,j,nat
    nat = SIZE(atm)
    DO i = 1,nat/2
      j = nat+1-i
      at = atm(j)
      atm(j) = atm(i)
      atm(i) = at
    ENDDO
  END SUBROUTINE invert_order


  subroutine compare_pbc( geom , gref , mask , orphan , widow )
    use params , only: pi_
    use algebra3d, only: value, angle3d
    use parse, only: print_integers , print_human_float , group_elem , print_floats
    use types, only: pbc_convert
    ! --------------------------------------------------------------------------
    ! Compare atoms in geometry geom with a reference gref. Order atoms in geom
    ! to match gref as much as possible. 
    ! 
    ! mask: matching indices
    ! orphan: unmatched indices in geom
    ! widow: unmatched indices in gref
    !
    ! NOTE: it might produce incorrect results with terrible unit cell choices
    !       (angles > 135 degree or angles < 45 degree)
    ! --------------------------------------------------------------------------
    type(pbc) , intent(inout)                        :: geom      &
                                                      , gref

    integer   , intent(out) , allocatable , optional :: mask(:)   &
                                                      , orphan(:) &
                                                      , widow(:)

    integer  :: i,j,k,l,m,n,o,p
    integer  :: o_ , w_

    real(dp) :: rdiff(3) , vdiff(3) , adiff(3) , bdiff(3)

    real(dp) :: dist2     ( size(geom%atm) )
    logical  :: test_o    ( size(geom%atm) )
    integer  :: internal_o( size(geom%atm) )

    real(dp) :: mdist2    ( size(gref%atm) )
    integer  :: mloc      ( size(gref%atm) )
    integer  :: internal_w( size(gref%atm) )

    integer , allocatable :: fgeom(:,:) , fgref(:,:)

    character(len=*), parameter :: abc='abc'

    ! --------------------------------------------------------------------------
    ! check unit cells
    vdiff(:) = (/  value(geom%box(:,1))  ,  value(geom%box(:,2))  ,  value(geom%box(:,3))  /)
    rdiff(:) = (/  value(gref%box(:,1))  ,  value(gref%box(:,2))  ,  value(gref%box(:,3))  /)
    adiff(:) = (/  angle3d( geom%box(:,2) , geom%box(:,3) )/pi_*180.0d0  ,&
                   angle3d( geom%box(:,3) , geom%box(:,1) )/pi_*180.0d0  ,&
                   angle3d( geom%box(:,1) , geom%box(:,2) )/pi_*180.0d0  /)
    bdiff(:) = (/  angle3d( gref%box(:,2) , gref%box(:,3) )/pi_*180.0d0  ,&
                   angle3d( gref%box(:,3) , gref%box(:,1) )/pi_*180.0d0  ,&
                   angle3d( gref%box(:,1) , gref%box(:,2) )/pi_*180.0d0  /)
    do i=1,3
      if( abs( vdiff(i)-rdiff(i) )/vdiff(i) > 0.02d0 ) &
          write(stderr,'(a)') '[compare pbc] Warning, '//abc(i:i)//' vectors are different, ' &
                                               // trim(print_human_float( vdiff(i) , 3 ))//' vs. ' &
                                               // trim(print_human_float( rdiff(i) , 3 ))
      if( abs( adiff(i)-bdiff(i) ) > 2.0d0 ) &
          write(stderr,'(a)') '[compare pbc] Warning, '//abc(i:i)//' angles are different, ' &
                                               // trim(print_human_float( adiff(i) , 2 ))//' vs. ' &
                                               // trim(print_human_float( bdiff(i) , 2 ))
    end do

    ! --------------------------------------------------------------------------
    ! rearrange input

    ! run in direct coordinates (requires decent unit cells as only neighboring cells accounted for)
    call pbc_convert( geom , .false. )
    call pbc_convert( gref , .false. )

    ! save original order
    do i = 1,size(gref%atm)
      gref%atm(i)%t = i
    end do

    ! sort by atom
    call sort_one( 'e+' , geom%atm )
    call sort_one( 'e+' , gref%atm )

    ! get total formula
    call group_elem( geom%atm , fgeom )
    call group_elem( gref%atm , fgref )

    ! convert formula to array indices (last entry of each element group)
    do i = 2,size(fgeom,2)
      fgeom(2,i) = fgeom(2,i) + fgeom(2,i-1)
    end do
    do i = 2,size(fgref,2)
      fgref(2,i) = fgref(2,i) + fgref(2,i-1)
    end do
    ! --------------------------------------------------------------------------
    ! get relevant quantities

    ! loop through groups of elements
    ! i-k-m: fgeom, j-l-n: gref
    test_o(:) = .true.
    o_        =  0
    w_        =  0

    k = 1
    l = 1
    do o = 1,size(fgref,2)
  
      ! find same element groups
      do p = 1,size(fgeom,2)
        if( fgref(1,o) == fgeom(1,p) ) exit
      end do

      m = fgref(2,o)
      n = fgeom(2,p)

      ! skip if elements not equal
      if( fgeom(1,o) == fgref(1,p) ) then
        
        do i = k,m
          do j = l,n
            ! calculate distances
            vdiff(:) = geom%atm(j)%r - gref%atm(i)%r
            vdiff(:) = vdiff(:) - nint( vdiff(:) )     ! * include pbc
            vdiff(:) = matmul( gref%box(:,:) , vdiff ) ! * convert to cartesian
            dist2(j) = sum( vdiff(:)**2 )              ! * calculate distance
          end do
          ! get minimum distances 
          mloc(i) = MINLOC( dist2(l:n) , 1 ) + l-1
          j       = mloc(i)
          mdist2(i) = dist2(j)
!          write(stderr,'(a)') 'DEBUG mloc a '//print_integers(mloc(i:i))//'   '//print_floats(sqrt(mdist2(i:i)),2,.true.)//'   '&
!                                             //print_floats(sqrt(dist2(l:n)),2,.true.)


          ! assess redundancies
          ! ===================
          ! * mloc(geom) may point at the same atom in gref for several times
          !   -> leads to orphans when removed
          ! * not all atoms in gref may be found in mloc(geom)
          !   -> leads to widows
          ! strongly depends on user's intentions of how to proceed. at this
          ! stage, orphans and widows are filling voids in the list in order
          ! produce as compatible masks as possible, and orphan/widow arrays
          ! are provided as output

          ! find redundancies: 'test_o' will trigger every redundant
          !                     occurrence in minloc.
          !                    'internal_o' collects a list of orphans
          !                    'internal_w' collects a list of widows
          !                    'o_' and 'w_' will keep track of the number of
          !                     detected orphans and widows
          if( test_o( mloc(i) ) ) then
            ! first occurrence
            test_o( mloc(i) ) = .false.

          else
            ! redundant occurrence
            do j = i-1,k,-1
              if( mloc(i) == mloc(j) ) then
                ! keep closer one of two occurences, the other is a widow
                w_ = w_ + 1
                if( mdist2(i) < mdist2(j) ) then
                  mloc(j)        = 0
                  internal_w(w_) = j
                else
                  mloc(i)        = 0
                  internal_w(w_) = i
                end if
                
                exit  ! there are never more than two redundancies up to 'i'

              end if
            end do
          end if

        end do ! i = k,m
        
        do j = l,n
          if( test_o(j) ) then
            ! orphan detected
            geom%atm(j)%t = 0
            o_ = o_ + 1
            internal_o(o_) = j
          end if
        end do
        
      end if ! equal elements

      k = m+1
      l = n+1
    
    end do

    ! set up tag
    k = 0
    do i = 1,size(gref%atm)
      j = mloc(i)
      if( j > 0 ) then
        ! align atom in geom with closest atom image in gref
        rdiff(:)         = geom%atm(j)%r - gref%atm(i)%r
        geom%atm(j)%r(:) = geom%atm(j)%r - nint( rdiff )
        geom%atm(j)%t    = gref%atm(i)%t
      else if( k < min( o_ , w_ ) ) then
        k = k+1
        geom%atm(internal_o(k))%t  =  gref%atm(internal_w(k))%t
      end if
    end do
    if( o_ < w_ ) then
      ! collapse remaining widows
      do i = o_+1 , w_
        k = gref%atm(internal_w(i))%t

        do j = 1,size(geom%atm)

          l = geom%atm(j)%t
          geom%atm(j)%t = merge( l-1 , l , l > k )

        end do
      end do
    end if
    if( o_ > w_ ) then
      ! add remaining orphans in the end
      p = size(geom%atm)
      do j = o_ , w_+1 , -1
        geom%atm(internal_o(j))%t = p
        p = p-1
      end do
    end if
!    if( o_ <= w_ ) then
!      ! * fill all widows with orphans
!      do p = 1 , o_
!        geom%atm(internal_o(p))%t = gref%atm(internal_w(p))%t
!      end do
!      ! * collapse remaining widows
!      do p = o_+1 , w_
!        do o = 1,size(geom%atm(:))
!          geom%atm(o)%t = merge( geom%atm(o)%t-1 , geom%atm(o)%t , geom%atm(o)%t > gref%atm(internal_w(p))%t )
!        end do
!      end do
!    else
!      ! * fill first widows with orphans
!      do p = 1 , w_
!        geom%atm(internal_o(p))%t = gref%atm(internal_w(p))%t
!      end do
!      ! * add remaining orphans in the end
!      o = size(geom%atm)
!      do p = o_ , w_+1 , -1
!        geom%atm(internal_o(p))%t = o
!        o = o-1
!      end do
!    end if

    do i=1,size(geom%atm)
      if(geom%atm(i)%t == 0 ) print*,'DEBUG',i
    end do

    ! set up orphans
    if( present(orphan) ) then
      if( allocated(orphan) )  deallocate(orphan)
      allocate( orphan(o_) )
      orphan(:) = internal_o(:o_)
    end if

    ! set up widows
    if( present(widow) ) then
      if( allocated(widow) )  deallocate(widow)
      allocate( widow(w_) )
      widow(:) = internal_w(:w_)
    end if
    
    ! set up mask
    if( present(mask) ) then
      if( allocated(mask) )  deallocate(mask)
      k = 0
      do i = 1,size(mloc(:))
        if( mloc(i) /= 0 ) then
          k = k+1
          mloc(k) = mloc(i)
        end if
      end do
      allocate( mask(k) )
      mask(:) = mloc(:k)
    end if

  end subroutine compare_pbc


  SUBROUTINE compare_atoms( atm , refatm , mask , orphan , parent )
    ! --------------------------------------------------------------------------
    ! Compare two similar systems. Identify new, matching and missing atoms
    !
    ! mask:  matching atoms - useful to reorder atoms in atm consistent with
    !        refatm
    ! orphan:  new atoms in atm  /  missing atoms in refatm
    ! parent:  missing atoms in atm  /  new atoms in refatm
    !
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(IN)                :: atm(:) , refatm(:)
    INTEGER    , INTENT(OUT) , ALLOCATABLE :: mask(:)
    ! list of remainder atoms (no close counterpart)
    INTEGER    , INTENT(OUT) , ALLOCATABLE :: orphan(:) , parent(:)

    REAL(dp) :: dist2  ( SIZE(atm)    )
    REAL(dp) :: bestval( SIZE(refatm) )
    INTEGER  :: bestloc( SIZE(refatm) )
    INTEGER  :: iorphan( SIZE(atm)    )
    INTEGER  :: iparent( SIZE(refatm) )

    REAL(dp) :: tmpr(3)

    LOGICAL  :: test( SIZE(atm) )
    INTEGER  :: b,i,j,k,o,p
    INTEGER  :: nat , refnat

    nat    = SIZE(atm)
    refnat = SIZE(refatm)

    ! deallocation
    IF( ALLOCATED(mask) ) DEALLOCATE(mask)
    ALLOCATE(mask( SIZE(atm) ))

    ! get location of shortest distance atoms
    DO i=1,refnat
      DO j=1,nat
        dist2(j) =  ( atm(j)%r(1) - refatm(i)%r(1) )**2 + &
                    ( atm(j)%r(2) - refatm(i)%r(2) )**2 + &
                    ( atm(j)%r(3) - refatm(i)%r(3) )**2
      ENDDO
      bestloc(i) = MINLOC( dist2(:) , 1 )
      bestval(i) = SQRT( dist2(bestloc(i)) )
    ENDDO

    ! clear redundant values
    ! - keep closest value, reject other one (set to 0)
    ! - note other as parent
    ! - if a redundant value occurs, then one value between 1-nat in bestloc(:) is missing
    ! - values in test array remain true if orphans

    ! none allocated!
    test(:) = .TRUE.
    iorphan(:) = 0
    iparent(:) = 0

    p = 0
    DO i=1,refnat
      b = bestloc(i)
      IF( test(b) ) THEN
        test(b) = .FALSE.
      ELSE
        DO j = 1 , i-1
          IF( b == bestloc(j) ) THEN
            p = p + 1
            IF( bestval(i) < bestval(j) ) THEN
              bestloc(j) = 0
              iparent(p) = j
            ELSE
              bestloc(i) = 0
              iparent(p) = i
            END IF
          END IF
        ENDDO
      END IF
    ENDDO
    ! set up orphans
    o = 0
    DO i = 1 , nat
      IF( test(i) ) THEN
        o = o + 1
        iorphan(o) = i
      END IF
    ENDDO

    ! SET UP MASK
    ! shift gaps by parents
    j = 1
    DO i = 1,refnat
      mask(j) = bestloc(i)
      IF( bestloc(i) /= 0 ) j = j + 1
    ENDDO

    ! append orphans
    mask(j:nat) = iorphan(1:o)

    ! set parents for output
    IF( ALLOCATED(parent) ) DEALLOCATE(parent)
    IF( p > 0 ) THEN
      ALLOCATE( parent(p) )
      parent(:) = iparent(1:p)
    END IF

    ! set orphans for output
    IF( ALLOCATED(orphan) ) DEALLOCATE(orphan)
    IF( o > 0 ) THEN
      ALLOCATE( orphan(o) )
      orphan(:) = iorphan(1:o)
    END IF
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE compare_atoms


  SUBROUTINE shuffle_list( mask )
    ! --------------------------------------------------------------------------
    ! RETURN A MASK IN RANDOM ORDER (modern Fischer-Yates)
    ! --------------------------------------------------------------------------
    INTEGER , INTENT(INOUT) :: mask(:)
    INTEGER :: i , rn , tmp
    REAL(dp) :: rnum( SIZE(mask) )
    CALL rnginit()
    CALL RANDOM_NUMBER( rnum )

    DO i = SIZE(mask), 2 , -1
      ! pick random from remainder
      rn = CEILING(rnum(i)*i)

      ! swap
      tmp = mask(i)
      mask(i) = mask(rn)
      mask(rn) = tmp
    ENDDO
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE shuffle_list


  SUBROUTINE shuffle_atoms( atm )
    ! --------------------------------------------------------------------------
    ! RETURN A MASK IN RANDOM ORDER (modern Fischer-Yates)
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    TYPE(atom) :: a
    INTEGER :: i , rn
    REAL(dp) :: rnum( SIZE(atm) )
    CALL rnginit()
    CALL RANDOM_NUMBER( rnum )

    DO i = SIZE(atm), 2 , -1
      ! pick random from remainder
      rn = CEILING(rnum(i)*i)

      ! swap
      a = atm(i)
      atm(i) = atm(rn)
      atm(rn) = a
    ENDDO
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE shuffle_atoms


  SUBROUTINE mask2tag( mask , atm , stat )
    ! --------------------------------------------------------------------------
    ! write content of mask into the tag field of atoms
    ! --------------------------------------------------------------------------
    INTEGER    , INTENT(IN)    :: mask(:)
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    INTEGER    , INTENT(OUT)   :: stat
    INTEGER    :: i

    IF( SIZE(mask) /= SIZE(atm) ) THEN
      WRITE(stderr,'(A)') '[mask2tag] Cannot apply mask - size not equal!'
      RETURN
    END IF

    DO i=1,SIZE(atm)
      atm(i)%t = mask(i)
    ENDDO
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE mask2tag

  
  FUNCTION extract_atoms( selec , set ) RESULT( subset )
    ! --------------------------------------------------------------------------
    ! Return atoms given by selection
    ! --------------------------------------------------------------------------
    INTEGER    , INTENT(IN) :: selec(:)
    TYPE(atom) , INTENT(IN) :: set(:)
    TYPE(atom) :: subset( SIZE(selec) )
    INTEGER    :: i

    ! extract elements and positions
    DO i = 1,SIZE(selec)
      subset(i) = set(selec(i))
    ENDDO
  END FUNCTION extract_atoms


  SUBROUTINE check_mask( mask )
    INTEGER , INTENT(in) :: mask(:)
    INTEGER :: i,j,n

    n = SIZE(mask)

    DO i = 1,n
      IF( mask(i) > n .OR. mask(i) < 1 ) WRITE(stderr,'(A,I4,A,I4,A)') '[check mask] ERROR: Entry',i,' out of range,',mask(i),'.'
      DO j = 1,i-1
        IF( mask(i) == mask(j) ) WRITE(stderr,'(A,3I4)') '[check mask] ERROR: Redundancy,',mask(i),i,j
      ENDDO
    ENDDO

  END SUBROUTINE check_mask


  SUBROUTINE make_mask( nat , mask , selec )
    ! --------------------------------------------------------------------------
    ! bring all atoms in selec to the top of mask
    ! WARNING: assumes correct mask
    ! --------------------------------------------------------------------------
    INTEGER , INTENT(IN)  :: nat
    INTEGER , INTENT(OUT) , ALLOCATABLE ::  mask( : )
    INTEGER , INTENT(IN)  , ALLOCATABLE :: selec( : )
    INTEGER :: snat , i , j

    IF( ALLOCATED(mask) ) DEALLOCATE( mask )
    snat = SIZE(selec)
    
    ! initialize mask
    ALLOCATE( mask(nat) )
    mask(:) = 0

    ! gaps in old mask
    DO i = 1,snat
      mask(selec(i)) = -1
    ENDDO

    ! set up remainder
    j=nat
    DO i = nat,1,-1
      mask(j) = i
      IF( mask(i) == 0 ) j=j-1
    ENDDO

    ! selec on top
    DO i = 1,snat
      mask(i) = selec(i)
    ENDDO
  END SUBROUTINE make_mask


  FUNCTION print_integer( array , separator ) RESULT( list )
    ! --------------------------------------------------------------------------
    ! return a printable list if integer array
    ! --------------------------------------------------------------------------
    INTEGER , INTENT(IN) , ALLOCATABLE          :: array(:)
    CHARACTER(LEN=1) , INTENT(in) , OPTIONAL    :: separator
    CHARACTER(LEN=1)                            :: sep
    CHARACTER(LEN=:) , ALLOCATABLE              :: list , ilist
    CHARACTER(LEN=str_)                         :: tmp

    INTEGER :: nat , i

    ! set separator
    sep = ' ' 
    IF( PRESENT ( separator ) ) sep = separator

    nat = MERGE( SIZE(array) , 0 , ALLOCATED(array) )

    ALLOCATE( CHARACTER(LEN=str_*nat) :: ilist )
    ilist = ''

    ! fill list
    DO i = 1,nat
      WRITE(tmp,*) array(i)
      ilist = TRIM(ilist)//sep//ADJUSTL(tmp)
    ENDDO

    ! make output
    list = TRIM(ilist(2:))

  END FUNCTION print_integer


  SUBROUTINE sort_integers( ints )
    INTEGER , INTENT(INOUT) :: ints(:)
    INTEGER :: i , j , pint
    
    DO i = 2,SIZE(ints)
      pint = ints(i)
      j = i-1
      DO WHILE( ints(j) > pint .AND. j > 0 ) ! move
        ints(j+1) = ints(j)
        j = j-1
      ENDDO
      ints(j+1) = pint     ! return
    ENDDO
  END SUBROUTINE sort_integers


END MODULE accounting
