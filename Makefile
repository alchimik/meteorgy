
include config.mk

# rules to compile a .o from each .f and .f90
%.o: %.f
	@echo "make $@ from $<"
	$(FC) -ffixed-form $(FFLAGS) -c $< -o $@
%.o: %.f90
	@echo "make $@ from $<"
	$(FC) -std=$(STD) -ffree-form $(FFLAGS) -c $< -o $@


# libraries
FLIB = params.o dsyevj3.o algebra3d.o types.o parse.o accounting.o meteorgy.o file_io.o

# 
all: meteorgy


install:
	cp meteorgy $(BIN)
	ln -sf meteorgy $(BIN)/any2xsf
	ln -sf meteorgy $(BIN)/rewrite
	ln -sf meteorgy $(BIN)/adsorb
	ln -sf meteorgy $(BIN)/geominfo

clean:
	rm -f *.o *.mod $(EXE)

# build executables separately
meteorgy: $(FLIB) cli.o
	$(FC) -std=$(STD) -o $@ $^

test: $(FLIB) test.o
	$(FC) -std=$(STD) -o $@ $^
