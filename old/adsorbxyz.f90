PROGRAM adsorbxyz

 USE params     , ONLY: dp , str_ , lstr_
 USE types      , ONLY: pbc , atom , old_pbc_convert
 USE algebra3d  , ONLY: rot_align , print3dmat , value , nrotmat3d
 USE parse      , ONLY: parse_pair , random_filename , file => split_file , path => split_path
 USE file_io    , ONLY: xyz_read , vasp_read , vasp_write
 USE accounting , ONLY: sort_one
 USE meteorgy   , ONLY: shift_atoms , rotate_atoms => transform_atoms , add_atoms => add_and_remove_atoms

 ! debugging
 USE types   , ONLY: pbc_print , atom_print

 IMPLICIT NONE
 ! ---------------------------------------------------------------------------------------
 ! Adsorb fragment from xyz data on surface. 
 ! Copyright (C) 2019 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ---------------------------------------------------------------------------------------

  CHARACTER(LEN=:) , ALLOCATABLE :: xyzfile ,  vaspfile ,  outfile
  
  CHARACTER(LEN=:) , ALLOCATABLE :: subst_pos ,  subst_bond ,  dihedral_angle
  
  INTEGER    :: pos_x , pos_g , alg_x , alg_g , dihedral
  
  CHARACTER(LEN=4) :: suffix

  TYPE(pbc)                :: geom , g
  TYPE(atom) , ALLOCATABLE :: ads(:)

  REAL(dp)   :: dist , angle , dangle
  REAL(dp)   :: shift(3) , ads_bond(3) , geom_bond(3) , geom_pos(3)

  INTEGER , ALLOCATABLE :: rmselec(:)
  
  INTEGER    :: i , length , stat

  LOGICAL    :: failure = .FALSE.


  ! ----------------------------------------------------------------------------
  ! read input files
  ! ----------------------------------------------------------------------------
  ALLOCATE( CHARACTER( LEN = lstr_ ) :: xyzfile , vaspfile )
  ALLOCATE( CHARACTER( LEN =  str_ ) :: subst_pos , subst_bond , dihedral_angle )

  ! xyz file
  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=xyzfile , STATUS=stat )
  xyzfile = TRIM(xyzfile)
  IF( stat /= 0 ) THEN
    WRITE(*,'(A)') 'Cannot read xyz-file ['//xyzfile//']!!'
    failure = .TRUE.
  ENDIF

  CALL xyz_read( xyzfile , g , stat )
  IF( stat /= 0 ) failure = .TRUE.
  ads = g%atm
  DEALLOCATE( g%atm , g%comment )


  ! poscar
  CALL GET_COMMAND_ARGUMENT( 2 , VALUE=vaspfile , STATUS=stat )
  vaspfile = TRIM(vaspfile)
  IF( stat /= 0 ) THEN
    WRITE(*,'(A)') 'Cannot read vasp-file ['//vaspfile//']!'
    failure = .TRUE.
  ENDIF

  CALL vasp_read( vaspfile , geom , stat )
  IF( stat /= 0 ) failure = .TRUE.

  ! convert ASAP
  CALL old_pbc_convert( geom , 0 , stat )

  
  ! ----------------------------------------------------------------------------
  ! get shifting (and rotation) parameters
  ! ----------------------------------------------------------------------------

  ! surface atoms to match to shift in position (xyz atom will be removed, in case of meta data)
  CALL GET_COMMAND_ARGUMENT( 3 , VALUE=subst_pos , STATUS=stat )

  CALL parse_pair( subst_pos , SIZE(ads) , geom%nat , pos_x , pos_g , stat )
  IF( stat /= 0 ) failure = .TRUE.


  ! atoms on surface and in xyz to align bond (surface atom will be removed)
  alg_x = 0
  CALL GET_COMMAND_ARGUMENT( 4 , VALUE=subst_bond , STATUS=stat )

  IF( stat == 0 ) THEN
    CALL parse_pair( subst_bond , SIZE(ads) , geom%nat , alg_x , alg_g , stat )
    IF( stat /= 0 ) failure = .TRUE.
  END IF
  
  ! dihedral angle (overloaded, small numbers return multiple geometries)
  CALL GET_COMMAND_ARGUMENT( 5 , VALUE=dihedral_angle , STATUS=stat )

  dihedral = 0
  IF( stat == 0 ) THEN
    READ( dihedral_angle , * , iostat = stat ) dihedral
    IF( stat /= 0 ) failure = .TRUE.
  END IF


  ! ----------------------------------------------------------------------------
  ! rotate to align with bond, show simple use case of rotate_atoms
  ! ----------------------------------------------------------------------------
  IF( alg_x > 0 ) THEN
    geom_pos  = geom%atm(alg_g)%r
    geom_bond = geom%atm(alg_g)%r - geom%atm(pos_g)%r
     ads_bond =      ads(alg_x)%r -      ads(pos_x)%r

    IF( value(geom_bond) < 0.5D0 .OR. value(ads_bond) < 0.5D0 ) failure = .TRUE.

    CALL rotate_atoms( rot_align( ads_bond , geom_bond ) , ads )

    ALLOCATE( rmselec(2) )
    rmselec = (/ pos_x , SIZE(ads)+alg_g /)
  ELSE
    ALLOCATE( rmselec(1) )
    rmselec = (/ pos_x /)
  END IF


  ! ----------------------------------------------------------------------------
  ! print quick reference
  ! ----------------------------------------------------------------------------
  IF( failure ) THEN
    WRITE(*,'( )')
    WRITE(*,'(A)') 'Usage:  adsorbxyz <xyz-file> <vasp poscar> <m>,<n> [<o>,<p> [<q>]]'
    WRITE(*,'( )')
    WRITE(*,'(A)') ' m  - index of surface atom in xyz file'
    WRITE(*,'(A)') ' n  - index of surface atom in vasp poscar'
    WRITE(*,'( )')
    WRITE(*,'(A)') 'Optional:  Substitute atom with fragment.  Preserve bond orientation, without'
    WRITE(*,'(A)') '           this, the substitution requires correctly rotated xyz input. Input'
    WRITE(*,'(A)') '           fragment will be rotated to match the m-o bond with n-p bond.'
    WRITE(*,'( )')
    WRITE(*,'(A)') '           The dihedral angle between surface and fragment along the defined n-o'
    WRITE(*,'(A)') '           bond can be manipulated manually by parameter q  (values of 12 and'
    WRITE(*,'(A)') '           below return an equivalent number of candidate structures)'
    WRITE(*,'( )') 
    WRITE(*,'(A)') ' o  - atom that forms a bond with m'
    WRITE(*,'(A)') ' p  - index of surface atom to be substituted'
    WRITE(*,'(A)') ' q  - number of candidates (rotate along dihedral angle)'
    WRITE(*,'( )') 
    STOP 1
  ENDIF


  ! ----------------------------------------------------------------------------
  ! shift atoms, merge with geom, sort elements
  ! ----------------------------------------------------------------------------
  DO i = 1,SIZE(ads)
    ads%c = .TRUE.
  ENDDO
  shift =  geom%atm(pos_g)%r - ads(pos_x)%r

  CALL shift_atoms( shift , ads )

  ! saves ads atoms in the beginning of new array
  CALL add_atoms( geom , ads , rmselec )



  ! ----------------------------------------------------------------------------
  ! make related filename, 'POSCAR.*', from '*' or '*.xyz'
  ! ----------------------------------------------------------------------------

  length = LEN(xyzfile)

  IF( xyzfile == '-' ) THEN
    outfile = 'POSCAR.'//random_filename( 3 )

  ELSEIF( xyzfile(length-3:length) == '.xyz' ) THEN
    outfile = path(xyzfile)//'POSCAR.'//file(xyzfile(1:length-4))

  ELSE
    outfile = path(xyzfile)//'POSCAR.'//file(xyzfile)

  ENDIF

  ! ----------------------------------------------------------------------------
  ! rotate around new bond axis by given angle, or several times
  ! ----------------------------------------------------------------------------
  IF( dihedral > 12 ) THEN
    angle = REAL(dihedral,dp)
    CALL rotate_atoms( nrotmat3d( geom_bond , REAL(dihedral,dp) )   &
                     , geom%atm(1:SIZE(ads)-1)   &
                     , zero = geom_pos )

    CALL sort_one( 'e+' , geom%atm )
    CALL vasp_write( outfile , geom , stat )

  ELSE IF( dihedral > 0 ) THEN
    dangle = 360.0D0/dihedral
    angle  = 0.0D0
    DO i = 1,dihedral

      WRITE(suffix,'("-",I3.3)') FLOOR(angle+0.5D0)

      CALL rotate_atoms( nrotmat3d( geom_bond , REAL(dihedral,dp) )   &
                       , geom%atm(1:SIZE(ads)-1)   &
                       , zero = geom_pos )

      CALL sort_one( 'e+' , geom%atm )
      CALL vasp_write( outfile//suffix , geom , stat )
    
      angle  = angle + dangle
    ENDDO
  
  ELSE
    CALL sort_one( 'e+' , geom%atm )
    CALL vasp_write( outfile , geom , stat )

  END IF
  ! deallocate stuff
  DEALLOCATE( geom%atm )

END PROGRAM adsorbxyz
