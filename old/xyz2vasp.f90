PROGRAM xyz2vasp

 USE params     , ONLY: dp , lstr_
 USE types      , ONLY: pbc , old_pbc_convert
 USE file_io    , ONLY: xyz_read , vasp_write
 USE parse      , ONLY: random_filename , file => split_file , path => split_path
 USE accounting , ONLY: sort_one
 USE meteorgy   , ONLY: fix_molecule , shift_atoms , center_xyz , make_box

 IMPLICIT NONE
 ! ---------------------------------------------------------------------------------------
 ! Convert molecular system in xyz-file to a vasp geometry with user-defined box size
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ---------------------------------------------------------------------------------------

  CHARACTER(LEN=:)  , ALLOCATABLE  :: infile
  CHARACTER(LEN=:)  , ALLOCATABLE  :: outfile
  CHARACTER(LEN=:)  , ALLOCATABLE  :: query

  TYPE(pbc) :: geom

  REAL(dp)  :: dist
  REAL(dp)  :: shift(3)

  INTEGER   :: length
  INTEGER   :: stat


  ! ----------------------------------------------------------------------------
  ! parse command line
  ! ----------------------------------------------------------------------------

  ALLOCATE( CHARACTER(LEN=lstr_) :: infile , query )

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=infile , STATUS=stat )
  IF( stat /= 0 ) THEN
    WRITE(*,*) 'Usage:  vasp2xyz <infile> [<distance>]'
    WRITE(*,*) '    - Distance from periodic images, default: 20 Angstrom.'
    STOP
  ENDIF
  infile = TRIM(infile)

  CALL GET_COMMAND_ARGUMENT( 2 , VALUE=query  , STATUS=stat )
  IF( stat /= 0 ) THEN
    dist = 20.0
  ELSE
    READ(query,*) dist
  ENDIF


  ! ----------------------------------------------------------------------------
  ! read POSCAR
  ! ----------------------------------------------------------------------------
  CALL xyz_read( infile , geom , stat )
  IF( stat /= 0 ) STOP 1


  ! ----------------------------------------------------------------------------
  ! create box, move system to middle of the box
  ! ----------------------------------------------------------------------------

  geom%sys = 0
  geom%nat = SIZE(geom%atm)

  CALL make_box( geom%box , .TRUE. , dist )

  shift  =   ( geom%box(:,1)+geom%box(:,2)+geom%box(:,3) )/2.0D0   &
               - center_xyz( geom%atm )

  CALL shift_atoms( shift , geom%atm )
  CALL sort_one( 'e+' , geom%atm )


  ! ----------------------------------------------------------------------------
  ! make related filename, 'POSCAR.*', from '*' or '*.xyz'
  ! ----------------------------------------------------------------------------

  length = LEN(infile)

  IF( infile == '-' ) THEN
    outfile = 'POSCAR.'//random_filename( 3 )

  ELSEIF( infile(length-3:length) == '.xyz' ) THEN
    outfile = path(infile)//'POSCAR.'//file(infile(1:length-4))

  ELSE
    outfile = path(infile)//'POSCAR.'//file(infile)

  ENDIF

  ! ----------------------------------------------------------------------------
  ! write POSCAR
  ! ----------------------------------------------------------------------------
  CALL vasp_write( outfile , geom , stat )

  ! deallocate stuff
  DEALLOCATE( geom%atm )

END PROGRAM xyz2vasp
