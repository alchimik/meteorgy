PROGRAM vaspinfo
! ----------------------------------------------------------------------------------------
! Check geometry. Query information
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------
  USE params  , ONLY: lstr_
  USE types   , ONLY: pbc , pbc_print , box_print , atom_print , old_pbc_convert
  USE file_io , ONLY: vasp_read
  IMPLICIT NONE
  ! filenames
  CHARACTER(LEN=:) , ALLOCATABLE :: infile , query

  ! cell data
  TYPE(pbc) :: geom
  ! control
  INTEGER   :: stat

  ALLOCATE( CHARACTER(LEN=lstr_) :: infile , query )

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=query  , STATUS=stat )
  IF( stat /= 0 ) THEN
    WRITE(*,'(A)') 'Usage:  vaspinfo <option> <VASP geometry>'
    WRITE(*,'(A)') '   - option = box, cartesian, fractional'
    STOP
  ENDIF
  CALL GET_COMMAND_ARGUMENT( 2 , VALUE=infile , STATUS=stat )
  IF( stat /= 0 ) THEN
    WRITE(*,*) 'Cannot read file name.'
    STOP 2
  ENDIF

  query  = TRIM(query)
  infile = TRIM(infile)

  ! ----------------------------------------------------------------------------
  ! READ INPUT
  ! ----------------------------------------------------------------------------

  ! read POSCAR
  CALL vasp_read( infile , geom , stat )
  IF( stat /= 0 ) THEN
    WRITE(*,*) 'Cannot read input file.'
    STOP 3
  ENDIF

  ! ----------------------------------------------------------------------------
  ! DO STUFF BELOW
  ! ----------------------------------------------------------------------------

  IF( query == '-' ) THEN
    ! do nothing
  ELSEIF( query == 'box' ) THEN
    CALL box_print( geom%box )
  ELSEIF( query(1:4) == 'cart' ) THEN
    CALL old_pbc_convert( geom , 0 , stat )
    IF( stat /= 0 ) THEN
      WRITE(*,*) 'Unknown system.'
      STOP 255
    ENDIF
    CALL pbc_print( geom )
  ELSEIF( query(1:4) == 'frac' ) THEN
    CALL old_pbc_convert( geom , 3 , stat )
    IF( stat /= 0 ) THEN
      WRITE(*,*) 'Unknown system.'
      STOP 255
    ENDIF
    CALL pbc_print( geom )
  ELSEIF( query(1:4) == 'atom' ) THEN
    WRITE(*,*) 'W.I.P. will list atom info...'
    !CALL atom_print( geom%atm , geom%lc , geom%lv )
  ENDIF

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM vaspinfo
