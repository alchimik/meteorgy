PROGRAM rewrite

 USE params     , ONLY: lstr_
 USE types      , ONLY: pbc , pbc_print , atom_print , old_pbc_convert
 USE file_io    , ONLY: dftb_read , dftb_write
 USE accounting , ONLY: sort_one

 IMPLICIT NONE
 ! ----------------------------------------------------------------------------------------
 ! Rewrite a VASP input. If desired, convert between cartesian and fractional coordinates
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ----------------------------------------------------------------------------------------
 
  CHARACTER(LEN=:)  , ALLOCATABLE  :: infile
  CHARACTER(LEN=4)                 :: query

  TYPE(pbc)  :: geom

  INTEGER    :: stat
  INTEGER    :: sys
  INTEGER    :: i
  
  INTEGER , ALLOCATABLE :: elem(:)

  ALLOCATE( CHARACTER(LEN=lstr_) :: infile )

  ! ----------------------------------------------------------------------------
  ! PARSE COMMAND LINE
  ! ----------------------------------------------------------------------------

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=query , STATUS=stat )
  IF( stat /= 0 ) THEN
    WRITE(*,'(A)') 'Usage:  rewrite [<option>] <infile>'
    WRITE(*,'( )')
    WRITE(*,'(A)') ' options: elem[ents], srt{abcxyz}, cart[esian], frac[tional], dire[ct], verb[ose]'
    WRITE(*,'( )')
    WRITE(*,'(A)') ' example: sort atoms in CONTCAR along b axis'
    WRITE(*,'(20X,A)') 'rewrite CONTCAR srtb'
    STOP
  ENDIF
  query = TRIM(query)

  CALL GET_COMMAND_ARGUMENT( 2 , VALUE=infile  , STATUS=stat )
  IF( stat /= 0 ) infile(:) = query   ! no allocation with (:)
  infile = TRIM(infile)
  
  
  ! ----------------------------------------------------------------------------
  ! READ INPUT
  ! ----------------------------------------------------------------------------

  CALL dftb_read( infile , geom , stat )
  IF( stat /= 0 ) STOP 1

  ! ----------------------------------------------------------------------------
  ! DO STUFF BELOW
  ! ----------------------------------------------------------------------------
  sys = geom%sys

  SELECT CASE( query(1:4) )
    CASE( 'elem' )
      CALL sort_one( 'e+' , geom%atm )
    CASE( 'srta' )
      CALL old_pbc_convert( geom , 3 , stat )
      CALL sort_one( '1-' , geom%atm )
    CASE( 'srtb' )
      CALL old_pbc_convert( geom , 3 , stat )
      CALL sort_one( '2-' , geom%atm )
    CASE( 'srtc' )
      CALL old_pbc_convert( geom , 3 , stat )
      CALL sort_one( '3-' , geom%atm )
    CASE( 'srtx' )
      CALL old_pbc_convert( geom , 0 , stat )
      CALL sort_one( '1-' , geom%atm )
    CASE( 'srty' )
      CALL old_pbc_convert( geom , 0 , stat )
      CALL sort_one( '2-' , geom%atm )
    CASE( 'srtz' )
      CALL old_pbc_convert( geom , 0 , stat )
      CALL sort_one( '3-' , geom%atm )
    CASE( 'cart' )
      sys = 0
    CASE( 'frac' )
      sys = 3
    CASE( 'dire' )
      sys = 3
    CASE( 'verb' )
      CALL pbc_print( geom )
  END SELECT
      
  CALL old_pbc_convert( geom , sys , stat )
  ! ----------------------------------------------------------------------------
  ! WRITE OUTPUT
  ! ----------------------------------------------------------------------------

  ! write POSCAR
  CALL dftb_write( infile//'.out' , geom , stat )

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM rewrite
