PROGRAM rewrite

 USE params     , ONLY: lstr_
 USE types      , ONLY: pbc , pbc_print , box_print , atom_print , old_pbc_convert
 USE file_io    , ONLY: vasp_read , dftb_write , random_filename , file , path
 USE accounting , ONLY: sort_one

 IMPLICIT NONE
 ! ----------------------------------------------------------------------------------------
 ! Rewrite a VASP input. If desired, convert between cartesian and fractional coordinates
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ----------------------------------------------------------------------------------------
 
  ! filenames
  CHARACTER(LEN=:) , ALLOCATABLE :: infile , outfile

  LOGICAL   :: loutfile

  TYPE(pbc) :: geom

  INTEGER   :: l
  INTEGER   :: stat
  INTEGER   :: i

  ALLOCATE( CHARACTER(LEN=lstr_) :: infile , outfile )

  ! ----------------------------------------------------------------------------
  ! PARSE COMMAND LINE
  ! ----------------------------------------------------------------------------

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=infile , STATUS=stat )
  IF( stat /= 0 ) THEN
    WRITE(*,*) 'Usage:  vasp2dftb <infile> [<outfile>]'
    STOP
  ENDIF
  infile = TRIM(infile)

  WRITE(outfile,'(A)') ''
  CALL GET_COMMAND_ARGUMENT( 2 , VALUE=outfile , STATUS=stat )
  outfile = TRIM(outfile)

  loutfile = stat == 0
  IF( outfile == '-' )  loutfile = .FALSE.
  
  ! ----------------------------------------------------------------------------
  ! READ INPUT
  ! ----------------------------------------------------------------------------

  CALL vasp_read( infile , geom , stat )
  IF( stat /= 0 ) STOP 1

  ! ----------------------------------------------------------------------------
  ! make related filename, 'POSCAR.*', from '*' or '*.gen'
  ! ----------------------------------------------------------------------------

  IF( .NOT. loutfile ) THEN
    ! use outfile as buffer
    outfile =  file(infile)

    IF( outfile(1:7) == 'POSCAR.' .AND. LEN(outfile) > 7 ) THEN
      outfile = path(infile)//outfile(8:)

    ELSEIF( outfile(1:8) == 'CONTCAR.' .AND. LEN(outfile) > 8 ) THEN
      outfile = path(infile)//outfile(9:)
    ELSE
      outfile = random_filename( 6 )

    ENDIF
  ENDIF

  l = LEN(outfile)
  IF( outfile(l-3:l) /= '.gen' )  outfile = outfile//'.gen'

  ! ----------------------------------------------------------------------------
  ! write POSCAR
  ! ----------------------------------------------------------------------------
  CALL dftb_write( outfile , geom , stat )

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM rewrite
