PROGRAM rewrite

 USE params     , ONLY: lstr_
 USE types      , ONLY: pbc , pbc_print , atom_print , old_pbc_convert
 USE file_io    , ONLY: dftb_read , vasp_write , random_filename , file , path
 USE accounting , ONLY: sort_one

 IMPLICIT NONE
 ! ----------------------------------------------------------------------------------------
 ! Rewrite a VASP input. If desired, convert between cartesian and fractional coordinates
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ----------------------------------------------------------------------------------------
 
  CHARACTER(LEN=:)  , ALLOCATABLE  :: infile
  CHARACTER(LEN=:)  , ALLOCATABLE  :: outfile

  TYPE(pbc)  :: geom

  INTEGER   :: length
  INTEGER    :: stat
  INTEGER    :: i

  ALLOCATE( CHARACTER(LEN=lstr_) :: infile )

  ! ----------------------------------------------------------------------------
  ! PARSE COMMAND LINE
  ! ----------------------------------------------------------------------------

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=infile  , STATUS=stat )
  IF( stat /= 0 ) THEN
    WRITE(*,'( )')
    WRITE(*,'(A)') 'Usage:  dftb2vasp <infile>'
    WRITE(*,'( )')
    STOP
  ENDIF
  infile = TRIM(infile)
  
  
  ! ----------------------------------------------------------------------------
  ! READ INPUT
  ! ----------------------------------------------------------------------------

  CALL dftb_read( infile , geom , stat )
  IF( stat /= 0 ) STOP 1

  ! ----------------------------------------------------------------------------
  ! make related filename, 'POSCAR.*', from '*' or '*.xyz'
  ! ----------------------------------------------------------------------------

  length = LEN(infile)

  IF( infile == '-' ) THEN
    outfile = 'POSCAR.'//random_filename( 3 )

  ELSEIF( infile(length-3:length) == '.gen' ) THEN
    outfile = path(infile)//'POSCAR.'//file(infile(1:length-4))

  ELSE
    outfile = path(infile)//'POSCAR.'//file(infile)

  ENDIF
  ! ----------------------------------------------------------------------------
  ! write POSCAR
  ! ----------------------------------------------------------------------------
  CALL vasp_write( outfile , geom , stat )

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM rewrite
