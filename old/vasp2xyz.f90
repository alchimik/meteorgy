PROGRAM vasp2xyz
 USE params     , ONLY: lstr_
 USE types      , ONLY: pbc , pbc_convert
 USE file_io    , ONLY: vasp_read , xyz_write
 USE parse      , ONLY: random_filename , path => split_path , file => split_file
 USE meteorgy   , ONLY: fix_molecule , shift_atoms , center_xyz
 USE accounting , ONLY: sort_one

 IMPLICIT NONE
 ! ----------------------------------------------------------------------------------------
 ! Convert POSCAR or CONTCAR files to *.xyz files
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ----------------------------------------------------------------------------------------

  ! filenames
  CHARACTER(LEN=:) , ALLOCATABLE :: infile , outfile

  ! cell data
  TYPE(pbc) :: geom
  ! control
  INTEGER   :: l , stat
  LOGICAL   :: loutfile

  ALLOCATE( CHARACTER(LEN=lstr_) :: infile , outfile )
  ! ----------------------------------------------------------------------------
  ! PARSE COMMAND LINE
  ! ----------------------------------------------------------------------------

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=infile , STATUS=stat )
  IF( stat /= 0 ) THEN
    WRITE(*,*) 'Usage:  vasp2xyz <infile> [<outfile>]'
    STOP
  ENDIF
  infile = TRIM(infile)

  WRITE(outfile,'(A)') ''
  CALL GET_COMMAND_ARGUMENT( 2 , VALUE=outfile , STATUS=stat )
  outfile = TRIM(outfile)

  loutfile = stat == 0
  IF( outfile == '-' )  loutfile = .FALSE.

  ! ----------------------------------------------------------------------------
  ! READ INPUT
  ! ----------------------------------------------------------------------------

  ! read POSCAR
  CALL vasp_read( infile , geom , stat )
  IF( stat /= 0 ) STOP 1

  ! ----------------------------------------------------------------------------
  ! convert to xyz, fix split molecules, shift center of mass to origin
  ! ----------------------------------------------------------------------------

  CALL fix_molecule( geom )

  !CALL pbc_convert( geom , .TRUE. )

  !CALL shift_atoms( -center_xyz(geom%atm) , geom%atm )

  !CALL sort_one( 't+' , geom%atm )

  ! ----------------------------------------------------------------------------
  ! WRITE OUTPUT
  ! ----------------------------------------------------------------------------
  IF( .NOT. loutfile ) THEN
    ! use outfile as buffer
    outfile =  file(infile)

    IF( outfile(1:7) == 'POSCAR.' .AND. LEN(outfile) > 7 ) THEN
      outfile = path(infile)//outfile(8:)

    ELSEIF( outfile(1:8) == 'CONTCAR.' .AND. LEN(outfile) > 8 ) THEN
      outfile = path(infile)//outfile(9:)
    ELSE
      outfile = random_filename( 6 )

    ENDIF
  ENDIF

  l = LEN(outfile)
  IF( outfile(l-3:l) /= '.xyz' )  outfile = outfile//'.xyz'

  ! write POSCAR
  CALL xyz_write( outfile , geom , stat )

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM vasp2xyz
