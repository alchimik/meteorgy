PROGRAM vasp2xsf

 USE params  , ONLY: lstr_ , out2err , noerr
 USE types   , ONLY: pbc , pbc_print , old_pbc_convert
 USE file_io , ONLY: dftb_read , xsf_write

 IMPLICIT NONE
 ! ---------------------------------------------------------------------------------------
 ! Interface tool to be used for XCrysden. Alternativly primitive converter
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ---------------------------------------------------------------------------------------

  ! filenames
  CHARACTER(LEN=:) , ALLOCATABLE :: infile

  ! cell data
  TYPE(pbc) :: geom
  ! control
  INTEGER   :: stat


  ALLOCATE( CHARACTER(LEN=lstr_) :: infile )

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=infile , STATUS=stat )

  IF( stat /= 0 ) THEN
    WRITE(*,'(A)') 'Used as direct interface to XCrysden. Add the following lines'
    WRITE(*,'(A)') 'to your custom-definitions:'
    WRITE(*,'(A)') ''
    WRITE(*,'(A)') 'addOption --dftb dftb2xsf {'
    WRITE(*,'(A)') '                    load structure from vasp file format'
    WRITE(*,'(A)') '}'
    STOP
  ENDIF

  infile = TRIM(infile)

  CALL noerr
  CALL out2err

  ! ----------------------------------------------------------------------------
  ! READ INPUT
  ! ----------------------------------------------------------------------------

  ! read POSCAR
  CALL dftb_read( infile , geom , stat )
  IF( stat /= 0 ) STOP 1

  ! ----------------------------------------------------------------------------
  ! WRITE OUTPUT
  ! ----------------------------------------------------------------------------

  ! write POSCAR to stdout
  CALL xsf_write( '-' , geom , stat )

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM vasp2xsf
