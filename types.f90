module types

use params    , only: dp , stderr , pi_
  
 implicit none
 private     ::      dp , stderr , pi_
 public

! ----------------------------------------------------------------------------------------
! I/O routines to access/write vasp geometries and xyz files
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------

 integer , parameter  ::  maxsorts_= 100       ! maximum amount of atom sorts

 integer                   , parameter :: list_length=111 , sym_length=2
 character(len=sym_length) , parameter :: anon_element='Xx'
 real(dp)                  , parameter :: anon_weight =0.0d0
 
 character(len=2)          , parameter :: list_symbols(*) = (/ &
   'H ', 'He', 'Li', 'Be', 'B ', 'C ', 'N ', 'O ', 'F ', 'Ne', &
   'Na', 'Mg', 'Al', 'Si', 'P ', 'S ', 'Cl', 'Ar', 'K ', 'Ca', &
   'Sc', 'Ti', 'V ', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', &
   'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y ', 'Zr', &
   'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', &
   'Sb', 'Te', 'I ', 'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', &
   'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', &
   'Lu', 'Hf', 'Ta', 'W ', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', &
   'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', &
   'Pa', 'U ', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', &
   'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', &
   'Rg'/)

 real(dp)                  , parameter :: list_weights(*) = (/             &
     1.0079d0,   4.0026d0,   6.9410d0,   9.0122d0,  10.8110d0,  12.0107d0, &
    14.0067d0,  15.9994d0,  18.9984d0,  20.1797d0,  22.9897d0,  24.3050d0, &
    26.9815d0,  28.0855d0,  30.9738d0,  32.0650d0,  35.4530d0,  39.9480d0, &
    39.0983d0,  40.0780d0,  44.9559d0,  47.8670d0,  50.9415d0,  51.9961d0, &
    54.9380d0,  55.8450d0,  58.9332d0,  58.6934d0,  63.5460d0,  65.3900d0, &
    69.7230d0,  72.6400d0,  74.9216d0,  78.9600d0,  79.9040d0,  83.8000d0, &
    85.4678d0,  87.6200d0,  88.9059d0,  91.2240d0,  92.9064d0,  95.9400d0, &
    98.0000d0, 101.0700d0, 102.9055d0, 106.4200d0, 107.8682d0, 112.4110d0, &
   114.8180d0, 118.7100d0, 121.7600d0, 127.6000d0, 126.9045d0, 131.2930d0, &
   132.9055d0, 137.3270d0, 138.9055d0, 140.1160d0, 140.9077d0, 144.2400d0, &
   145.0000d0, 150.3600d0, 151.9640d0, 157.2500d0, 158.9253d0, 162.5000d0, &
   164.9303d0, 167.2590d0, 168.9342d0, 173.0400d0, 174.9670d0, 178.4900d0, &
   180.9479d0, 183.8400d0, 186.2070d0, 190.2300d0, 192.2170d0, 195.0780d0, &
   196.9665d0, 200.5900d0, 204.3833d0, 207.2000d0, 208.9804d0, 209.0000d0, &
   210.0000d0, 222.0000d0, 223.0000d0, 226.0000d0, 227.0000d0, 232.0381d0, &
   231.0359d0, 238.0289d0, 237.0000d0, 244.0000d0, 243.0000d0, 247.0000d0, &
   247.0000d0, 251.0000d0, 252.0000d0, 257.0000d0, 258.0000d0, 259.0000d0, &
   262.0000d0, 261.0000d0, 262.0000d0, 266.0000d0, 264.0000d0, 277.0000d0, &
   268.0000d0, 270.0000d0, 272.0000d0 /)

! ----------------------------------------------------------------------------------------
! common data structures
! ... to decrease argument lists but kept as simple as possible
! ----------------------------------------------------------------------------------------

  type vector
    logical  :: l        ! boolean, multi purpose
    integer  :: t        ! tag, multi purpose
    real(dp) :: r(3)     ! vector data
  end type vector

  type atom
    integer  :: e        ! element
    integer  :: t        ! tag (anything - enumerator for partitioning, reordering, ...)
    real(dp) :: r(3)     ! coordinate, can be anything (fractional, cartesian, ...)
    real(dp) :: v(3)     ! velocities (hopefully given in cartesian coordinates)
    logical  :: c        ! constraint (false = fixed, true = free)
  end type atom

  type pbc
!    integer  :: nat      ! number of atoms

    logical  :: lr &     ! cartesian coordinates (fractionals if false)
             ,  lc &     ! use constraints
             ,  lv       ! use velocities
    real(dp) :: box(3,3) ! unit cell (note: can be smaller than structure)
    integer  :: sys      ! system identifier (not related to coordinate system)
                         ! careful: in general, cell vectors are not clearly defined
                         ! for non-periodic directions.
                         !
                         ! [0] atom/molecules
                         ! [1] wire/tube (periodicity along a
                         !     [4]           ...            b
                         !     [7]           ...            c
                         ! [2] slab      (periodicity along a and b
                         !     [5]           ...            b and c
                         !     [8]           ...            a and c
                         ! [3] bulk
                         ! [6] undefined

    character(len=:) , allocatable :: comment  ! comment
    type(atom)       , allocatable :: atm(:)   ! atoms
  end type pbc

 contains

! ----------------------------------------------------------------------------------------
! check all derived types
! ----------------------------------------------------------------------------------------
  function chk_pbc( geom ) result( stat )
    use algebra3d , only: det3d
    type(pbc), intent(in) :: geom
    integer :: stat

    stat = 1

    ! allocated
    if(  allocated(geom%atm) .and. allocated(geom%comment)  ) then
      ! right-handed sensible sized unit cell, larger than unit cube
      ! (i.e. Beryllium: 16.23 Angstrom, 2 atoms = 8.12 Angstrom/atom)
      if(  det3d( geom%box(:,:) ) >  0.99d0  ) then

        stat = 0

      else
        write(stderr,'(a)') "[check pbc]   Error, left-handed unit cell or unrealistic cell volume."
      end if
    else
      if( .not. allocated(geom%atm)     ) write(stderr,'(a)') "[check pbc]  Error, geom%atm not allocated."
      if( .not. allocated(geom%comment) ) write(stderr,'(a)') "[check pbc]  Error, geom%comment not allocated."
    end if
  end function chk_pbc

! ----------------------------------------------------------------------------------------
! convert between element symbols and atomic number
! ----------------------------------------------------------------------------------------

  elemental pure function atomic_number( ielement_symbol )
  ! returns atomic number of element given by symbol
    integer                                 :: atomic_number
    character(len=sym_length) , intent(in)  :: ielement_symbol
    character(len=sym_length)               :: int_element_symbol   ! internal modifiable symbol
    integer                                 :: counter , tmp

    int_element_symbol = ielement_symbol
    call clean_symbol( int_element_symbol )

    atomic_number = 0
    do counter=1,list_length
     if( int_element_symbol == list_symbols(counter) ) then
       atomic_number = counter
       return
     end if
    end do

    contains

     elemental subroutine clean_symbol( ioelement_symbol )
       ! first letter upper case, second letter lower case
       character(len=sym_length), intent(inout) :: ioelement_symbol
       integer                         :: counter , tmp
       logical                         :: integrity
   
       ! worst case
       integrity = .false.
   
       ! convert first letter to upper case
       tmp = iachar(ioelement_symbol(1:1))
       if( tmp > 96 .and. tmp <= 122 ) then
         ioelement_symbol(1:1) = achar(tmp-32)
       else if( tmp < 64 .and. tmp >= 90 ) then
         ioelement_symbol(1:1) = anon_element(1:1)
       end if
       ! convert second letter to lower case
       tmp = iachar(ioelement_symbol(2:2))
       if( tmp > 64 .and. tmp <= 90 ) then
         ioelement_symbol(1:1) = achar(tmp+32)
       else if( tmp < 96 .and. tmp >= 122 ) then
         ioelement_symbol(2:2) = anon_element(2:2)
       end if
       ! check if the element exists
       do counter=1,list_length
         if( ioelement_symbol == list_symbols(counter) ) then
           integrity = .true.
           exit
         end if
       end do
       if( .not. integrity ) then
         ioelement_symbol = anon_element
       end if
     end subroutine clean_symbol

  end function atomic_number


  elemental pure function element_symbol( iatomic_number )
    ! returns element symbol, of element with input atomic number
    character(len=sym_length)       :: element_symbol
    integer, intent(in)             :: iatomic_number

    if( iatomic_number <= 0 .or. iatomic_number > list_length ) then
      element_symbol = anon_element
    else
      element_symbol = list_symbols( iatomic_number )
    end if
  end function element_symbol


  elemental pure function element_weight( iatomic_number )
    ! returns element symbol, of element with input atomic number
    real(dp)                        :: element_weight
    integer, intent(in)             :: iatomic_number

    if( iatomic_number <= 0 .or. iatomic_number > list_length ) then
      element_weight = anon_weight
    else
      element_weight = list_weights( iatomic_number )
    end if
  end function element_weight


! -----------------------------------------------------------------------------
! convert between pbc formats
! -----------------------------------------------------------------------------

  subroutine pbc_convert( geom , lr )
    use algebra3d , only: matinv3d
    ! -------------------------------------------------------------------------
    ! convert between fractional and cartesian coordinates
    ! -------------------------------------------------------------------------
    type(pbc) , intent(inout) :: geom
    logical   , intent(in)    :: lr
    integer  :: i
    real(dp) :: trans(3,3)
    
    if( geom%lr .eqv. lr ) then
    ! nothing to convert
      return 
    else if( lr ) then
    ! to cartesian
      trans =  geom%box
    else
    ! to fractional
      trans =  matinv3d( geom%box )
    end if

    geom%lr = lr

    ! transform coordinates
    do i=1,size(geom%atm)
      geom%atm(i)%r = matmul( trans , geom%atm(i)%r )
    enddo
  end subroutine pbc_convert


  function get_sys( plane , wire , crystal ) result( sys )
    ! --------------------------------------------------------------------------
    ! return number of system, used for external procedures that do not want to
    ! bother with any changing defaults. plane or wire according to description
    ! above
    ! > intended to give only one or no input
    ! 
    ! call get_sys( wire=2 )
    ! 
    ! --------------------------------------------------------------------------
    integer , optional , intent(in) :: plane , wire , crystal
    integer :: sys , tmp

    if( present(plane) ) then
      sys = (modulo(plane,3)+1)*3-1
    else if( present(wire) ) then
      sys = (modulo(wire-1,3)+1)*3-2
    else if( present(crystal) ) then
      sys = 3
    else
      sys = 0
    endif
  end function get_sys


! ----------------------------------------------------------------------------------------
! auxiliary procedures
! ----------------------------------------------------------------------------------------

  pure subroutine setup_selection( ssize , maxsize , selec )
  ! (mechanic to implement full prints, if no selection is given)
  !
  ! if allocated, wrap values into range (maxsize), else set up with increment

    integer , intent(out)                 :: ssize
    integer , intent(in)                  :: maxsize
    integer , intent(inout) , allocatable :: selec(:)
    integer :: i

    if ( allocated(selec) ) then

      ssize = size(selec)
      selec = modulo(selec(:)-1,maxsize)+1

    else

      ssize = maxsize
      allocate( selec( ssize ) )
      selec(:)  =  (/ ( i , i = 1 , ssize ) /)
      do i = 1,ssize
        selec(i) = i
      enddo

    end if

  end subroutine setup_selection


  subroutine incr_tag( atm , selection )
    type(atom)         , intent(inout) :: atm(:)
    integer , optional , intent(in)    :: selection(:)
    integer , allocatable              :: selec(:)
    integer :: i,nat

    ! selection
    if( present(selection) ) then
      allocate( selec(size(selection)) )
      selec = selection
    end if
    call setup_selection( nat , size(atm) , selec )

    do i=1,nat
      atm(selec(i))%t = i
    enddo

  end subroutine incr_tag


  ! ----------------------------------------------------------------------------
  pure function is_digit( a ) result( ldigit )
  !
  ! True if character a is a digit. False else.
  !
    character(len=1) , intent(in) :: a
    logical :: ldigit

    ldigit = ( iachar(a) >= 48 .and. iachar(a) < 58 )

  end function  is_digit
  ! ------------------------------------------------------------------------ end

  ! ----------------------------------------------------------------------------
  pure function is_alpha( a ) result( lalpha )
  !
  ! True if character a is any English character. False else.
  !
    character(len=1) , intent(in) :: a
    logical :: lalpha

    lalpha = ( iachar(a) > 96 .and. iachar(a) <= 122 )  &
        .or. ( iachar(a) > 64 .and. iachar(a) <=  90 )

  end function  is_alpha
  ! ------------------------------------------------------------------------ end

  ! ----------------------------------------------------------------------------
  pure function is_char( a , str ) result( lchar )
  !
  ! True if character a is anywhere in string str. False else.
  !
    character(len=1) , intent(in) :: a
    character(len=*) , intent(in) :: str
    logical :: lchar
    integer :: i

    lchar = .false.
    do i = 1,len(str)
      if( a == str(i:i) ) then
        lchar = .true.
        return
      end if
    enddo

  end function is_char
  ! ------------------------------------------------------------------------ end


! ----------------------------------------------------------------------------------------
! print nice output
! ----------------------------------------------------------------------------------------

  subroutine box_print( box , output )
    ! unit cell geometry data
    real(dp)     ,       intent(in) :: box(3,3)
    integer , optional , intent(in) :: output
    integer                         :: out
    real(dp) :: value(3) , angle(3)
    value(1) = sqrt(sum( (box(:,1)**2) ))
    value(2) = sqrt(sum( (box(:,2)**2) ))
    value(3) = sqrt(sum( (box(:,3)**2) ))

    angle(1) = acos( sum(box(:,2)*box(:,3))/(value(2)*value(3)) )
    angle(2) = acos( sum(box(:,1)*box(:,3))/(value(1)*value(3)) )
    angle(3) = acos( sum(box(:,1)*box(:,2))/(value(1)*value(2)) )
    angle    = 180.0d0/pi_ * angle

    write(out,'(a12,3a13,a11)') 'x' , 'y' , 'z' , 'r' , 'ang'
    write(out,'(5x,58("-"))')
    write(out,'(2x,"a",3f13.8,f11.4,f10.2)') box(:,1) , value(1) , angle(1)
    write(out,'(2x,"b",3f13.8,f11.4,f10.2)') box(:,2) , value(2) , angle(2)
    write(out,'(2x,"c",3f13.8,f11.4,f10.2)') box(:,3) , value(3) , angle(3)
  end subroutine box_print


  subroutine atom_print( atm , selection , lconstraints , lvelocities , lcartesian , output )
    ! print coordinates
    type(atom)         , intent(in) :: atm(:)
    integer , optional , intent(in) :: selection(:)                             ! print only selected atoms
    integer , allocatable           :: selec(:)
    logical , optional , intent(in) :: lconstraints , lvelocities , lcartesian  ! printed columns, column labels, default: f f "f/t"
    logical                         :: lconstr      , lvel
    integer , optional , intent(in) :: output                                   ! set output, default: stderr
    integer                         :: out
    character(len=3) :: coord(3) , vcoord(3)
    integer          :: nat , i , s

    ! selection
    if( present(selection) ) then
      allocate( selec(size(selection)) )
      selec = selection
    end if
    call setup_selection( nat , size(atm) , selec )

    ! switches
    lconstr =  .false.
    lvel    =  .false.
    out     =  stderr
    coord   =  (/ 'a/x' , 'b/y' , 'c/z' /)
    vcoord  =  (/ 'v_x' , 'v_y' , 'v_z' /)

    if( present(lconstraints) )  lconstr = lconstraints
    if( present(lvelocities)  )  lvel    = lvelocities
    if( present(output)       )  out     = output

    if( present(lcartesian) ) then
      if( lcartesian ) then
        coord = (/ ' x ' , ' y ' , ' z ' /)
      else
        coord = (/ ' a ' , ' b ' , ' c ' /)
      end if
    end if


    ! print
    if( lvel .and. lconstr ) then

      write(out,'(2x,a3,2x,a4,2x,a4,a7,2a13,2x,3a13,a12)') '#','tag','sym.',coord,vcoord,'constr.'

      do i = 1,nat
        s = selec(i)
        write(out,'(i5,2x,i4,2x,a2,3f13.8,2x,3f13.8,2x,l2)') &
          i , atm(s)%t , element_symbol( atm(s)%e ) , atm(s)%r , atm(s)%v , atm(s)%c
      end do

    elseif( lvel .and. .not. lconstr ) then

      write(out,'(2x,a3,2x,a4,2x,a4,a7,2a13,2x,3a13)') '#','tag','sym.',coord,vcoord

      do i = 1,nat
        s = selec(i)
        write(out,'(i5,2x,i4,2x,a2,3f13.8,2x,3f13.8)') &
          i , atm(s)%t , element_symbol( atm(s)%e ) , atm(s)%r , atm(s)%v
      end do

    elseif( .not. lvel .and. lconstr ) then

      write(out,'(2x,a3,2x,a4,2x,a4,a7,3a13)') '#','tag','sym.',coord,'constr.'

      do i = 1,nat
        s = selec(i)
        write(out,'(i5,2x,i4,2x,a2,3f13.8,2x,l2)') &
          i , atm(s)%t , element_symbol( atm(s)%e ) , atm(s)%r , atm(s)%c
      end do

    else

      write(out,'(2x,a3,2x,a4,2x,a4,a7,2a13)') '#','tag','sym.',coord

      do i = 1,nat
        s = selec(i)
        write(out,'(i5,2x,i4,2x,a2,3f13.8)') &
          i , atm(s)%t , element_symbol( atm(s)%e ) , atm(s)%r
      end do

    end if
  end subroutine atom_print


  subroutine pbc_print( geom , selection )

    type(pbc) , intent(in) :: geom
    integer   , intent(in) , optional :: selection(:)

    if( geom%sys /= 0 ) call box_print( geom%box )

    if( present(selection) ) then
      call atom_print( geom%atm , selection=selection , lcartesian=geom%lr , lconstraints=geom%lc , lvelocities=geom%lv )
    else
      call atom_print( geom%atm , lcartesian=geom%lr , lconstraints=geom%lc , lvelocities=geom%lv )
    end if

  end subroutine pbc_print


  subroutine vector_print( vec , selection , center , truefalse , output )
  use algebra3d, only: value
    ! unit cell geometry data
    type(vector)            , intent(in) :: vec(:)
    integer      , optional , intent(in) :: selection(:)
    integer      , allocatable           :: selec(:)
    real(dp)     , optional , intent(in) :: center(3)
    character(len=2),optional,intent(in) :: truefalse
    character(len=2)                     :: tf
    integer      , optional , intent(in) :: output
    integer                              :: out
    character(len=2) :: c
    integer          :: i,s,nv
    real(dp)         :: dist

    if( present(output)       )  out     = output
    
    ! selection
    if( present(selection) ) then
      allocate( selec(size(selection)) )
      selec = selection
    end if
    call setup_selection( nv , size(vec) , selec )

    ! switches
    tf      =  'tf'
    out     =  stderr

    if( present(truefalse) )  tf   = truefalse
    if( present(output)    )  out  = output

    ! print, conditional in loop
    write(out,'(2x,a3,2x,a4,a7,2a13,6x,a10)') '#','tag','x','y','z','dist.'

    do i = 1,nv
      s = selec(i)
      c = merge( tf(1:1) , tf(2:2) , vec(s)%l )
      if( present(center) .and. vec(s)%l ) then
        dist = value( vec(s)%r - center )
        write(out,'(i5,2x,i4,3f13.8,2x,f10.5)')   s , vec(s)%t , vec(s)%r , dist
      else
        write(out,'(i5,2x,i4,3f13.8,6x,a1)')   s , vec(s)%t , vec(s)%r , c
      end if
    end do

  end subroutine vector_print


end module types
