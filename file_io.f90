module file_io

use params , only: dp , sstr_ , str_ , lstr_ , stdout , stderr , default_comment , default_box
use types  , only: pbc , atom , chk_pbc
use types  , only: pbc_convert , atomic_number , element_symbol
use parse  , only: group_elem , print_elem , print_integers , print_floats , print_human_float

 implicit none
 private
 public :: file_attr , init_file , get_filename , basename , dirname
 public :: any_read , any_write , convert_frmt , increment_enum , name_to_frmt

 ! ----------------------------------------------------------------------------------------
 ! I/O routines to access/write geometry files
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ----------------------------------------------------------------------------------------
 integer                   , parameter :: file_formats(5) = (/ 0, 1, 2, 3, 4/)
 character(len=4)          , parameter :: format_names(5) = (/ 'xyz ', 'vasp', 'dftb', 'xsf ', 'cp2k' /)

 type file_attr
   ! general form is <path>/<name><enum>.<ext>
   ! for instance data/CONTCAR2 or data/test/water_04.xyz
   character(len=:) , allocatable :: path     ! dirname
   character(len=:) , allocatable :: name     ! basename, excluding enum and ext
   character(len=:) , allocatable :: enum     ! enumerator, saved as string including separator
   character(len=:) , allocatable :: ext      ! file extension
   integer  :: frmt     ! [0] xyz
                        ! [1] vasp
                        ! [2] dftb
                        ! [3] xsf
                        ! [4] cp2k
   integer  :: stat     ! [0] all OK
                        ! else untested or error
   logical  :: lreplace
 end type file_attr

 contains

  ! ----------------------------------------------------------------------------
  function init_file( filename , lreplace ) result( file )
  ! ----------------------------------------------------------------------------
  ! initialize file_attr 'file' from a given full filename and internal switches
  !
  ! file%name  file%path  file%enum  file%ext  file%frmt  file%lreplace
  ! ----------------------------------------------------------------------------

    use params , only: default_filename , default_format
    use parse  , only: lower_case
    use types  , only: is_char, is_digit

    type(file_attr)                :: file
    logical, optional, intent(in)  :: lreplace

    character(len=*) , intent(in)  :: filename
    character(len=:) , allocatable :: tmp
    character(len=*) , parameter   :: sep=',.^_-+=' ! separates enum

    integer  :: i !, x , y , z

    ! --------------------------------------------------------------------------
    ! defaults
    file%name = default_filename
    file%path = '.'
    file%enum = ''
    file%ext  = ''
    file%frmt = default_format
    file%stat = 0
    file%lreplace = .true.
    ! --------------------------------------------------------------------------
    ! split filename string into categories in form 'path/name_enum.ext'

    ! dirname
    file%path = dirname(filename)
    ! (full) basename
    tmp       = basename(filename)
    
    ! clip extension
    do i = len(tmp),1,-1
      if( tmp(i:i) == '.' ) exit
    end do

    if( i > 1 ) then
      file%ext = tmp(i:)
      tmp      = tmp(:i-1)
    end if

    ! clip enumerator
    do i = len(tmp),1,-1
      if( .not. is_digit( tmp(i:i) ) )  exit
    end do
    if( i < len(tmp) .and. is_char( tmp(i:i), sep ) )  then
      file%name = tmp(:i-1)
      file%enum = tmp(i:)
    else
      ! no enumerator detected. separator needed to avoid mixing up chemical
      ! formulae
      file%name = tmp(:)
    end if
    !print*,'DEBUG path : ',file%path
    !print*,'DEBUG name : ',file%name
    !print*,'DEBUG enum : ',file%enum
    !print*,'DEBUG ext  : ',file%ext 
    ! --------------------------------------------------------------------------
    ! try to determine frmt from filename extension
    select case( lower_case(file%ext) )
      case( '.xyz'     ); file%frmt = 0
      case( '.vasp'    ); file%frmt = 1
      case( '.gen'     ); file%frmt = 2
      case( '.xsf'     ); file%frmt = 3
      case( '.cp2k'    ); file%frmt = 4
      case( '.restart' ); file%frmt = 4
      case default
        ! try to determine frmt from filename
        select case( lower_case(file%name(:6)) )
          case( 'poscar' ); file%frmt = 1
          case( 'contca' ); file%frmt = 1
          case default
            write(stderr,'(a)') "[init file]   File extension undetected. Will try by reading..."
            file%frmt = 4
        end select
    end select

    if( present( lreplace ) ) then
      file%lreplace = lreplace
    end if
  end function init_file
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function dirname( filename ) result( string )
  ! ----------------------------------------------------------------------------
  ! chop filename from full path, i.e. /path/to/geom.xyz -> geom.xyz
  ! split path instead if lpath is given (and true)
  ! ----------------------------------------------------------------------------
    character(len=*)  , intent(in)   :: filename
    character(len=:)  , allocatable  :: string
    integer  :: i
    ! --------------------------------------------------------------------------
    do i = len(filename),1,-1
      if( filename(i:i) == '/' ) then
        exit
      endif
    enddo
    ! print dirname (without trailing '/')
    string =  trim(adjustl(filename(:i-1)))
  end function dirname
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function basename( filename ) result( string )
  ! ----------------------------------------------------------------------------
  ! equivalent with POSIX 'basename <filename>'
  ! ----------------------------------------------------------------------------
    character(len=*)  , intent(in)   :: filename
    character(len=:)  , allocatable  :: string
    integer  :: i
    ! --------------------------------------------------------------------------
    do i = len(filename),1,-1
      if( filename(i:i) == '/' ) then
        exit
      endif
    enddo
    ! print basename
    string = trim(filename(i+1:))
  end function basename
  ! ------------------------------------------------------------------------ end


!  ! ----------------------------------------------------------------------------
!  function random_string( length ) result( string )
!  use params , only: rnginit
!    ! --------------------------------------------------------------------------
!    ! create random filename (lowercase characters)
!    ! --------------------------------------------------------------------------
!    integer , intent(in)  :: length
!    character(len=length) :: string
!    real(dp) :: rnum(length)
!    integer  :: i
!
!    ! --------------------------------------------------------------------------
!    call rnginit()
!    call random_number( rnum(:) )
!
!    do i = 1,length
!      string(i:i) = achar( 97 + floor(rnum(i)*26) )
!    enddo
!
!  end function random_string
!  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function get_filename( file ) result( filename )
    use params , only: default_filename , default_format
  ! ----------------------------------------------------------------------------
  ! make full filename from file attributes
  ! ----------------------------------------------------------------------------
    type(file_attr)  , intent(in)  :: file
    character(len=:) , allocatable :: filename , path

    ! add '/' to path
    path = ''
    if( len(file%path) > 0 ) path = file%path//'/'

    filename = path // file%name // file%enum // file%ext

  end function get_filename
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure subroutine convert_frmt( file , frmt )
  use parse  , only: lower_case
  ! ----------------------------------------------------------------------------
  ! convert file_attr for different formats
  ! ----------------------------------------------------------------------------
    type(file_attr)  , intent(inout) :: file
    integer          , intent(in)    :: frmt

    file%frmt = frmt

    select case( frmt )
      case( 0 )
        file%ext = '.xyz'
      case( 1 )
        ! no file extension for VASP
        file%ext = ''
        ! add prefix 'POSCAR_'
        if( len(file%name) >= 6 )  then
          if(   lower_case(file%name(:6)) /= 'poscar' &
          .and. lower_case(file%name(:6)) /= 'contca' ) then
            file%name  = 'POSCAR_' // file%name
          end if
        else
          file%name  = 'POSCAR_' // file%name
        end if
      case( 2 )
        file%ext  = '.gen'
      case( 3 )
        file%ext  = '.xsf'
      case( 4 )
        file%ext  = '.cp2k'
      case default
        file%stat = 1
    end select

    ! remove potentially annoying 'POSCAR_'
    if( frmt /= 1 .and. len( file%name ) >= 8 )  then
      if( file%name(:7) == 'POSCAR_' ) then
        file%name = file%name(8:)
      end if
    end if
  end subroutine convert_frmt
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure subroutine increment_enum( file )
  ! ----------------------------------------------------------------------------
  ! add or increment file enumeration
  ! ----------------------------------------------------------------------------
    use types  , only: is_digit

    type(file_attr)  , intent(inout) ::  file
    character(len=:) , allocatable   ::  pfmt , sep , tmp
    integer  ::  fcount , adig

    sep  = ''
    if( len(file%enum) == 0 ) file%enum = '_0'

    ! set separator and number of assumed digits (preceeding zeroes)
    adig = len(file%enum)
    if( .not. is_digit( file%enum(1:1) ) ) then
      adig = adig - 1
      sep  = file%enum(1:1)
      read( file%enum(2:) , * ) fcount
    else
      read( file%enum(:) , * ) fcount
    end if

    ! 
    ! format enumerator
    pfmt = '(I??.??)'
    write(pfmt,'(a,i2.2,a,i2.2,a)') '(I',adig+2,'.',adig,')' ! (i03.01), (i04.02), ...

    allocate( character(len=adig+2) :: tmp )

    write( tmp , pfmt )  fcount
    file%enum = sep // trim( adjustl( tmp ) ) 

  end subroutine increment_enum
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  function chk_file_attr( file ) result( stat )
  ! ----------------------------------------------------------------------------
    type(file_attr), intent(in) :: file
    integer :: stat

    stat = 1

    ! allocated
    if(  allocated(file%name) .and. &
         allocated(file%path) .and. &
         allocated(file%enum) .and. &
         allocated(file%ext)) then
      ! correct ranges
      if(  file%frmt >= 0 .and. file%frmt <= 4  ) then
        ! own assumption ..
        if(  file%stat == 0  ) then

          stat = 0

        else
          write(stderr,'(a)') "[check file]  Error, unknown error."
          write(stderr,'(a)') "                name: '"//file%name//"'"
          write(stderr,'(a)') "                path: '"//file%path//"'"
          write(stderr,'(a)') "                enum: '"//file%enum//"'"
          write(stderr,'(a)') "                ext:  '"//file%ext//"'"
        end if
      else
        write(stderr,'(a)') "[check file]  Error, undefined file format."
      end if
    else
      if( .not. allocated(file%name) ) write(stderr,'(a)') "[check file]  Error, file%name not allocated."
      if( .not. allocated(file%path) ) write(stderr,'(a)') "[check file]  Error, file%path not allocated."
      if( .not. allocated(file%enum) ) write(stderr,'(a)') "[check file]  Error, file%enum not allocated."
      if( .not. allocated(file%ext)  ) write(stderr,'(a)') "[check file]  Error, file%ext not allocated."
    end if

  end function chk_file_attr
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  elemental pure function name_to_frmt( name ) result( frmt )
  ! ----------------------------------------------------------------------------
    character(len=*)       , intent(in) :: name
    character(len=len(format_names(1))) :: tmp
    integer          :: frmt
    integer          :: i , n

    n   = len(format_names(1))
    tmp = adjustl(name)

    frmt = -1
    do i = 1,size(file_formats)
      if( format_names(i) == tmp ) then
        frmt = file_formats(i)
        exit
      end if
    end do

  end function name_to_frmt
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  pure function frmt_to_name( frmt ) result( name )
  ! ----------------------------------------------------------------------------
    character(len=4)     :: name
    integer , intent(in) :: frmt
    integer              :: i

    do i = 1,size(file_formats)
      if( file_formats(i) == frmt ) exit
    end do

    name = trim(format_names(i))
  end function frmt_to_name
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  subroutine any_read( file , geom )
  ! ----------------------------------------------------------------------------
    type(file_attr)  , intent(inout) :: file
    type(pbc)        , intent(out)   :: geom
    integer          , allocatable   :: attempt(:)
    logical :: l2nd_attempt
    integer :: i , funit

    ! ----------------------------------------------------------
    ! check input
    ! open file
    ! parse given frmt  |  __\  done within
    ! test other frmts  |    /    a loop
    ! ----------------------------------------------------------
    l2nd_attempt = .false.

    ! check input  (error messages in procedures)
    if( chk_file_attr(file) /= 0  ) then
      file%stat = 1
      return
    end if

    ! attempt to read file with all known input formats, starting from
    ! format provided in file%frmt
    
    attempt = file_formats(:)
    call priorize( attempt , file%frmt ) ! reorder file formats
    

    do i = 1 , size(attempt)

      ! open file  (error messages in procedures)
      call open_infile( )

      if(  file%stat /= 0  ) then
        close(funit)
        return
      end if

      ! parse  (error messages in procedures)
      call parse_n_close( attempt(i) )
      
      if(  file%stat == 0  )  exit
      l2nd_attempt = .true.

    end do

    ! update extension if guess was incorrect
    if( l2nd_attempt ) then
      call convert_frmt( file , attempt(i) )
    else
      file%frmt = attempt(i)
    end if

    if( .not. allocated( geom%comment ) ) geom%comment = default_comment

 contains

    ! ----------------------------------------------------------
    subroutine open_infile( )
    ! ----------------------------------------------------------
    ! open input file or read from stdin
     use params , only: stdin_ , inunit

     logical                           :: exists ! file

     file%stat = 1

     ! ---------------------------------------------------------
     if( file%name == '-' ) then
       ! read from stdin
       funit = stdin_
       write(stderr,'(a)') "[any read]    Read from standard input ..."

     else
       ! check if file exists
       funit = inunit
       inquire(  file = get_filename(file) ,  exist = exists  )

       if( .not. exists ) then
         write(stderr,'(a)') "[any read]    Error, file '"//get_filename(file)//"' does not exist."
         return
       endif

       ! open file
       open(  unit = funit ,  file = get_filename(file) ,  status = "old" ,  iostat = file%stat  )

       if( file%stat /= 0 ) then
         write(stderr,'(a)') "[any read]    Error, cannot open '" // get_filename(file) // "'"
         return
       endif

     endif

     file%stat = 0

    end subroutine open_infile
    ! ------------------------------------------------------ end

    ! ----------------------------------------------------------
    subroutine parse_n_close( frmt )
      integer :: frmt

     if( allocated( geom%comment ) ) deallocate (geom%comment)
     if( allocated( geom%atm     ) ) deallocate (geom%atm)

     select case( frmt )

       case( 0 )
         call xyz_read( funit , geom , file%stat )

       case( 1 )
         call vasp_read( funit , geom , file%stat )

       case( 2 )
         call dftb_read( funit , geom , file%stat )

       case( 3 )
         write(stderr,'(a)') "[any read]    My apologies, but xsf parsing is not yet implemented. Only XcrysDen output..."
         file%stat = 1

       case( 4 )
         call cp2k_read( funit , geom , file%stat )

       case default
         write(stderr,'(a)') "[any read]    Error, format not recognized!"
         file%stat = 1

     end select

     close( funit )

    end subroutine parse_n_close
    ! ------------------------------------------------------ end

    ! ----------------------------------------------------------------------------
    subroutine priorize( avail , best )
    ! ----------------------------------------------------------------------------
    ! - move first occurence of best in avail(:) to front.
    ! - shift prior array elements by one
    ! - do nothing if best is not found
    ! ----------------------------------------------------------------------------
      integer, intent(inout) :: avail(:)
      integer, intent(in)    :: best
      integer                :: i , j , t1 , t2
      
      do i = 1 , size( avail )
        
        ! Find first occurence of best in avail (if existing),
        if( avail(i) /= best ) cycle

        ! (run below only if 'best' is found in 'avail')
        
        ! shift previous entries in avail,
        do j = i , 2 , -1
          avail(j) = avail(j-1)
        end do

        ! put best first,
        avail(1) = best

        ! and leave loop.
        exit

      end do

    end subroutine priorize
    ! ------------------------------------------------------------------------ end

  end subroutine any_read
  ! ------------------------------------------------------------------------ end


  ! ----------------------------------------------------------------------------
  subroutine any_write( file , geom )
  ! ----------------------------------------------------------------------------
    type(file_attr)  , intent(inout) :: file
    type(pbc)        , intent(inout) :: geom
    
    integer                          :: funit
    integer                          :: i

    ! --------------------------------------------------------------------------
    ! check input
    ! open file
    ! write
    ! --------------------------------------------------------------------------

    if( chk_pbc(geom) /= 0 .or. chk_file_attr(file) /= 0  ) then
      file%stat = 1
      return
    end if

   
    if( file%stat /= 0 )  return  ! Error messages in open_outfile

    select case( file%frmt )

      case( 0 )
        call open_outfile( file , funit )
        call xyz_write( funit , geom , file%stat )

      !case( 'xyz_def'  )
        !call join_molecule( geom )
        !call pbc_convert( geom , .true. )
        !shift = center_xyz( geom%atm )
        !do i = 1,size(geom%atm)
        !  geom%atm(i)%r = geom%atm(i)%r - shift
        !enddo
        !call xyz_write( funit , geom , file%stat )

      case( 1 )
        call open_outfile( file , funit )
        call vasp_write( funit , geom , file%stat )

      case( 2 )
        call open_outfile( file , funit )
        call dftb_write( funit , geom , file%stat )

      case( 3 )
        call open_outfile( file , funit )
        call xsf_write( funit , geom , file%stat )

      case( 4 )
        call open_outfile( file , funit )
        call cp2k_write( funit , geom , file%stat )
        !write(stderr,'(a)') "[any write]   Will only provide snippets for CP2k output"
        !file%stat = 1

      case default
        write(stderr,'(a)') "[any write]   Error, format not recognized!"
        return
    
    end select

    close( funit )

    if( file%name /= '-' ) write(stderr,'(a)') "[any write]   Geometry written to '"//get_filename(file)//"'."


  contains


   ! -----------------------------------------------------------
   subroutine open_outfile( file , funit )
   ! -----------------------------------------------------------
   ! open output file or write to stdout

    use params , only: stdout_ , outunit

    type(file_attr)   , intent(inout) :: file
    integer           , intent(out)   :: funit

    logical                           :: exists ! file

    ! ----------------------------------------------------------
    if( file%name == '-' ) then
      ! write to standard output parameter, as all variable outputs write to /dev/null
      !  (routine should not be affected by suppressed writes)
      funit = stdout_

    !  ! do not write at all (useful to suppress writing i.e. in (planned) geominfo
    !  return

    else

      ! only replace file if allowed
      inquire(  file = get_filename(file) ,  exist = exists  )
 
      if(  exists .and. .not. file%lreplace  ) then
        write(stderr,'(a)') "[any write]   Error, '"//get_filename(file)// &
                                        "' already exists and replacing was disabled!"
        file%stat = 1

      else ! open file

        funit = outunit

        open(  unit = funit ,  file = get_filename(file) ,  status = "replace" ,  iostat = file%stat  )

        if(  file%stat /= 0  ) then
          write(stderr,'(a)') "[any write]   Error, missing permission to replace '" // get_filename(file) // "'."
          return
        end if

      end if

    end if

   end subroutine open_outfile
   ! ------------------------------------------------------- end
  
  
  end subroutine any_write
  ! ------------------------------------------------------------------------ end


  ! ---------------------------------------------------------------------------------------
  subroutine xyz_read( funit , geom , stat )
  use meteorgy, only: add_and_delete_atoms
  ! ---------------------------------------------------------------------------------------
  ! read *.xyz files

    integer    , intent(in)  :: funit
    type(pbc)  , intent(out) :: geom
    integer    , intent(out) :: stat

    type(atom) , allocatable :: atm(:)

    integer                  :: nat
    integer                  :: i , j , l(3)
    character(len=2)         :: elem_sym
    character(len=lstr_)     :: line
    ! --------------------------------------------------------------------------

    geom%lr  = .true.
    geom%lc  = .false.
    geom%lv  = .false.
    geom%sys = 0
    geom%box = default_box

    ! read number of atoms
    read(funit,*,iostat=stat) nat
    if(stat/=0) then; write(stderr,'(a,1x,i5)') "[xyz read]    Error reading number of atoms!",stat; return; endif

    ! read comment
    read(funit,'(a)',iostat=stat) line
    if(stat/=0) then; write(stderr,'(a,1x,i5)') "[xyz read]    Error reading comment!",stat; return; endif
    geom%comment = trim(line)

    ! seek and parse unit cell data from comment
    do i = 3 , len(geom%comment)
      if( geom%comment(i-2:i) == '%uc' ) then
        read( geom%comment(i+1:) , * , iostat=stat ) geom%box
        if(stat==0) then
          write(stderr,'(a,1x,i5)') "[xyz read]    Parsed unit cell from comment!",stat
          geom%sys = 3
          geom%comment = trim( geom%comment(:i-3) )
        else
          write(stderr,'(a,1x,i5)') "[xyz read]    Error parsing cell vectors!",stat
          return
        end if
        exit
      end if
    end do

    ! allocate
    if( allocated( geom%atm ))   deallocate( geom%atm )
    allocate( geom%atm(nat) )

    ! read atoms
    do i = 1 , nat
      read(funit,*,iostat=stat) elem_sym , geom%atm(i)%r
      if(stat/=0) then; write(stderr,'(a,i4,a,1x,i5)') "[xyz read]    Error reading atom", i ,"!",stat; return; endif
      geom%atm(i)%e = atomic_number( elem_sym )
      geom%atm(i)%c = .true. ! no constraint
      geom%atm(i)%t = i
      geom%atm(i)%v(:) = 0.0d0
    end do
    
    ! --------------------------------------------------------------------------
    close(funit)
    stat = 0

  end subroutine xyz_read
  ! ---------------------------------------------------------------------------------------- end


  ! --------------------------------------------------------------------------------------------
  subroutine xyz_write( funit , geom , stat )
  ! ---------------------------------------------------------------------------------------
  ! write *.xyz files

    integer   , intent(in)    :: funit
    type(pbc) , intent(inout) :: geom
    integer   , intent(out)   :: stat
    
    character(len=sstr_)      :: tmp
    integer                   :: i
    ! --------------------------------------------------------------------------

    call pbc_convert( geom , .true. )

    write(tmp,'(i8)') size( geom%atm(:) )
    write(funit,'(a)') trim(adjustl(tmp))

    ! add unit cell to comment
    if( geom%sys == 0 ) then
      write(funit,'(a)') trim(geom%comment)
    else
      write(funit,'(a)') trim(geom%comment) // ' %uc ' // print_floats( reshape( geom%box , (/9/) ) , digits = 7 , lnoexp = .true. )
    end if

    do i = 1,size(geom%atm)
      write(funit,'(a2,1x,3f21.16)') element_symbol(geom%atm(i)%e) , geom%atm(i)%r
    enddo

    close(funit)

  end subroutine xyz_write
  ! ---------------------------------------------------------------------------------------- end


  ! ---------------------------------------------------------------------------------------
  subroutine vasp_read( funit , geom , stat )
  ! ---------------------------------------------------------------------------------------
    use algebra3d,      only: det3d
    use parse, only:    parse_elem
    
    integer   , intent(in)  :: funit
    type(pbc) , intent(out) :: geom
    integer   , intent(out) :: stat

    character(len=str_)     :: efmt         ! Error format
    character(len=lstr_)    :: tmpline      ! temporary line
    real(dp)                :: volume       ! unit cell volume
    real(dp)                :: scaling      ! scaling factor
    
    integer                 :: nat          ! number of atoms
    integer                 :: atm_sorts    ! number of atom sorts
    integer  , allocatable  :: elem    (:)  ! contained element list, atomic number
    integer  , allocatable  :: nat_elem(:)  ! number of atoms per element

    real(dp)                :: vasp_r(3)    ! read lines
    logical                 :: vasp_c(3)    ! before conversion
    logical                 :: ltags        ! try 

    integer                 :: i , j , k    ! counters

    ! ------------------------------------------------------------------------------------
    ! assume periodic if reading vasp input
    geom%sys = 3

    ! ------------------------------------------------------------------------------------
    ! reading first five lines (comment, scaling, unit cell)
    ! ------------------------------------------------------------------------------------

    efmt = '(A,1X,I5)'
    tmpline = ''
    scaling = 0.0d0

    read(funit,'(A)',iostat=stat) tmpline       ! line 1: comment

    if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading comment!"; return; endif
    geom%comment = trim(tmpline)


    read(funit,*,iostat=stat) scaling           ! line 2: scaling

    if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading scaling parameter!"; return; endif
    if(scaling==0.0d0) then; write(stderr,efmt) "[vasp read]   Error scaling is zero!"; return; endif

    read(funit,*,iostat=stat) geom%box(:,1)     ! line 3, 4, 5: unit cell
    if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading cell vector a!"; return; endif
    read(funit,*,iostat=stat) geom%box(:,2)
    if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading cell vector b!"; return; endif
    read(funit,*,iostat=stat) geom%box(:,3)
    if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading cell vector c!"; return; endif

    ! check if right hand coordinate system
    volume = det3d(geom%box)

    if(volume<=0.0d0) then; write(stderr,efmt)  &
      "[vasp read]   Error, linear dependencies or left-handed cell vectors"; return; endif

    ! negative scaling values are treated as cell volume in angstrom^3
    if( scaling <  0.0d0 ) scaling = ( -scaling / volume )**(1.0d0/3.0d0)
    
    ! ------------------------------------------------------------------------------------
    ! read in elements
    ! ------------------------------------------------------------------------------------

    read(funit,'(a)',iostat=stat , size = i , advance='no') tmpline       ! line 6: elements  (missing in legacy VASP, read as line 7 on if no element detected)
    ! fail if line too long using this tags, stat <= if size not filled...

    if(i == lstr_) then; write(stderr,efmt) "[vasp read]   Error, way too many elements/atoms!"; return; endif
    if( stat>= 0 ) then; write(stderr,efmt) "[vasp read]   Error reading elements/atoms!"; return; endif

    ! get available elements
    call parse_elem( tmpline , elem )

    if( size(elem)  == 0 ) then    ! (empty line)
        write(stderr,efmt) "[vasp read]   Error element line is empty!"; return; endif
    
    if( elem(1)     /= 0 ) then    ! (if element symbols are missing, group sizes (nat_elem) would have been read in)
      ! read next line
      read(funit,'(A)',iostat=stat , size = i , advance='no') tmpline     ! (line 7)
      ! fail if line too long using this tags, stat <= if size not filled...

      if(i == lstr_) then; write(stderr,efmt)  "[vasp read]   Error, way too many atom sorts!"; return; endif
      if( stat>= 0 ) then; write(stderr,efmt)  "[vasp read]   Error reading atom sorts!"; return; endif

    else
      ! ... if not then elements are missing!!!
      write(stderr,'(A)') stat , "[vasp read]   Warning, problem while reading elements, will make up some on my own!"

      do i = 1,size(elem)
        elem(i) = 111-size(elem) + i
      end do
    end if
    
    ! read number of atoms per element
    allocate( nat_elem(size(elem)) )
    read(tmpline,*,iostat=stat) nat_elem(:)     ! line 6 or 7: number of atoms per element

    if(stat> 0) then; write(stderr,efmt) "[vasp read]   Error reading atoms per element!"; return; endif

    ! ------------------------------------------------------------------------------------
    ! test if constraints are used and read in the used coordinate system (cartesian or
    ! fractional)
    ! ------------------------------------------------------------------------------------
    geom%lc  = .false.

    read(funit,*,iostat=stat) tmpline           ! line 7 or 8: constraints
    if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading 'selective'/'basis' switch!"; return; endif

    ! check if constraints where switched on
    if( tmpline(1:1) =='S' .or. tmpline(1:1) =='s' ) then
      geom%lc = .true.
      read(funit,*,iostat=stat) tmpline         ! (line 8 or 9)
      if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading 'basis' switch!"; return; endif
    else
      geom%lc = .false.
    end if

    if( tmpline(1:1)=='C' .or.  &               ! line 7, 8 or 9: test cartesian switch
        tmpline(1:1)=='c' .or.  &
        tmpline(1:1)=='K' .or.  &
        tmpline(1:1)=='k' ) then
      geom%lr = .true.
    else
      geom%lr = .false.                         ! https://www.vasp.at/wiki/index.php/POSCAR (sic!)
    end if
    
    ! ------------------------------------------------------------------------------------
    ! apply scaling to unit cell
    geom%box = scaling * geom%box

    ! get number of atoms, allocate geom, set up element array
    ! ------------------------------------------------------------------------------------

    ! allocate
    nat = sum(nat_elem)
    if( allocated( geom%atm )) deallocate( geom%atm )
    allocate( geom%atm( nat ) )

    ! legacy code
    !geom%nat = nat
    ! end legacy
    
    ! assign element list
    k=1
    ! assign elements
    do i=1,size(elem)
      do j=1,nat_elem(i) 
        geom%atm(k)%e = elem(i)
        k=k+1
      end do
    end do

    ! ------------------------------------------------------------------------------------
    ! read atom list (with or without constraints based on geom%lc)
    ! ------------------------------------------------------------------------------------
    efmt = '(a,i4,a)'
    if( .not. geom%lr ) scaling = 1.0d0

    if( geom%lc ) then
      geom%atm(:)%c = .false.

      do i=1,size(geom%atm)
        read(funit,'(A)',iostat=stat) tmpline
        read(tmpline,*,iostat=stat) vasp_r(:) , vasp_c(:)
        if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading coordinates of atom",i,"!"; return; endif

        ! make enumeration in tag 
        geom%atm(i)%t = i

        ! set coordinates
        geom%atm(i)%r = scaling * vasp_r

        ! set constraints
        if( vasp_c(1) .and. vasp_c(2) .and. vasp_c(3) ) then
          geom%atm(i)%c = .true.
        elseif( vasp_c(1) .or. vasp_c(2) .or. vasp_c(3) ) then
          write(stderr,efmt) stat,"[vasp read]   Warning, partial constraints not supported, atom",i," assumed fixed!"
        endif

      enddo

    else
      geom%atm(:)%c = .true.

      do i=1,size(geom%atm)
        read(funit,*,iostat=stat) vasp_r(:)
        if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading coordinates of atom",i,"!"; return; endif

        ! set coordinates
        geom%atm(i)%r = scaling * vasp_r

      enddo

    endif

    ! ------------------------------------------------------------------------------------
    ! read velocities (if available, adding some cases to succeed without successful read)
    ! ------------------------------------------------------------------------------------

    geom%lv = .false.
    do i=1,size(geom%atm)
      geom%atm(i)%v(:) = 0.0d0
    enddo

    read(funit,'(a)',iostat=stat) tmpline      ! read empty line
    if(stat < 0) then; stat=0; return; endif  ! return from routine as end of file reached
    if(stat > 0) then; write(stderr,'(a)') "[vasp read]   Error before velocity list!"; return; endif

    ! buffer and try to read first line as floats
    read(funit,'(a)',iostat=stat) tmpline      ! buffer first line of velocities
    if(stat < 0) then; stat=0; return; endif
    if(stat > 0) then; write(stderr,'(a)') "[vasp read]   Severe error at first line of velocity list!"; return; endif

    tmpline = adjustl(tmpline)                 ! check for nan
    if( tmpline(1:1) == 'N' .or. tmpline(1:1) == 'n' ) then; stat=0; return; endif

    read(tmpline,*,iostat=stat) vasp_r(:)      ! try to read first line
    if(stat/=0) then; write(stderr,efmt) "[vasp read]   Error reading velocity of atom!",1,"!"; return; endif
    geom%atm(1)%v = vasp_r(:)
    
    do i=2,size(geom%atm)                      ! loop from 2nd since first velocity is added manually
      read(funit,*,iostat=stat) geom%atm(i)%v
      if(stat /= 0) then;
        write(stderr,efmt) "[vasp read]   Error reading velocities. last atom is",i,"! continue without..."; stat = 0; return;
      endif
    enddo
    geom%lv = .true.                           ! enable velocity if non-zero entries detected


  end subroutine vasp_read
  ! ----------------------------------------------------------------------------------- end


  ! ---------------------------------------------------------------------------------------
  subroutine vasp_write( funit , geom , stat )
  use accounting, only: sort_one
  ! ---------------------------------------------------------------------------------------
    ! file unit number
    integer         , intent(in)    :: funit
    ! unit cell geometry data
    type(pbc)       , intent(inout) :: geom
    ! Error status
    integer         , intent(out)   :: stat
    ! ------------------------------------------------------------------------------------
    ! element's atomic number amount of atoms
    integer          , allocatable  :: formula(:,:)

    ! ------------------------------------------------------------------------------------
    ! internal
    character(len=lstr_)            :: tmpline

    ! formatting (ffrmt = float format)
    character(len=str_)             :: ffrmt , frmt
    ! counters
    integer                         :: i , j , k

    ! ------------------------------------------------------------------------------------
    ! get element list / number of atoms per element
    ! ------------------------------------------------------------------------------------
    call group_elem( geom%atm , formula )

    ! warn when multiple sets identified
    multi: do i = 2,size(formula,2)
      do j = 1 , i-1
        if( formula(1,j) == formula(1,i) ) then
          write(stderr,'(a)') "[vasp write]  Warning, detected multiple sets of elements!"
          exit multi
        end if
      end do
    end do multi
    tmpline = print_integers(formula(2,:))
    if( len(trim(tmpline)) > len(tmpline) - 10 ) then
      write(stderr,'(a)') "[vasp write]  Way too many element sets. Will sort by element!"
      
      call sort_one( 'e+' , geom%atm )
      call group_elem( geom%atm , formula )
    end if

    ! ------------------------------------------------------------------------------------
    ! formatted print
    ! ------------------------------------------------------------------------------------

    write(funit,'(a)',iostat=stat) trim(geom%comment)
    frmt = '(1x,a)'                                   ! 2. line format
    write(funit,frmt,iostat=stat) '  1.0'
    frmt = '(1x,3f12.6)'                              ! 3.-5. line format
    write(funit,frmt,iostat=stat) geom%box(:,1)
    write(funit,frmt,iostat=stat) geom%box(:,2)
    write(funit,frmt,iostat=stat) geom%box(:,3)

    write(funit,'(a)',iostat=stat) print_elem   (formula(1,:))
    write(funit,'(a)',iostat=stat) print_integers(formula(2,:))

    frmt = '(a)'                                      ! 8.(-9.) line format
    if( geom%lc ) then
      write(funit,'(a)',iostat=stat) "Selective dynamics"
    end if
    if( geom%lr ) then
      write(funit,'(a)',iostat=stat) "Cartesian"
    else
      write(funit,'(a)',iostat=stat) "Direct"
    end if
    ! ------------------------------------------------------------------------------------
    ffrmt =  'f14.8'
    if( geom%lc ) then
      frmt = '(1x,3'//trim(ffrmt)//',2x,3l2,3x,"! ",a2,i4)'   ! coordinate format with constraints
      do i = 1,size(geom%atm)
        write(funit,frmt,iostat=stat) geom%atm(i)%r , geom%atm(i)%c , &
          geom%atm(i)%c , geom%atm(i)%c , element_symbol( geom%atm(i)%e ) , i
      end do
    else
      frmt = '(1x,3'//trim(ffrmt)//',11x,"! ",a2,i4)'         ! coordinate format
      do i = 1,size(geom%atm)
        write(funit,frmt,iostat=stat) geom%atm(i)%r , element_symbol( geom%atm(i)%e ) , i
      end do
    end if
    ! ------------------------------------------------------------------------------------
    if( geom%lv ) then
      write(funit,*,iostat=stat) ""                           ! empty line
      frmt = '(1x,3'//trim(ffrmt)//',11x,"! ",a2,i4)'         ! coordinate format
      do i = 1,size(geom%atm)
        write(funit,frmt,iostat=stat) geom%atm(i)%v , element_symbol( geom%atm(i)%e ) , i
      end do
    end if
    stat = 0
  end subroutine vasp_write


  ! ---------------------------------------------------------------------------------------
  subroutine dftb_read( funit , geom , stat )
  ! ---------------------------------------------------------------------------------------
    use types , only: maxsorts_
    use meteorgy , only: make_box
    integer   , intent(in)  :: funit
    type(pbc) , intent(out) :: geom
    integer   , intent(out) :: stat

    integer                 :: i , j , k
    character(len=1)        :: syschar
    character(len=lstr_)    :: tmpline
    logical                 :: mkcell
    real(dp)                :: shift(3)
    integer                 :: nat
    integer                 :: atm_sorts
    character(len=2)        :: elem_sym(maxsorts_)
    integer                 :: elem_num(maxsorts_)
    integer                 :: nat_elem(maxsorts_)

    mkcell = .false.
    
    read(funit,*,iostat=stat) nat , syschar
    if( stat /= 0 ) then; write(stderr,'(a)') "[dftb+ read]  Error reading first line!"; return; end if

    if( syschar == 'F' .or. syschar == 'f' ) then
      geom%sys = 3
      geom%lr  = .false.
    else if( syschar == 'S' .or. syschar == 's' ) then
      geom%sys = 3
      geom%lr  = .true.
    else if( syschar == 'C' .or. syschar == 'c' ) then
      geom%sys = 0
      geom%lr  = .true.
      mkcell   = .true.
    else
      write(stderr,'(A)') "[dftb+ read]  Error, unknown system type ["//syschar//"]"
      stat = 1
      return
    end if
    
    if( allocated( geom%atm ) ) deallocate( geom%atm )
    allocate( geom%atm(nat) )

    ! --------------------------------------------------------------------------
    ! reading element data
    ! --------------------------------------------------------------------------
    elem_sym  =  element_symbol(0)
    elem_num  =  0
    nat_elem  =  0
    atm_sorts =  0

    read(funit,'(a)',iostat=stat) tmpline       !
    read(tmpline,*,iostat=stat) elem_sym(:)     ! read element symbols

    elem_num  = atomic_number(elem_sym)

    do i = 1,maxsorts_

      if( elem_num(i) == 0  ) then
        atm_sorts = i-1

        if( i == 1 ) then
          write(stderr,'(a)') "[dftb+ read]  Error reading element symbol!"
          return
        end if

        exit

      end if
    enddo

    ! --------------------------------------------------------------------------
    ! read atom data
    ! --------------------------------------------------------------------------

    do i = 1 , size(geom%atm)

      read(funit,*,iostat=stat) geom%atm(i)%t , k , geom%atm(i)%r

      geom%atm(i)%e = elem_num(k)

      if( stat /= 0 ) then
        write(stderr,'(i5,a,i4,a)') stat , "[dftb+ read]  Error reading atom [",i,"]!"
        return
      end if

      ! zero out velocities...
      geom%atm(i)%v(:) = 0.0d0

      ! no constraints...
      geom%atm(i)%c = .true.
    enddo

    ! --------------------------------------------------------------------------
    ! read or make unit cell
    ! --------------------------------------------------------------------------
    shift(:) = 0.0d0

    if( mkcell ) then
      call make_box( geom%box , .true. , 20.0d0 )
    else
      read(funit,*,iostat=stat) shift(:)
      if( stat /= 0 ) then; write(stderr,'(a)') "[dftb+ read]  Error reading shift!"; return; end if
      read(funit,*,iostat=stat) geom%box(:,1)
      if( stat /= 0 ) then; write(stderr,'(a)') "[dftb+ read]  Error reading a vector!"; return; end if
      read(funit,*,iostat=stat) geom%box(:,2)
      if( stat /= 0 ) then; write(stderr,'(a)') "[dftb+ read]  Error reading b vector!"; return; end if
      read(funit,*,iostat=stat) geom%box(:,3)
      if( stat /= 0 ) then; write(stderr,'(a)') "[dftb+ read]  Error reading c vector!"; return; end if
    end if

    return

  end subroutine dftb_read


  ! ---------------------------------------------------------------------------------------
  subroutine dftb_write( funit , geom , stat )
  ! ---------------------------------------------------------------------------------------
    integer        , intent(in)    :: funit
    type(pbc)      , intent(inout) :: geom
    integer        , intent(out)   :: stat

    character(len=1)               :: sysc
    character(len=str_)            :: ffrmt , frmt
    integer                        :: i , j , k , el
    logical                        :: test
    real(dp)                       :: shift(3)
    integer                        :: atm_sorts
    integer          , allocatable :: formula(:,:) , selem(:)
    character(len=2) , allocatable :: elem_sym(:)
    
    ! --------------------------------------------------------------------------
    ! fractional or cartesian coordinates?
    if( geom%sys == 0 )  then
      call pbc_convert( geom , .true. )
      sysc = 'c'
    else
      if( geom%lr )  then
        sysc = 's'
      else
        sysc = 'f'
      end if
    end if

    ! get list of elements
    call group_elem( geom%atm , formula )

    ! array connecting element list and coordinate (2nd column in gen format)
    allocate( selem(size(geom%atm)) )
    selem = 1
    k=1
    do i = 2,size(geom%atm)
      if( geom%atm(i-1)%e /= geom%atm(i)%e ) k=k+1
      selem(i) = k
    enddo
    
    ! set up symbols
    k = size( formula , 2 )
    allocate( elem_sym(k) )
!    elem_sym(1:k) = element_symbol( selem(1:k) )
    elem_sym = element_symbol( formula(1,:) )

    ! --------------------------------------------------------------------------
    ! print output

    ! 1st line
    write(funit,'(i5,2x,a1)',iostat=stat)  size(geom%atm) , sysc

    ! 2nd line
    write(frmt,'("(",i3.3,"a3)")') k
    write(funit,frmt,iostat=stat)  elem_sym
      
    ! atoms
    ffrmt = 'f14.8'
    frmt  = '(i5,i3,3'//trim(ffrmt)//')'         ! coordinate format
    do i = 1,size(geom%atm)
      write(funit,frmt,iostat=stat)  i ,  selem(i) ,  geom%atm(i)%r
    end do

    ffrmt = 'f12.6'
    frmt  = '(3'//trim(ffrmt)//')'
    
    if( geom%sys /= 0 ) then
      ! shift
      write(funit,'(a7,2a12)',iostat=stat) '0.0','0.0','0.0'

      ! unit cell
      write(funit,frmt,iostat=stat)  geom%box(:,1)
      write(funit,frmt,iostat=stat)  geom%box(:,2)
      write(funit,frmt,iostat=stat)  geom%box(:,3)
    end if


    deallocate( formula , selem , elem_sym )


  end subroutine dftb_write


  ! ---------------------------------------------------------------------------------------
  subroutine xsf_write( funit , geom , stat )
  ! ---------------------------------------------------------------------------------------
    !!! input
    integer   , intent(in)    :: funit
    type(pbc) , intent(inout) :: geom
    integer   , intent(out)   :: stat

    character(len=str_)       :: ffrmt , frmt
    integer                   :: i

    ! set precision
    ffrmt             = 'f21.16'
    ! --------------------------------------------------------------------------
    ! convert to cartesian coordinates
    ! --------------------------------------------------------------------------
    call pbc_convert( geom , .true. )

    ! --------------------------------------------------------------------------
    ! write xsf file
    ! --------------------------------------------------------------------------
    write(funit,'("# ",a)',iostat=stat) trim(geom%comment)
    frmt = '(1x,3'//trim(ffrmt)//')'
    if( geom%sys == 0 ) then
      write(funit,'(a)',iostat=stat) 'ATOMS'
    else
      write(funit,'(a)',iostat=stat) 'CRYSTAL'
      write(funit,'(a)',iostat=stat) 'PRIMVEC'
      write(funit,frmt,iostat=stat) geom%box(:,1)
      write(funit,frmt,iostat=stat) geom%box(:,2)
      write(funit,frmt,iostat=stat) geom%box(:,3)
      write(funit,'(a)',iostat=stat) 'PRIMCOORD'
      write(funit,'(2i4)',iostat=stat) size(geom%atm) , 1
    end if
    if( geom%lv ) then
      frmt = '(1x,i3,3x,3'//trim(ffrmt)//',3x,3'//trim(ffrmt)//')'
      do i = 1,size(geom%atm)
        write(funit,frmt) geom%atm(i)%e , geom%atm(i)%r , geom%atm(i)%v
      enddo
    else
      frmt = '(1x,i3,3x,3'//trim(ffrmt)//')'
      do i = 1,size(geom%atm)
        write(funit,frmt) geom%atm(i)%e , geom%atm(i)%r
      enddo
    endif

    close(funit)
  end subroutine xsf_write


  ! --------------------------------------------------------------------------------------------
  subroutine cp2k_read( funit , geom , stat )
  use parse, only: glob_separator , parse_selection , compact_selection , upper_case , lower_case &
                 , split_keyline , string_substitute , print_integers
  ! --------------------------------------------------------------------------------------------
  ! Search cp2k input for &CELL and &COORD section
  ! parse 
  ! --------------------------------------------------------------------------------------------
    integer    , intent(in)  :: funit
    type(pbc)  , intent(out) :: geom
    integer    , intent(out) :: stat
    integer    , parameter   :: INCR_ = 1000
    type(atom) , allocatable :: atm(:) , tmp(:)
    integer                  :: i,j,k,estnat
    character(len=lstr_)     :: line
    character(len=2)         :: elem_sym
    character(len=:) &
               , allocatable :: key, constr_sel
    integer    , allocatable :: constr(:)
    real(dp)                 :: vec(3)
    integer                  :: vmul_cell , vmul_topo(3)
    
    ! start clean
    if( allocated( geom%atm ))   deallocate( geom%atm )
    geom%comment = default_comment
    geom%lr      = .true.
    geom%lc      = .false.
    geom%lv      = .false.
    geom%box     = reshape( (/ 1000.0, 0.0, 0.0, 0.0, 1000.0, 0.0, 0.0, 0.0, 1000.0 /) , (/3,3/) )
    geom%sys     = 0
    constr_sel   = ''

    ! -------------------------------------------------------------------------------------
    ! read file line by line
    stat = 0
    do while( stat == 0 )
      read(funit,'(a)',iostat=stat) line
      if(stat < 0) then; stat=0; exit; endif ! end of file
      if(stat > 0) then; write(stderr,'(a)') "[cp2k read]   Unknown error while reading line."; return; endif
      line = upper_case(adjustl(line))


      ! -----------------------------------------------------------------------------------
      ! constraints
      if(     line(1:13) == '&FIXED_ATOMS ' ) then
        geom%lc = .true.
        do while( stat == 0 )
          read(funit,'(a)',iostat=stat) line
          if(stat /= 0) then; write(stderr,'(a)') "[cp2k read]   Error while reading constraints."; return; endif
          line = upper_case( line )
          call split_keyline( key , line )
          
          select case( key )
            case( 'LIST' )

              constr_sel = constr_sel // &
                           glob_separator( string_substitute(line,'..','-') , ',' )  //  ','

            case( '&END' )
              exit ! cell loop

            case default
              write(stderr,'(a)') "[cp2k read]   Warning: Keyword '"//lower_case(key)//"' ignored in &fixed_atoms."
          end select
        end do
        ! remove ending comma
        k = len(constr_sel)-1
        constr_sel = constr_sel(1:k)

      ! -----------------------------------------------------------------------------------
      ! cell
      elseif( line(1:6)  == '&CELL '       ) then
        geom%sys = 3
        do while( stat == 0 )
          read(funit,'(a)',iostat=stat) line
          if(stat /= 0) then; write(stderr,'(a)') "[cp2k read]   Error while reading cell."; return; endif
          line = upper_case( line )
          call split_keyline( key , line )

          select case( key )
            case( 'A' )
              read(line,*,iostat=stat) vec
              if(stat /= 0) write(stderr,'(a)') "[cp2k read]   Error while reading cell vector A."
              geom%box(:,1) = vec
            case( 'B' )
              read(line,*,iostat=stat) vec
              if(stat /= 0) write(stderr,'(a)') "[cp2k read]   Error while reading cell vector A."
              geom%box(:,2) = vec
            case( 'C' )
              read(line,*,iostat=stat) vec
              if(stat /= 0) write(stderr,'(a)') "[cp2k read]   Error while reading cell vector A."
              geom%box(:,3) = vec
            case( 'PERIODIC' )
              if( trim(line) /= 'XYZ') write(stderr,'(a)') "[cp2k read]   Warning 'periodic xyz' expected and not found..."// &
                                                                        " ignored anyway."
            case( 'MULTIPLE_UNIT_CELL' )
              read(line,*,iostat=stat) vmul_cell
              if(stat /= 0) write(stderr,'(a)') "[cp2k read]   Error while reading 'multiple_unit_cell' in &cell."
            case( '&END' )
              exit ! cell loop
          end select
        end do
        

      ! -----------------------------------------------------------------------------------
      ! topology
      elseif( line(1:10) == '&TOPOLOGY '   ) then
        do while( stat == 0 )
          read(funit,'(a)',iostat=stat) line
          if(stat /= 0) then; write(stderr,'(a)') "[cp2k read]   Error while reading topology."; return; endif
          line = upper_case( line )
          call split_keyline( key , line )
          
          select case( key )
            case( 'MULTIPLE_UNIT_CELL' )
              read(line,*,iostat=stat) vmul_topo
              if(stat /= 0) write(stderr,'(a)') "[cp2k read]   Error while reading multiple_unit_cell."

            case( '&END' )
              exit ! cell loop

            case default
              write(stderr,'(a)') "[cp2k read]   Warning: Keyword '"//lower_case(key)//"' ignored in &topology."
          end select
        enddo

      ! -----------------------------------------------------------------------------------
      ! coordinates
      elseif( line(1:7)  == '&COORD '      ) then
        ! guess number of atoms
        i = 0
        k = 1
        allocate( atm(k*INCR_) )
        do while( stat == 0 )
          read(funit,'(a)',iostat=stat) line
          if(stat /= 0) then; write(stderr,'(a)') "[cp2k read]   Error while reading coordinates."; return; endif
          line = adjustl(line)

          ! check other keywords
          if( line(1:6)  == 'SCALED'     ) then
            call split_keyline( key , line )
            if( .not. trim(line) == 'F' )  geom%lr = .false.
            cycle
          end if
          if( line(1:4)  == 'UNIT'       ) then
            write(stderr,'(a)') "[cp2k read]   Warning, 'unit' ignored, expect angstrom."
            cycle
          end if
          if( line(1:10) == '&END COORD' ) exit 

          ! parse atom
          i = i+1
          read(line,*,iostat=stat) elem_sym , atm(i)%r
          if(stat/=0) then; write(stderr,'(a,i4,a,1x,i5)') "[cp2k read]   Error reading atom", i ,"!",stat; return; endif
          atm(i)%e = atomic_number( elem_sym )
          atm(i)%c = .true.
          atm(i)%t = i

          ! increase number of possible atoms
          if( i == size(atm) ) then
            tmp = atm
            deallocate( atm )
            allocate( atm((k+1)*INCR_) )
            atm(:k*INCR_) = tmp(:)
            k = k+1
          end if
        end do
        ! allocate geom%atm
        allocate( geom%atm(i) )
        geom%atm(:)%c= .true.
        geom%atm = atm(:i)

      ! -----------------------------------------------------------------------------------
      ! velocities
      elseif( line(1:10) == '&VELOCITY '   ) then
        geom%lv = .true.
        if( allocated( geom%atm ) ) then
          do i = 1,size(geom%atm)
            read(funit,'(a)',iostat=stat) line
            if( trim(adjustl(line)) == '&END VELOCITY' ) then
              write(stderr,'(a)') "[cp2k read]   Error, missing velocities!"
              stat = 1; return
            end if
            read(line,*,iostat=stat)  geom%atm(i)%v
            if(stat /= 0) then; write(stderr,'(a)') "[cp2k read]   Error while reading velocities."; return; endif
          end do
        else
          write(stderr,'(a)') "[cp2k read]   Must read coord first"
          stat = 1; return
        endif

        ! read expected ending line of velocities
        read(funit,'(a)',iostat=stat) line
        if( trim(adjustl(line)) /= '&END VELOCITY' ) then
          write(stderr,'(a)') "[cp2k read]   Error, too many velocities!"
          stat = 1; return
        end if
          
      end if
    end do


    ! -------------------------------------------------------------------------------------
    ! set up constraints
    if( .not. allocated( geom%atm ) ) then
          write(stderr,'(a)') "[cp2k read]   Error, missing coordinates!"
          stat = 1; return
    end if
    if( geom%lc ) then
      call parse_selection( size(geom%atm) , constr_sel , constr , stat )
      do i = 1 , size(constr)
        geom%atm(constr(i))%c = .false.
      end do
    end if
  end subroutine cp2k_read
  ! ---------------------------------------------------------------------------------------- end


  ! --------------------------------------------------------------------------------------------
  subroutine cp2k_write( funit , geom , stat )
  use parse, only: compact_selection , string_substitute , print_w_newline , print_integers
  ! --------------------------------------------------------------------------------------------
  ! #write incomplete cp2k output based on available geometry data
  !
  ! --------------------------------------------------------------------------------------------
    integer    , intent(in)    :: funit
    type(pbc)  , intent(inout) :: geom
    integer    , intent(out)   :: stat
    character(len=:) &
               , allocatable   :: constr_sel
    integer    , allocatable   :: constr(:) , tmp(:)
    integer                    :: i,j,k,l

    ! -------------------------------------------------------------------------------------
    ! set up constraints
    
    if( geom%lc ) then
      allocate( tmp(size(geom%atm)) )
      k = 0
      do i = 1,size(geom%atm)
        if( .not. geom%atm(i)%c ) then
          k      = k+1
          tmp(k) = i
        end if
      end do

      allocate( constr(k) )
      constr     = tmp(1:k)
      constr_sel = compact_selection( constr , size(geom%atm) )
      constr_sel = string_substitute( constr_sel , '-' , '..' )
      constr_sel = '       LIST '//string_substitute( constr_sel , ',' , '\n       LIST ' )

      write(funit,'(a)')     ' $MOTION'
      write(funit,'(a)')     '   $CONSTRAINT'
      write(funit,'(a)')     '     &FIXED_ATOMS'
      call print_w_newline( funit , constr_sel )
      write(funit,'(a)')     '     &END FIXED_ATOMS'
      write(funit,'(a)')     '   $END CONSTRAINT'
      write(funit,'(a)')     ' $END MOTION'
    end if
    write(funit,'(a)')       ' &FORCE_EVAL'
    write(funit,'(a)')       '   &SUBSYS'
    if( geom%sys == 3 ) then
    write(funit,'(a)')       '     &CELL'
    write(funit,'(a,3f12.6)')'       A ',geom%box(:,1)
    write(funit,'(a,3f12.6)')'       B ',geom%box(:,2)
    write(funit,'(a,3f12.6)')'       C ',geom%box(:,3)
      write(funit,'(a)')     '       PERIODIC  XYZ'
      write(funit,'(a)')     '       MULTIPLE_UNIT_CELL  1 1 1'
    write(funit,'(a)')       '     &END CELL'
    end if
    write(funit,'(a)')       '     $COORD'
    do i = 1,size(geom%atm)
      write(funit,'(a2,1x,3f21.16)') element_symbol(geom%atm(i)%e) , geom%atm(i)%r
    enddo
    write(funit,'(a)')       '     $END COORD'
    if( geom%lv ) then
      write(funit,'(a)')     '     &VELOCITY'
      do i = 1,size(geom%atm)
        write(funit,'(3f21.16)')     geom%atm(i)%v
      enddo
      write(funit,'(a)')     '     &END VELOCITY'
      write(funit,'(a)')     '     &TOPOLOGY'
      write(funit,'(a,i5)')  '       NUMBER_OF_ATOMS',size(geom%atm)
      write(funit,'(a)')     '       MULTIPLE_UNIT_CELL  1 1 1'
      write(funit,'(a)')     '     &END TOPOLOGY'
    end if
    write(funit,'(a)')       '   &END SUBSYS'
    write(funit,'(a)')       ' &END FORCE_EVAL'

  end subroutine cp2k_write
  ! ---------------------------------------------------------------------------------------- end


  ! --------------------------------------------------------------------------------------------
  subroutine template_read( funit , geom , stat )
  ! --------------------------------------------------------------------------------------------
  ! template read routine
  ! =====================
  ! never accessed immediately but from any_read
  !
  ! * reads from input stream defined by integer 'funit' (opened in any_read, above)
  ! * should be capable of accepting a pbc-type input 'geom' in any structure (see types.f90)
  ! * clears and fully re-initializes the structure
  ! * returns with stat /= 0 on error
  ! * geom%atm is expected to exactly match the number of atoms.
  !
  ! NOTE: initialization means that after a successful run, 'geom' is fully allocated!
  !       best practice is to allocate 'geom%comment' with the default from 'params.f90' and
  !       'geom%atm(:)%t' with incrementing values from 1:size(geom%atm)
  ! --------------------------------------------------------------------------------------------
    integer    , intent(in)  :: funit
    type(pbc)  , intent(out) :: geom
    integer    , intent(out) :: stat
    integer                  :: i,j,nat
    character(len=lstr_)     :: line
    
    ! start clean
    if( allocated( geom%atm ))   deallocate( geom%atm )
    geom%comment = default_comment

    ! -------------------------------------------------------------------------------------
    ! read data needed for allocation of geom%atm
    !   (If not possible, in a single run, add an internal 'atm' array and fill it sequentially.
    !    Preferably build reallocation scheme to grow structure. Allocate 'geom%atm' and copy

    line = '   5   '
    !! better example: read single line as raw string 'line'
    !read(funit,(a),iostat=stat) line
    !if( stat /= ) then; write(stderr,'(a)') 'ERROR MESSAGE'; return; end if
    !
    !! parse 'line'
    read(line,*,iostat=stat) nat
    allocate( geom%atm(nat) )


    ! -------------------------------------------------------------------------------------
    ! read data in loop
    do i = 1 , size(geom%atm)
      ! make enumeration in tag 
      geom%atm(i)%t = i
      !!example from xyz_read
      !read(funit,*,iostat=stat) elem_sym , geom%atm(i)%r
    end do
  end subroutine template_read
  ! ---------------------------------------------------------------------------------------- end


  ! --------------------------------------------------------------------------------------------
  subroutine template_write( funit , geom , stat )
  ! --------------------------------------------------------------------------------------------
  ! template write routine
  ! ======================
  ! never accessed immediately but from any_write
  !
  ! * writes to output stream defined by integer 'funit' (opened in any_write, above)
  ! * assumes that pbc-type input structure is intact (checked before in any_write)
  ! * write formatted output. Should be readable by its counterpart routine 'template_read'
  ! * optionally cast warnings if data needs separate treatment
  ! * 'stat' is ignored, since external writing errors are caught by any_write and any error
  !   in this routine would be a bug in itself.
  ! --------------------------------------------------------------------------------------------
    integer    , intent(in)    :: funit
    type(pbc)  , intent(inout) :: geom
    integer    , intent(out)   :: stat
  end subroutine template_write
  ! ---------------------------------------------------------------------------------------- end


  ! ---------------------------------------------------------------------------------------
  !
  ! legacy code
  !
  ! ---------------------------------------------------------------------------------------

  subroutine displ_read( funit , nat , displ , constr , stat )
  ! ---------------------------------------------------------------------------------------
  ! read displacecar (part of henkelman-patches)
  
    integer          , intent(in)                :: funit
    integer          , intent(in)                :: nat
    real(dp)         , intent(out)               :: displ
    logical          , intent(out) , allocatable :: constr(:,:)
    integer          , intent(out)               :: stat
    logical :: setdispl
    integer ::  i
    real(dp):: tmpdispl(3)

    ! --------------------------------------------------------------------------

    setdispl = .true.
    if( allocated( constr ) ) deallocate( constr )
    allocate( constr(3,nat) )
    constr = .false.
    do i = 1,nat
      read(funit,*,iostat=stat) tmpdispl(:)
      if( stat /= 0 ) then
        write(stderr,'(a)') "[displ read]  Error reading displacecar."
        return
      endif
      if( tmpdispl(1) /= 0.0d0 .or.   &
          tmpdispl(2) /= 0.0d0 .or.   &
          tmpdispl(3) /= 0.0d0    )   then
        constr(:,i) = .true.
        if( setdispl ) then
          if( tmpdispl(1) /= 0.0d0 ) then
            displ = tmpdispl(1)
            setdispl = .false.
          elseif( tmpdispl(2) /= 0.0d0 ) then
            displ = tmpdispl(2)
            setdispl = .false.
          elseif( tmpdispl(3) /= 0.0d0 ) then
            displ = tmpdispl(3)
            setdispl = .false.
          endif
        endif
      endif
    enddo
  end subroutine displ_read


  ! ----------------------------------------------------------------------------
  subroutine displ_write( funit , nat , displ , constr )
  ! ----------------------------------------------------------------------------
  ! write displacecar

    integer , intent(in)  :: funit
    integer , intent(in)  :: nat
    real(dp), intent(in)  :: displ
    logical , intent(in)  :: constr(3,nat)
    logical :: iconstr(3,nat)
    integer :: i , stat

    ! --------------------------------------------------------------------------

    do i = 1,nat
      if( constr(1,i) .or. constr(2,i) .or. constr(3,i) ) then
        write(funit,'(2x,3f7.3)') displ , displ , displ
      else
        write(funit,'(1x,3f7.3)') 0.0 , 0.0 , 0.0
      endif
    enddo
  end subroutine displ_write


end module file_io
