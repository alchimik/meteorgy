MODULE meteorgy
 
USE params     , ONLY: dp
USE types      , ONLY: pbc , atom , pbc_convert , setup_selection
USE accounting , ONLY: sort_one , sort_by_tag

 IMPLICIT NONE
 PRIVATE      ::       dp
 PRIVATE      ::       pbc , atom , pbc_convert
 PRIVATE      ::       sort_one , sort_by_tag
 PUBLIC

 ! ---------------------------------------------------------------------------------------
 ! Manipulate geometry datasets
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ---------------------------------------------------------------------------------------
 
 ! ------------------------------------------------------------------------------
 ! - auxiliary functions
 ! - geometry tweaks
 ! - conversions
 !
 ! DEFINITIONS
 ! ===========
 ! vacuum_   -  distance between atoms along unit cell axis larger than vacuum_
 !              will be considered as vacuum to distinguish bulks, slabs, wires
 !              molecules
 ! 
 ! ------------------------------------------------------------------------------

 CONTAINS

  subroutine make_supercell( geom , tdiag )
    ! --------------------------------------------------------------------------
    ! create supercell simply by multiplying cell in a, b and c direction given
    ! as diagonal of the transformation matrix
    ! --------------------------------------------------------------------------
    type(pbc)  , intent(inout)  :: geom
    integer    , intent(in)     :: tdiag(3)
    type(atom) , allocatable    :: atm(:)
    integer :: i,j,k,l,m,vmul,nat

    CALL pbc_convert( geom , .TRUE. )
    
    vmul = tdiag(1)*tdiag(2)*tdiag(3)
    nat  = size(geom%atm)
    atm  =  geom%atm(:)

    deallocate(geom%atm)
    allocate( geom%atm( size(atm)*vmul ) )

    ! replicate unit cell content
    m = 1
    do i=1,nat
      do j=0,tdiag(1)-1
        do k=0,tdiag(2)-1
          do l=0,tdiag(3)-1
            geom%atm(m)    =  atm(i)
            geom%atm(m)%r  =  atm(i)%r + j*geom%box(:,1) + k*geom%box(:,2) + l*geom%box(:,3)
            m = m+1
          end do
        end do
      end do
    end do

    ! increase unit cell size
    geom%box(:,1) = tdiag(1) * geom%box(:,1)
    geom%box(:,2) = tdiag(2) * geom%box(:,2)
    geom%box(:,3) = tdiag(3) * geom%box(:,3)
  
  end subroutine make_supercell

  SUBROUTINE make_slab( geom , plane , bound1 , bound2 )
  !USE types,     ONLY: pbc , atom !, box_print , atom_print
    ! --------------------------------------------------------------------------
    !   cut slab from bulk geometry (supports 100, 010 and 001 slab cuts)
    !
    ! geom:    input/output geometry
    ! plane:   integer 
    ! boundX:  boundaries (in fractional units, their order is irrelevant).
    ! 
    !   use integer multiple of perpendicular vector as new provisional vector
    !   perpendicular to slab
    ! --------------------------------------------------------------------------
    REAL(dp)   , PARAMETER      :: minthickness = 4.0D0 , defaultseparation = 10.0D0
    TYPE(pbc)  , INTENT(INOUT)  :: geom
    INTEGER    , INTENT(IN)     :: plane
    REAL(dp)   , INTENT(IN)     :: bound1 , bound2

    TYPE(atom) , ALLOCATABLE    :: atm(:)
    CHARACTER(LEN=2) :: sortkey
    REAL(dp)         :: up , down , tmp
    
    ! plane
    INTEGER          :: p
    ! first atom, last atom, first block, last block
    INTEGER          :: fa , la , fb , lb
    ! counters
    INTEGER          :: i , j , k , m , n
    INTEGER          :: nat

    ! --------------------------------------------------------------------------
    ! get all auxiliary values, make work copy in atm
    ! --------------------------------------------------------------------------

    nat = SIZE(geom%atm)

    ! plane of choice
    p = MODULO(plane-1,3)+1

    ! geom in fractional units
    CALL pbc_convert( geom , .FALSE. )
    
    ! boundaries ordered
    up   = MERGE( bound1 , bound2 , bound1 >= bound2 )
    down = MERGE( bound1 , bound2 , bound1 <  bound2 )

    ! make wrapped and sorted copy of atoms
    ALLOCATE( atm(nat) )
    DO i = 1,nat
      atm(i)      = geom%atm(i)
      atm(i)%r(p) = geom%atm(i)%r(p) - FLOOR( geom%atm(i)%r(p) )
    ENDDO
    
    WRITE(sortkey,'(I1,A1)') p , '-'
    CALL sort_one( sortkey , atm )

    ! get first atom and block
    fb = CEILING( up ) - 1
    tmp = up - REAL(fb,dp)
    fa = 1
    DO WHILE( atm(fa)%r(p) >= tmp )
      fa = fa+1
    ENDDO
    
    ! get last atom and block
    lb  = FLOOR( down )
    tmp = down - REAL(lb,dp)
    la = nat
    DO WHILE( atm(la)%r(p) <= tmp )
      la = la-1
    ENDDO
    
    ! --------------------------------------------------------------------------
    ! make slab
    ! --------------------------------------------------------------------------
    DEALLOCATE( geom%atm )
    nat = nat*(fb-lb)-fa+la+1
    ALLOCATE( geom%atm(nat) )

    m = fa
    n = nat
    k = 1
    DO i = fb,lb,-1
      IF( i == lb ) n = la
      DO j = m,n
        geom%atm(k)      = atm(j)
        geom%atm(k)%r(p) = atm(j)%r(p) + REAL(i,dp)
        k = k + 1
      ENDDO
      m = 1
    ENDDO
    
    CALL pbc_convert( geom , .TRUE. )
    ! duplicate unit cell to fit thickness
    geom%box(:,p) = (fb-lb+1) * geom%box(:,p)

    ! ---------------------------------------------------------------------- END
  END SUBROUTINE make_slab


  SUBROUTINE set_vacuum( geom , plane , value , mode )
  USE params     , ONLY: vacuum_
  USE algebra3d  , ONLY: ncross3d , boxheight
  USE types,     ONLY: pbc , atom , box_print , atom_print
    ! --------------------------------------------------------------------------
    !   create nice vacuum region (assume joint slab)
    !
    ! geom:      input/output geometry
    ! plane:     integer 
    ! value:     value depending on sign: positive is absolute cell height,
    !            negative is minimum vacuum distance (rounded to full angstrom)
    !
    ! mode:      no shift, (c)enter of mass, scale (s)ymmetric
    ! 
    !   use integer multiple of perpendicular vector as new provisional vector
    !   perpendicular to slab
    ! --------------------------------------------------------------------------
    TYPE(pbc)        , INTENT(INOUT)  :: geom
    INTEGER          , INTENT(IN)     :: plane
    REAL(dp)         , INTENT(IN)     :: value
    CHARACTER(len=1) , INTENT(IN)     :: mode

    INTEGER  :: i , p , q , r
    REAL(dp) :: val , zvec , oldh , low , high , shift(3)

    p = MODULO(plane-1,3)+1
    q = MODULO(plane , 3)+1
    r = MODULO(plane+1,3)+1

    ! before touching unit cell...
    CALL pbc_convert( geom , .TRUE. )

    ! save previous height
    oldh = boxheight( geom%box , p )

    ! calculate thickness = high - low
    low  = geom%atm(1)%r(p)
    high = low
    DO i = 2,SIZE(geom%atm)
      low  = MIN( low  , geom%atm(i)%r(p) )
      high = MAX( high , geom%atm(i)%r(p) )
    ENDDO

    ! get z vector
    ! --------------------------------------------------------------------------
    val = MERGE( value , -20.0D0 , ABS(val) >= high - low + vacuum_ )

    IF( val > 0.0D0 ) THEN
      zvec = value

    ELSE IF( val < 0.0D0 ) THEN

      ! use integer value >= thickness + val
      zvec = REAL( CEILING( high - low - val ) , dp )

    END IF

    ! get scaled direction perpendicular to slab
    geom%box(:,p) = ncross3d( geom%box(:,q) , geom%box(:,r) ) * zvec


    ! shift slab depending on mode
    ! --------------------------------------------------------------------------
    IF( mode == 'c' .OR. mode == 's' ) THEN

      IF( mode == 'c' ) THEN
        shift(:)  = -center_slab( geom , p )
        shift(:)  = shift(:) + geom%box(:,p)/2.0D0
      ELSE IF( mode == 's' ) THEN
        shift(:)  = 0.0D0
        shift(p)  = -( zvec - oldh ) / 2.0D0
      END IF

      DO i=1,SIZE(geom%atm)
        geom%atm(i)%r = geom%atm(i)%r + shift
      ENDDO

    END IF
  
  END SUBROUTINE set_vacuum


  PURE SUBROUTINE slab_box( box , plane , scale )
    USE algebra3d , ONLY: ncross3d
    ! --------------------------------------------------------------------------
    ! make pseudo-2D unit cell. Replace third cell vector by scaled normal vector
    ! --------------------------------------------------------------------------

    REAL(dp) , INTENT(INOUT)         :: box(3,3)
    INTEGER  , INTENT(IN)            :: plane
    REAL(dp) , INTENT(IN) , OPTIONAL :: scale

    REAL(dp) :: sc
    INTEGER  :: p , q , r

    sc = 1.0
    IF( PRESENT(scale) ) sc = scale

    p = MODULO(plane-1,3)+1
    q = MODULO(plane , 3)+1
    r = MODULO(plane+1,3)+1


    ! ---------------------------------------------------------------------- END
  END SUBROUTINE slab_box


  SUBROUTINE make_box( box , lrhom , separation )
    ! --------------------------------------------------------------------------
    ! create unsymmetric boxes with near cubic or rhombohedral shape and
    ! given separation
    !  - separation in angstrom is the nearest distance between two images
    !  - rhombohedron is equivalent fcc closest sphere packing
    !    (best volume/distance ratio)
    !  - symmetry broken by diff-parameter. Workaround for known bug in VASP
    !    when using cubic sells for molecule calculations
    ! --------------------------------------------------------------------------
    REAL(dp) , PARAMETER :: sqrt2 = 1.4142135623730950488D0 , diff=0.05

    ! modes - 0: custom cell (already given), 1: appr. cubic cell, 2: appr. orthorhombic cell
    LOGICAL  , INTENT(IN)  :: lrhom
    REAL(dp) , INTENT(IN)  :: separation

    REAL(dp) , INTENT(OUT) :: box(3,3)

    ! set cell
    IF( lrhom ) THEN
      ! ~ rhombohedron
      box(:,:) = 0.0D0
      box(2,1) = separation/sqrt2*(1.0D0-diff)
      box(3,1) = separation/sqrt2*(1.0D0-diff)
      box(1,2) = separation/sqrt2*(1.0D0)
      box(3,2) = separation/sqrt2*(1.0D0)
      box(1,3) = separation/sqrt2*(1.0D0+diff)
      box(2,3) = separation/sqrt2*(1.0D0+diff)
    ELSE
      ! ~ cube
      box(:,:) = 0.0D0
      box(1,1) = separation*(1.0D0-diff)
      box(2,2) = separation*(1.0D0)
      box(3,3) = separation*(1.0D0+diff)
    ENDIF

    ! ---------------------------------------------------------------------- END
  END SUBROUTINE make_box


  SUBROUTINE slab_to_xy( geom , plane )
    ! --------------------------------------------------------------------------
    ! 
    ! --------------------------------------------------------------------------
    TYPE(pbc)  , INTENT(INOUT)  :: geom
    INTEGER    , INTENT(IN)     :: plane
    INTEGER   :: x , y , z
    
    x = MODULO(plane  ,3)+1
    y = MODULO(plane+1,3)+1
    z = MODULO(plane-1,3)+1

    CALL pbc_convert( geom , .FALSE. )

    geom%box(:,1) = (/ geom%box(x,1) , geom%box(y,1) , geom%box(z,1) /)
    geom%box(:,2) = (/ geom%box(x,2) , geom%box(y,2) , geom%box(z,2) /)
    geom%box(:,3) = (/ geom%box(x,3) , geom%box(y,3) , geom%box(z,3) /)

    ! ---------------------------------------------------------------------- END
  END SUBROUTINE slab_to_xy


  PURE SUBROUTINE transform_atoms( rmat , atm , zero , selec )
    USE algebra3d , ONLY: rotmat3d , trans3d
    ! --------------------------------------------------------------------------
    ! transform (and shift) selection (around shifted zero axis)
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    REAL(dp)   , INTENT(IN)    :: rmat(3,3)
    REAL(dp)   , INTENT(IN)    &
               , OPTIONAL      :: zero(3)
    INTEGER    , INTENT(IN)    &
               , OPTIONAL      :: selec(:)
    REAL(dp)  :: shift(3)
    INTEGER   :: i

    IF( PRESENT(zero) ) THEN
      shift = zero - trans3d( rmat , zero )
    ELSE
      shift = 0.0D0
    END IF

    IF( PRESENT(selec) ) THEN
      DO i=1,SIZE(selec)
        atm(selec(i))%r = trans3d( rmat ,atm(selec(i))%r ) + shift
      ENDDO
    ELSE
      DO i=1,SIZE(atm)
        atm(i)%r = trans3d( rmat ,atm(i)%r ) + shift
      ENDDO
    END IF

    ! ---------------------------------------------------------------------- END
  END SUBROUTINE transform_atoms


  PURE FUNCTION center_xyz( atm , selection ) RESULT( center )
    USE types, ONLY: element_weight
    ! --------------------------------------------------------------------------
    ! get center of mass of aperiodic system (or selected subset)
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(IN) :: atm(:)
    INTEGER , OPTIONAL , INTENT(IN) :: selection(:)
    INTEGER , ALLOCATABLE           :: selec(:)
    REAL(dp)                :: center(3)
    REAL(dp)  :: mxyz(3) , m , mm
    INTEGER   :: i,k,nat

    ! selection
    IF( PRESENT(selection) ) THEN
      ALLOCATE( selec(SIZE(selection)) )
      selec = selection
    END IF
    CALL setup_selection( nat , SIZE(atm) , selec )

    ! calculate center
    mxyz(:) = 0.0D0
    mm      = 0.0D0

    DO i=1,nat
      k = selec(i)
      m       = element_weight(atm(k)%e) 
      mxyz(:) = mxyz(:) + m * atm(k)%r
      mm      = mm      + m
    ENDDO

    center = mxyz(:) / mm
    ! ---------------------------------------------------------------------- END
  END FUNCTION center_xyz


  FUNCTION center_slab( geom , plane ) RESULT( center )
  USE algebra3d  , ONLY: dot3d
    ! --------------------------------------------------------------------------
    ! get center of mass for slabs and wires. shift perpendicular to periodic
    ! direction(s)
    !
    ! - convert to desired system
    ! - in the general case use join_slab before centering
    ! - subroutine converts coordinates by key
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(IN)  :: geom
    INTEGER   , INTENT(IN)  :: plane
    TYPE(pbc) :: g
    REAL(dp)  :: center(3) , dir(3) , proj
    INTEGER   :: p

    p = MODULO(plane-1,3)+1

    ! get center
    IF( .NOT. geom%lr ) THEN
      g = geom
      CALL pbc_convert( g , .TRUE. )
      center =  center_xyz( g%atm )
      DEALLOCATE( g%atm )
    ELSE
      center =  center_xyz( geom%atm )
    END IF

    ! remove component in periodic directions
    dir = geom%box(:,p)
    proj = dot3d( dir , center ) / ( dir(1)**2 + dir(2)**2 + dir(3)**2 )
    center = dir * proj
    ! ---------------------------------------------------------------------- END
  END FUNCTION center_slab
  

  SUBROUTINE wrap_in_cell( geom , shift )
    ! --------------------------------------------------------------------------
    ! Wrap all atoms in cell dimensions. Use cell dimensions from 0...1
    !
    ! Optionally: shift origin by fractionals (!)
    ! --------------------------------------------------------------------------
    TYPE(pbc)           , INTENT(INOUT) :: geom
    REAL(dp) , OPTIONAL , INTENT(IN)    :: shift(3)
    
    REAL(dp) :: shf(3)
    LOGICAL  :: lr
    INTEGER  :: i
    
    lr = geom%lr
    CALL pbc_convert( geom , .FALSE. )  ! convert to fractionals

    shf  =  0.0D0                       ! set up 
    IF( PRESENT(shift) ) shf = shift

    DO i = 1,size(geom%atm)             ! wrap into cell
      geom%atm(i)%r(:) = geom%atm(i)%r(:) - FLOOR( geom%atm(i)%r(:) - shf(:) )
    ENDDO
    CALL pbc_convert( geom , lr )  ! convert back
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE wrap_in_cell


  SUBROUTINE rjoin_slab( geom , plane , maxdist )
    ! --------------------------------------------------------------------------
    ! repair a disjoint multislab that is split along unit cell borders
    !
    ! What it does:  reorder (!), find vacuum layer, wrap atoms to joint layer
    !
    ! What it does not:  shift atoms within unit cell (all atoms are between
    !                    -dir and +dir
    ! --------------------------------------------------------------------------
    TYPE(pbc)        ,  INTENT(INOUT)  :: geom
    INTEGER            ,   INTENT(IN)  :: plane
    REAL(dp) , OPTIONAL , INTENT(OUT)  :: maxdist

    CHARACTER(LEN=2) :: sortkey
    REAL(dp)         :: gap , maxgap , cutpos
    INTEGER          :: i , p

    p = MODULO(plane-1,3)+1
    WRITE(sortkey,'(I1,A1)') p , '-'

    ! wrap in cell
    CALL pbc_convert( geom , .FALSE. )
    CALL wrap_in_cell( geom )

    ! sort in chosen direction
    CALL sort_one( sortkey , geom%atm )

    ! find cut position
    maxgap = geom%atm(size(geom%atm))%r(p) + 1.0D0 - geom%atm(1)%r(p)
    cutpos = geom%atm(1)%r(p) + 1.0D0 - maxgap/2.0D0
    DO i = 2,size(geom%atm)
      gap = geom%atm(i-1)%r(p) - geom%atm(i)%r(p)
      IF( gap > maxgap ) THEN
        maxgap = gap
        cutpos = geom%atm(i)%r(p) + maxgap/2.0
      END IF
    ENDDO

    ! cut, wrap atoms below cut, connecting slab
    DO i = 1,size(geom%atm)
      IF( geom%atm(i)%r(p) > cutpos ) THEN
        geom%atm(i)%r(p) = geom%atm(i)%r(p) - 1.0D0
      END IF
    ENDDO

    ! return vacuum distance
    IF( PRESENT(maxdist) ) maxdist = maxgap
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE rjoin_slab


  SUBROUTINE join_slab( geom , layer )
    ! --------------------------------------------------------------------------
    ! apply rjoin_slab subroutine and fix order afterwards
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(INOUT)  :: geom
    INTEGER   , INTENT(IN)     :: layer
    INTEGER                    :: i

    DO i=1,SIZE(geom%atm)
      geom%atm(i)%t = i
    END DO

    CALL rjoin_slab( geom , layer )

    CALL sort_by_tag( geom%atm )
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE join_slab


  SUBROUTINE join_wire( geom , axis )
    ! --------------------------------------------------------------------------
    ! apply rjoin_slab subroutine two times and fix order afterwards
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(INOUT)  :: geom
    INTEGER   , INTENT(IN)     :: axis
    INTEGER                    :: i

    DO i=1,SIZE(geom%atm)
      geom%atm(i)%t = i
    END DO

    CALL rjoin_slab( geom , axis+1 )
    CALL rjoin_slab( geom , axis+2 )   ! (fixed by modulo in rjoin_slab)

    CALL sort_by_tag( geom%atm )
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE join_wire


  SUBROUTINE join_molecule( geom )
    ! --------------------------------------------------------------------------
    ! apply rjoin_slab subroutine three times and fix order afterwards
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(INOUT)  :: geom
    INTEGER                    :: i

    DO i=1,SIZE(geom%atm)
      geom%atm(i)%t = i
    END DO

    CALL rjoin_slab( geom , 1 )
    CALL rjoin_slab( geom , 2 )
    CALL rjoin_slab( geom , 3 )

    CALL sort_by_tag( geom%atm )
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE join_molecule


  SUBROUTINE join_system( geom , gap )
    ! --------------------------------------------------------------------------
    ! apply rjoin_slab subroutine three times and fix order afterwards
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(INOUT)  :: geom
    REAL(dp)  , INTENT(OUT)    :: gap(3)
    INTEGER                    :: i

    DO i=1,SIZE(geom%atm)
      geom%atm(i)%t = i
    END DO

    CALL rjoin_slab( geom , 1 , gap(1) )
    CALL rjoin_slab( geom , 2 , gap(2) )
    CALL rjoin_slab( geom , 3 , gap(3) )

    CALL sort_by_tag( geom%atm )
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE join_system


  SUBROUTINE turn_unit_cell( geom , lturn )
    ! --------------------------------------------------------------------------
    ! turn unit cell vectors
    ! lturn = T: a->b , b->c , c->a
    ! lturn = F: other direction
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(INOUT)  :: geom
    LOGICAL   , INTENT(IN)     :: lturn
    REAL(dp)  :: r(3)

    CALL pbc_convert( geom , .TRUE. )

    r = geom%box(:,1) 

    IF( lturn ) THEN
      geom%box(:,1) = geom%box(:,3) 
      geom%box(:,3) = geom%box(:,2) 
      geom%box(:,2) = r

    ELSE
      geom%box(:,1) = geom%box(:,2) 
      geom%box(:,2) = geom%box(:,3) 
      geom%box(:,3) = r

    END IF
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE turn_unit_cell


  SUBROUTINE standardize_box( geom )
  USE params     , ONLY: vac => vacuum_
  USE algebra3d , ONLY: default_align_box , boxheight
  USE params    , ONLY: thr => boxthr_
    ! --------------------------------------------------------------------------
    ! rigorously standardize given geometry (W.I.P)
    ! useful starting point to compare different calculations
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(INOUT)  :: geom
    REAL(dp)  :: gap(3)
    
    ! identify vacuum layers and turn slabs perpendicular to c vector
    ! (layers in two directions are 1D structures, arranged along a vector)
    ! --------------------------------------------------------------------------
    CALL join_system( geom , gap )
    
    gap(1) = gap(1) * boxheight( geom%box , 1 )
    gap(2) = gap(2) * boxheight( geom%box , 2 )
    gap(3) = gap(3) * boxheight( geom%box , 3 )

    ! slabs
    IF( gap(1) >= vac .AND. gap(2) <  vac .AND. gap(3) <  vac ) THEN
      CALL turn_unit_cell( geom , .FALSE. )
    ELSE IF( gap(1) <  vac .AND. gap(2) >= vac .AND. gap(3) <  vac ) THEN
      CALL turn_unit_cell( geom , .TRUE.  )
    ! (wires)
    ELSE IF( gap(1) >= vac .AND. gap(2) >= vac .AND. gap(3) <  vac ) THEN
      CALL turn_unit_cell( geom , .TRUE.  )
    ELSE IF( gap(1) >= vac .AND. gap(2) <  vac .AND. gap(3) >= vac ) THEN
      CALL turn_unit_cell( geom , .FALSE. )
    END IF

    ! find Bravais lattice and swap vectors according to conventions. (this is
    ! also possible for surfaces, there are 5 2D Bravais lattices)
    
    ! rotate to standard direction
    CALL pbc_convert( geom , .FALSE. )
    CALL default_align_box( geom%box )

    ! round small components to zero
    geom%box(1,1) = MERGE( 0.0D0 , geom%box(1,1) , ABS(geom%box(1,1)) < thr )
    geom%box(2,1) = MERGE( 0.0D0 , geom%box(2,1) , ABS(geom%box(2,1)) < thr )
    geom%box(3,1) = MERGE( 0.0D0 , geom%box(3,1) , ABS(geom%box(3,1)) < thr )
    geom%box(1,2) = MERGE( 0.0D0 , geom%box(1,2) , ABS(geom%box(1,2)) < thr )
    geom%box(2,2) = MERGE( 0.0D0 , geom%box(2,2) , ABS(geom%box(2,2)) < thr )
    geom%box(3,2) = MERGE( 0.0D0 , geom%box(3,2) , ABS(geom%box(3,2)) < thr )
    geom%box(1,3) = MERGE( 0.0D0 , geom%box(1,3) , ABS(geom%box(1,3)) < thr )
    geom%box(2,3) = MERGE( 0.0D0 , geom%box(2,3) , ABS(geom%box(2,3)) < thr )
    geom%box(3,3) = MERGE( 0.0D0 , geom%box(3,3) , ABS(geom%box(3,3)) < thr )

    ! ---------------------------------------------------------------------- END
  END SUBROUTINE standardize_box


  SUBROUTINE adsorb_atoms( geom , atm , sads , ssurf , lchain , oshift )
  USE params    , ONLY: pi_ , stderr
  USE algebra3d , ONLY: angle3d , dihedral3d , ncross3d , rotmat3d , rot_align , rot_align_plane

    TYPE(pbc)                , INTENT(INOUT) :: geom
    TYPE(atom) , ALLOCATABLE , INTENT(INOUT) :: atm(:)
    INTEGER    , ALLOCATABLE , INTENT(IN)    :: sads(:) , ssurf(:)
    LOGICAL                  , INTENT(IN)    :: lchain    ! chain mode: bind via one link (1st atom) and use 2nd/3rd atom for angle/dihedral arrangement
    REAL(dp)   , OPTIONAL    , INTENT(IN)    :: oshift(3) ! shift (after arrengements in adsorbate coordinates)

    INTEGER , ALLOCATABLE :: selec(:)               ! substitution based on sads, ssurf, lchain

    REAL(dp)              :: pads(3)  , psurf(3)    ! pads/psurf: reference points
    REAL(dp)              :: shift(3) , rotate(3,3) ! arrange with shift and rotation
    REAL(dp)              :: ishift(3)

    REAL(dp)              :: a_b(3) , a_c(3)        ! bond vectors in adsorbate
    REAL(dp)              :: x_y(3) , x_z(3)        ! bond vectors in surface

    REAL(dp)              :: norm(3)                ! normal vector
    REAL(dp)              :: tmp , rtmp(3)          ! (for averaging positions)

    REAL(dp)              :: angles(3)              ! angles to average


    INTEGER               :: i , k , l

    ! --------------------------------------------------------------------------
    ! always: make cartesian , shift atoms, merge/substitute atoms
    ! conditionals: set shift/rotation matrix
    ! --------------------------------------------------------------------------
    CALL pbc_convert( geom , .TRUE. )              ! 1. convert to cartesian
    ishift = 0.0D0
    IF( PRESENT(oshift) ) ishift = oshift
    
    rotate = RESHAPE( (/1.0D0,0.0D0,0.0D0,0.0D0,1.0D0,0.0D0,0.0D0,0.0D0,1.0D0/) , (/3,3/) )

    ! --------------------------------------------------------------------------
    ! allocate and initialize selection
    ! 1. adsorbate atoms shall be replaced by surface atoms
    ! 2. remaining surface atoms are deleted
    !    (rectified accordingly in 1-bond mode)
    ! 3. if more adsorbate than surface atoms, ignore last atoms
    k  = MIN( SIZE(sads) , SIZE(ssurf) )
    ALLOCATE( selec(SIZE(ssurf)) )
    selec( : k     ) = sads
    selec(   k+1 : ) = ssurf( k+1 : ) + SIZE(atm)

    ! --------------------------------------------------------------------------
    IF( lchain ) THEN

      pads  =       atm( sads (1) )%r          ! center around 1st atom
      psurf =  geom%atm( ssurf(1) )%r          !

      IF( SIZE(sads) >= 2 ) THEN
        selec(2) = ssurf(2) + SIZE(atm)        ! substitute surface atom

        a_b =       atm( sads (2) )%r - pads   ! set bonds
        x_y =  geom%atm( ssurf(2) )%r - psurf  !

        rotate = rot_align( a_b , x_y )        ! set rotations matrix
      END IF

      IF( SIZE(sads) == 3 ) THEN
        a_c =       atm( sads (3) )%r - pads   ! set more bonds
        x_z =  geom%atm( ssurf(3) )%r - psurf  !

        a_c = MATMUL( rotate , a_c )           ! rotate bond in adsorbate

                                               ! update rotation matrix
        rotate = MATMUL( rot_align_plane( a_c , x_y , x_z ) , rotate )

      END IF

    ! --------------------------------------------------------------------------
    ELSE

      ! center around selected atoms
      pads  =  0.0D0
      psurf =  0.0D0
      DO i = 1,k
        pads  =  pads  +      atm( sads (i) )%r
        psurf =  psurf + geom%atm( ssurf(i) )%r
      END DO
      pads  =  pads  / k
      psurf =  psurf / k
        

      ! ---------------------------------------------------------
      ! 2 atom pairs
      IF( SIZE(sads) == 2 ) THEN
        
        a_b =       atm( sads (2) )%r -      atm( sads (1) )%r
        x_y =  geom%atm( ssurf(2) )%r - geom%atm( ssurf(1) )%r


!        ! swap adsorbate selection if angle beyond 90 deg
!        IF( angle3d(a_b,x_y) >  pi_/2 ) THEN
!          a_b = -a_b
!          k   = sads(2)
!          l   = sads(1)
!        ELSE
!          k   = sads(1)
!          l   = sads(2)
!        END IF

        ! align atoms
        rotate = rot_align( a_b , x_y )          ! align bond

      ! ---------------------------------------------------------
      ! >= 3 atom pairs
      ELSE IF( SIZE(sads) >= 3 ) THEN

        ! two further bonds define triangles in adsorbate, ABC, and surface, XYZ:
        a_b =       atm( sads (2) )%r -      atm( sads (1) )%r
        a_c =       atm( sads (3) )%r -      atm( sads (1) )%r
        x_y =  geom%atm( ssurf(2) )%r - geom%atm( ssurf(1) )%r
        x_z =  geom%atm( ssurf(3) )%r - geom%atm( ssurf(1) )%r

        ! align 3D plane spanned by ABC with XYZ...
        norm   = ncross3d( x_y , x_z )           ! normal vector saved for several uses
        rotate = rot_align( ncross3d(a_b,a_c) , norm )

        ! rotate ABC...
        a_b = MATMUL( rotate , a_b )
        a_c = MATMUL( rotate , a_c )
        
        ! calculate angles from geometric center O, A-O-X, B-O-Y and C-O-Z...
        angles(1) = dihedral3d( a_b+  a_c , norm , x_y+  x_z )
        angles(2) = dihedral3d( a_c-2*a_b , norm , x_z-2*x_y )
        angles(3) = dihedral3d( a_b-2*a_c , norm , x_y-2*x_z )
        
        ! average...
        angles(1) = ( angles(1) + angles(2) + angles(3) ) / 3.0D0

        ! rotate by average... (final rotation matrix!)
        rotate = MATMUL( rotmat3d( norm , angles(1) ) , rotate )

      END IF

      ! ---------------------------------------------------------
      ! rotate + shift 3 adsorbate atoms...
      shift =  psurf - MATMUL( rotate  ,  pads - ishift )

      DO i = 1,k
        ! position adsorbate...
        rtmp  =   MATMUL( rotate , atm(sads(i))%r ) + shift

        tmp = SQRT( SUM( ( geom%atm(ssurf(i))%r - rtmp )**2 ) )
        IF( tmp >= 0.5D0 ) WRITE(stderr,'(A,F6.2,A,I3,A,I3,A)')        &
            "[adsorb]      WARNING, high distance,",tmp," for atoms",  &
                                    ssurf(i), " and", sads(i),"."

        ! average positions...
        geom%atm(ssurf(i))%r = ( geom%atm(ssurf(i))%r + rtmp ) / 2.0D0

      ENDDO

    END IF
    ! ----------------------------------------------------------------------------

    ! rotate and shift
    shift =  psurf - MATMUL( rotate  ,  pads - ishift )
    DO i = 1,SIZE(atm)
      atm(i)%r = MATMUL( rotate , atm(i)%r ) + shift
    ENDDO

    ! saves ads atoms in the beginning of new array
    CALL add_and_delete_atoms( geom , atm , selec )
  
  END SUBROUTINE adsorb_atoms


  SUBROUTINE add_and_delete_atoms( geom , oatm , oselec )
    ! ------------------------------------------------------------------------------------
    ! add and remove atoms
    ! rmselec relates to the new formed set of atoms in geom = atm + old geom
    ! ------------------------------------------------------------------------------------

    TYPE(pbc)  , INTENT(INOUT)         :: geom
    TYPE(atom) , INTENT(IN) , OPTIONAL :: oatm(:)
    INTEGER    , INTENT(IN) , OPTIONAL :: oselec(:)
    
    INTEGER                  :: add , del , nat
    TYPE(atom) , ALLOCATABLE :: atm(:) , tmp(:)
    INTEGER    , ALLOCATABLE :: selec(:)

    INTEGER    :: i , k

    ! ------------------------------------------------------------------------------------
    ! initialization of optionals
    ! atm
    IF( PRESENT(oatm) ) THEN
      ALLOCATE( atm( SIZE(oatm) ) )
      atm = oatm
    ELSE
      ALLOCATE( atm( 0 ) )
    END IF

    ! selec
    IF( PRESENT(oselec) ) THEN
      ALLOCATE( selec( SIZE(oselec) ) )
      selec = oselec
    ELSE
      ALLOCATE( selec( 0 ) )
    END IF

    ! added, deleted and (current) number of atoms
    add = SIZE(atm)
    del = SIZE(selec)
    nat = size(geom%atm)

    ! ------------------------------------------------------------------------------------
    ! add atoms in tmp
    nat = nat + add
    ALLOCATE( tmp( nat ) )

    tmp(     :add) = atm(:)
    tmp(add+1:   ) = geom%atm(:)

    ! mark selected atoms
    DO i = 1,del
      tmp( selec(i) )%t = -42
    ENDDO

    nat = nat - del

    ! reallocate geom
    DEALLOCATE( geom%atm )
    ALLOCATE( geom%atm( nat ) )

    ! write back atoms, ignore tagged
    k = 0
    DO i = 1,SIZE(tmp) ! geom atoms
      IF( tmp(i) % t /= -42 ) THEN
        k = k+1
        geom%atm(k) = tmp(i)
      END IF
    ENDDO

    DEALLOCATE( tmp , atm , selec )

  END SUBROUTINE add_and_delete_atoms


  SUBROUTINE make_sphere( geom , center , radius , nat )
  USE algebra3d, ONLY: bh => boxheight , matinv3d
  USE params, ONLY: stderr
    ! --------------------------------------------------------------------------
    ! make sphere of radius rad, centered around center coordinates.
    ! atoms are sorted by distance. system must be wrapped in cell, with
    ! reasonable cell (all angles 60 < a < 120)
    !
    ! geom positions are not arbitrarily altered, but wrapped around center
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(INOUT)         :: geom
    REAL(dp)  , INTENT(IN)            :: center(3)
    REAL(dp)  , INTENT(IN)            :: radius
    INTEGER   , INTENT(IN) , OPTIONAL :: nat


    REAL(dp)   ,  ALLOCATABLE    :: tmpimg(:,:) &
                                  , img(:,:)  ! image, shift vector for atoms in cell
    TYPE(atom) ,  ALLOCATABLE    :: atm(:)    ! temporary atom list
    TYPE(atom)::  at                          ! temporary atom
    REAL(dp)  ::  rad                         ! positive radius
    REAL(dp)  ::  tmp(3) , rtmp               ! temporary vector + length
    REAL(dp)  ::  diag(3)                     ! body diagonal from origin
    REAL(dp)  ::  ha , hb , hc , maxd         ! box heights, 1/2 of longest diagonal
    INTEGER   ::  na , nb , nc                ! number of periodic images along cell vectors
    INTEGER   ::  i , j , k , l               ! loop counts



    ! ------------------------------------------------------------------
    ! needed quantities
    rad      =  ABS( radius )
    ha       =  bh( geom%box , 1 )
    hb       =  bh( geom%box , 2 )
    hc       =  bh( geom%box , 3 )
    
    ! length of longest diagonal and zero diagonal
    diag(:)  =  - geom%box(:,1) + geom%box(:,2) + geom%box(:,3)
    maxd     =  diag(1)**2 + diag(2)**2 + diag(3)**2

    diag(:)  =  geom%box(:,1) - geom%box(:,2) + geom%box(:,3)
    maxd     =  MAX( maxd , diag(1)**2 + diag(2)**2 + diag(3)**2 )

    diag(:)  =  geom%box(:,1) + geom%box(:,2) - geom%box(:,3)
    maxd     =  MAX( maxd , diag(1)**2 + diag(2)**2 + diag(3)**2 )

    diag(:)  =  geom%box(:,1) + geom%box(:,2) + geom%box(:,3)      ! used to wrap
    maxd     =  MAX( maxd , diag(1)**2 + diag(2)**2 + diag(3)**2 )
    
    maxd     =  SQRT( maxd ) / 2

    ! estimate number of images
    ! --> large box, like na x nb x nc fold supercell
    na       =  ( FLOOR( rad / ha * 2.0D0 ) )
    nb       =  ( FLOOR( rad / hb * 2.0D0 ) )
    nc       =  ( FLOOR( rad / hc * 2.0D0 ) )
    
    ! ------------------------------------------------------------------
    ! set up images

    k   =  (2*na+1) * (2*nb+1) * (2*nc+1)
    ALLOCATE( tmpimg(3,k) )

    ! find actual needed images
    ! --> sphere of images with minecraft-esk resolution
    IF( k > 1 ) THEN

      l = 0
      DO i = -nc , nc
        DO j = -nb , nb
          DO k = -na , na
            tmp(:) =  i*geom%box(:,3) + j*geom%box(:,2) + k*geom%box(:,1)
            rtmp   =  SQRT( tmp(1)**2 + tmp(2)**2 + tmp(3)**2 )
            IF( rtmp <= rad+maxd ) THEN
              l = l+1
              tmpimg(:,l) = tmp(:)
            END IF
            !l = l+1
            !tmpimg(:,l)  =  i*geom%box(:,3) + j*geom%box(:,2) + k*geom%box(:,1)
          ENDDO
        ENDDO
      ENDDO

    ELSE ! one box
    
      l = 1
      tmpimg(:,1) =  0.0D0

    END IF
    
    ALLOCATE( img(3,l) )
    img(:,:)  = tmpimg(:,:l)
    DEALLOCATE( tmpimg )

    ! ------------------------------------------------------------------
    ! find atoms in sphere
    ! --> sphere with real atomic resolution

    ALLOCATE(  atm( l*SIZE(geom%atm) )  )
    
    ! wrap cell around center
    CALL wrap_in_cell( geom , MATMUL( matinv3d(geom%box) , center(:) ) - 0.5D0  )
    CALL pbc_convert( geom , .TRUE. )

    IF( PRESENT(nat) ) l = nat

    k = 0

    DO i = 1 , SIZE(img,2)
      DO j = 1 , SIZE(geom%atm)

        ! set up atom
        at    =  geom%atm(j)
        at%r  =  at%r + img(:,i)

        ! distance to center
        !rtmp  =  SQRT( (at%r(1)-center(1))**2 + (at%r(2)-center(2))**2 + (at%r(3)-center(3))**2 )
        rtmp  =  SQRT( SUM( (at%r-center)**2 ) )

        ! add atom
        IF( rtmp <= rad ) THEN
          k = k+1
          atm(k) = at
        END IF

      ENDDO
    ENDDO
    
    ! ------------------------------------------------------------------
    ! if requested, pick closest nat atoms
    !IF( PRESENT(nat) ) THEN
    !  DO i = 1 , 
    !END IF
    ! ------------------------------------------------------------------
    ! set up new geom

    ! box
    !geom%box  =  ( 2.0D0 * rad + 10.0D0 ) * &
    !             RESHAPE( (/ 1.0D0, 0.0D0, 0.0D0, 0.0D0, 1.0D0, 0.0D0, 0.0D0, 0.0D0, 1.0D0 /) , (/ 3 , 3 /) )
    geom%box  =  ( 2.0D0 * rad + 10.0D0 )/SQRT(2.0D0) * &
                 RESHAPE( (/ 0.0D0, 1.0D0, 1.0D0, 1.0D0, 0.0D0, 1.0D0, 1.0D0, 1.0D0, 0.0D0 /) , (/ 3 , 3 /) )

    ! system
    geom%sys = 0

    ! atom
    !geom%nat = k  ! compatibility
    DEALLOCATE( geom%atm )
    ALLOCATE( geom%atm(k) )
    geom%atm(:) = atm(:k)

    ! ---------------------------------------------------------------------- END
  END SUBROUTINE make_sphere


  SUBROUTINE split_bond( atma , atmb )
    USE types, ONLY: element_weight
    ! --------------------------------------------------------------------------
    ! make initial search vector saved in velocities,
    ! useful starting point in dimer TS searches
    ! CAUTION: cartesian are assumed, zero value for error
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(INOUT)  :: atma , atmb
    REAL(dp)  :: dist , diff(3) , ma , mb

    ! check correctness of input
    diff = atma%r - atmb%r
    dist = SQRT( SUM( diff**2 ) )

    IF( dist <= 0.5 ) THEN
      atma%v = 0.0D0
      atmb%v = 0.0D0
      RETURN
    ENDIF

    ! make normalized vector
    diff = diff / dist

    ! get inverse sqrt masses
    ma = 1 / SQRT( element_weight( atma%e ) )
    mb = 1 / SQRT( element_weight( atmb%e ) )

    ! set up velocity
    atma%v = -ma / (ma+mb) * diff
    atmb%v = +mb / (ma+mb) * diff
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE split_bond

 ! TO DO: sort procedures in groups below

 ! ------------------------------------------------------------------------------
 ! AUXILIARY FUNCTIONS
 ! ------------------------------------------------------------------------------

 ! ------------------------------------------------------------------------------
 ! GEOMETRY TWEAKS (keep structure intact)
 ! ------------------------------------------------------------------------------

 ! ------------------------------------------------------------------------------
 ! CONVERSIONS (add, exhchange or remove atoms, chance unit cell, cut slabs, ...)
 ! ------------------------------------------------------------------------------

END MODULE meteorgy
