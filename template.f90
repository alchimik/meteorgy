PROGRAM rewrite

 USE params     , ONLY: lstr_
 USE parse      , ONLY: generate_output_filename
 USE types      , ONLY: pbc , pbc_print , atom_print , pbc_convert
 USE file_io    , ONLY: dftb_read , dftb_write
 USE accounting , ONLY: sort_one

 IMPLICIT NONE
 ! ----------------------------------------------------------------------------------------
 ! Rewrite a VASP input. If desired, convert between cartesian and fractional coordinates
 ! Copyright (C) 2018 Adrian Hühn
 ! 
 ! This program is free software; you can redistribute it and/or modify it under
 ! the terms of the GNU General Public License as published by the Free Software
 ! Foundation; either version 2 of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful, but WITHOUT ANY
 ! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 ! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License along with this
 ! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
 ! Fifth Floor, Boston, MA  02110-1301, USA.
 ! ----------------------------------------------------------------------------------------
 
  CHARACTER(LEN=:)  , ALLOCATABLE  :: infile , code

  TYPE(pbc)  :: geom

  INTEGER    :: stat
  INTEGER    :: sys
  INTEGER    :: i
  
  INTEGER , ALLOCATABLE :: elem(:)

  ALLOCATE( CHARACTER(LEN=lstr_) :: infile )

  ! ----------------------------------------------------------------------------
  ! PARSE COMMAND LINE
  ! ----------------------------------------------------------------------------

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=infile  , STATUS=stat )
  IF( stat /= 0 ) infile(:) = query   ! no allocation with (:)
  infile = TRIM(infile)
  
  
  ! ----------------------------------------------------------------------------
  ! READ INPUT
  ! ----------------------------------------------------------------------------

  CALL magic_read( infile , geom , stat , code )   ! read system
  IF( stat /= 0 ) STOP 1

  ! ----------------------------------------------------------------------------
  ! DO STUFF BELOW
  ! ----------------------------------------------------------------------------

  ! ----------------------------------------------------------------------------
  ! WRITE OUTPUT
  ! ----------------------------------------------------------------------------
  CALL any_write( generate_output_filename(infile,code) , geom , stat , code )

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM rewrite
